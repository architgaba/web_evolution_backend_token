package com.sage.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * EXPOSING API ON SWAGGER
 *
 *
 **/
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("BrilliantLink API Documentation")
                .description("Build By Nile Technologies Pvt. ltd.")
                .version("1.0.0")
                .contact(new Contact("Nile Technologies Pvt. Ltd.", "http://www.niletechnologies.com", "contact@contact.com.uy"))
                .build();
    }

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sage.api"))
                .build()
                .apiInfo(apiInfo());
    }
}
