package com.sage.api.configuration;

import com.sage.api.domain.login.Agent;
import com.sage.api.service.login.AgentLoginServiceImpl;
import com.sage.api.utils.JwtConstants;
import com.sage.api.utils.LicenseParser;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger log = LogManager.getLogger(JwtAuthenticationFilter.class);
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AgentLoginServiceImpl agentLoginService;
    @Autowired
    private LicenseParser parser;

    /**
     *
     * FILTERING REQUEST( VALIDATING TOKEN)
     *
     *
     **/
    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(JwtConstants.HEADER_STRING);
        String username = null;
        String authToken = null;
        if (header != null && header.startsWith(JwtConstants.TOKEN_PREFIX)) {
            authToken = header.replace(JwtConstants.TOKEN_PREFIX, "");
            try {
                username = jwtTokenUtil.getUsernameFromToken(authToken);
                System.out.println(username);
            } catch (IllegalArgumentException e) {
                log.error("an error occurred during getting username from token", e);
            } catch (ExpiredJwtException e) {
                log.warn("the token is expired and not valid anymore", e);
            } catch (SignatureException e) {
                log.error("Authentication Failed. Username or Password not valid.");
            }
        } else {
            log.warn("couldn't find bearer string, will ignore the header");
        }
        if (username != null) {
            Agent agent = agentLoginService.loadUserByUsername(username);
            System.out.println("User Details:::::::::::::::::::::" + agent);
            if (jwtTokenUtil.validateToken(authToken, agent)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(agent, null, Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
                System.out.println("User Details:::::::::::::::::::::" + authToken);
                System.out.println("User Details:::::::::::::::::::::" + authentication);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
                log.info("authenticated user " + username + ", setting security context");
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(req, res);
    }
}
