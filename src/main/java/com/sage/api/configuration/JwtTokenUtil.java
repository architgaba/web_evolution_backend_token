package com.sage.api.configuration;

import com.sage.api.domain.login.Agent;
import com.sage.api.utils.JwtConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.function.Function;


@Component
public class JwtTokenUtil {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final Logger log = LogManager.getLogger(JwtTokenUtil.class);

    /**
     *
     * GENERATING TOKEN FOR NEW USER
     *
     *
     **/
    public String generateToken(Agent agent, String deviceType,String password) {
        return doGenerateToken(agent.getCAgentName(), agent.getIdAgents(), deviceType,password);
    }

    /**
     *
     * GENERATING TOKEN FOR NEW USER
     *
     *
     **/
    private String doGenerateToken(String subject, int agentId, String deviceType,String password) {
        Claims claims = Jwts.claims().setSubject(subject);
        String userId = String.valueOf(agentId);
        claims.put("deviceType", deviceType);
        claims.put("pass",password);
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer("https://brilliantlink.co.za")
                .setAudience(userId)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JwtConstants.ACCESS_TOKEN_VALIDITY_SECONDS * 100))
                .signWith(SignatureAlgorithm.HS256, JwtConstants.SIGNING_KEY)
                .compact();
        return token;
    }

    /**
     *
     * FETCHING AGENT NAME FROM TOKEN
     *
     *
     **/
    public String getUsernameFromToken(String token) {
        String key = token.replace(JwtConstants.TOKEN_PREFIX, "");
        return getClaimFromToken(key, Claims::getSubject);
    }

    /**
     *
     * FETCHING TOKEN EXPIRY DATE FROM TOKEN
     *
     *
     **/
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     *
     * FETCHING CLAIMS(PAYLOAD) FROM TOKEN
     *
     *
     **/
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     *
     * FETCHING CLAIMS FROM TOKEN
     *
     *
     **/
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     *
     * VALIDATING TOKEN
     *
     *
     **/
    public Boolean validateToken(String token, Agent userDetails) {
        final String username = getUsernameFromToken(token);
        String deviceType = getDeviceTypeFromToken(token);
        String query = "select " + deviceType + " from agent_login_status where name=?";
        String validToken = jdbcTemplate.queryForObject(query, new Object[]{username}, String.class);
        if(validToken.equals("0"))
            return false;
        else if (checkPass(token,validToken) && username.equals(userDetails.getCAgentName()) && !isTokenExpired(token))
            return true;
        else
            return false;
    }


    /**
     *
     * VALIDATING TOKEN EXPIRY
     *
     *
     **/
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     *
     * FETCHING DEVICE FROM TOKEN
     *
     *
     **/
    public String getDeviceTypeFromToken(String token) {
        String finalToken = token.replace(JwtConstants.TOKEN_PREFIX, "");
        Claims claims = Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(finalToken)
                .getBody();
        String deviceType = claims.get("deviceType", String.class);
        return deviceType;
    }

    /**
     *
     * FETCHING AGENT PASSWORD FROM TOKEN
     *
     *
     **/
  public String getPasswordFromToken(String token) {
        String finalToken = token.replace(JwtConstants.TOKEN_PREFIX, "");
        Claims claims = Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(finalToken)
                .getBody();
        String password = claims.get("pass", String.class);
        return password;
    }

    /**
     *
     * FETCHING AGENT ID FROM TOKEN
     *
     *
     **/
    public Integer getAgentIdFromToken(String token) {
        String finalToken = token.replace(JwtConstants.TOKEN_PREFIX, "");
        log.info("getAgentIdFromToken" + token);
        Claims claims = Jwts.parser()
                .setSigningKey(JwtConstants.SIGNING_KEY)
                .parseClaimsJws(finalToken)
                .getBody();
        String id = claims.getAudience();
        int agentID = Integer.parseInt(id);
        return agentID;
    }

    /**
     *
     * VALIDATING TOKEN
     *
     *
     **/
    private boolean checkPass(String plainPassword, String hashedPassword) {
        if (BCrypt.checkpw(plainPassword, hashedPassword))
            return true;
        else
            return false;
    }


}
