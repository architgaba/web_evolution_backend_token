package com.sage.api.controller.dashboard;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.service.dashboard.DashboardService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {
    private static final Logger log = LogManager.getLogger(DashboardController.class);
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /*
     *
     * API FOR FETCHING DASHBOARD DATA
     *
     *
     */
    @GetMapping("/data")
    ResponseEntity<?> dashboardData(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: DashboardController :::: method :::: dashboardData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return dashboardService.data(authKey);
    }

    /*
     *
     * API FOR LOGGING OUT AGENT
     *
     *
     */
    @GetMapping("/agentlogout")
    ResponseEntity<?> agentLogout(@RequestHeader("Authorization") String token) {
        log.info("Entering Controller Class :::: DashboardController :::: method :::: agentLogout :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(token));
        return dashboardService.agentLogOut(token);
    }
}
