package com.sage.api.controller.documentation;

import com.sage.api.domain.documentation.ParameterResultVO;
import com.sage.api.service.documentation.QuickLinksService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/quicklinks")
public class QuickLinksController {

    private static final Logger log = LogManager.getLogger(QuickLinksController.class);

    @Autowired
    private QuickLinksService quickLinksService;

    @ResponseBody
    @GetMapping("/listallendpoints")
    public ResponseEntity<?> getEndPointsList(){
        log.info("Enter into QuickLinksController class inside method getEndPointsList");
        return quickLinksService.getEndPointsList();
    }


    @PostMapping("/insertendpoint")
    public ResponseEntity<?> insertEndPoint(@RequestParam String endpoint,@RequestParam String description,@RequestParam String category){
        log.info("Enter into QuickLinksController class inside method insertEndPoint");

        return quickLinksService.insertEndPoint(endpoint,description,category);
    }

    @GetMapping("/getendpointbyid/{id}")
    public ResponseEntity<?> getEndPointById(@PathVariable String id){
        log.info("Enter into QuickLinksController class inside method getEndPointById");

        return quickLinksService.getEndPointById(id);

    }

    @PostMapping("/insertparameters")
    public ResponseEntity<?> insertparametersbyid(@RequestBody ParameterResultVO parameterResultVO){
        log.info("Enter into QuickLinksController class inside method insertparametersbyid");
        return quickLinksService.insertParametersById(parameterResultVO);

    }








}
