package com.sage.api.controller.purchaseorder.transactions;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.ExistingOrder;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.Order;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperExistingPuchaseOrder;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperPurchaseOrder;
import com.sage.api.domain.template.MailSender;
import com.sage.api.service.purchaseorder.transactions.PurchaseOrderTransactionService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/purchaseorder/transactions")
public class PurchaseOrderTransactionController {

    private static final Logger log = LogManager.getLogger(PurchaseOrderTransactionController.class);
    @Autowired
    private PurchaseOrderTransactionService purchaseOrderListService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR CREATE/PLACE PURCHASE ORDER
     *
     *
     **/
    @PostMapping("/placepurchaseorder")
    public ResponseEntity<?> placePurchaseOrder(@RequestHeader("Authorization") String authKey, @RequestBody WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController:::: method :::: placePurchaseOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        Order placePurchaseOrder = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null)
            return purchaseOrderListService.placePurchaseOrder(authKey, wrapperPurchaseOrderRO);
        log.info("Exiting Controller Class :::: PurchaseOrderTransactionController:::: method :::: placePurchaseOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API TO UPDATE EXISTING PURCHASE ORDER
     *
     *
     **/
    @PostMapping("/placeexistingpurchaseorder")
    ResponseEntity<?> purchaseOrderExistingPlaceOrder(@RequestHeader("Authorization") String authKey, @RequestBody WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController:::: method :::: purchaseOrderExistingPlaceOrder :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        ExistingOrder placePurchaseOrder = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null)
            return purchaseOrderListService.placeExistingPurchaseOrder(authKey, WrapperExistingPuchaseOrder);
        log.info("Exiting Controller Class :::: PurchaseOrderTransactionController:::: method :::: purchaseOrderExistingPlaceOrder :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API TO INVOICE EXISTING PURCHASE ORDER
     *
     *
     **/
    @PostMapping("/existingprocessinvoice")
    ResponseEntity<?> existingProcessInvoice(@RequestHeader("Authorization") String authKey, @RequestBody WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController:::: method :::: existingProcessInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        ExistingOrder placePurchaseOrder = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null)
            return purchaseOrderListService.purchaseOrderProcessInvoice(authKey, WrapperExistingPuchaseOrder);
        log.info("Exiting Controller Class :::: PurchaseOrderTransactionController::::  method :::: existingProcessInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR DIRECTLY NEW INVOICE
     *
     *
     **/
    @PostMapping("/purchaseorderprocessinvoice")
    ResponseEntity<?> purchaseOrderProcessInvoice(@RequestHeader("Authorization") String authKey, @RequestBody WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController:::: method :::: purchaseOrderProcessInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        Order placePurchaseOrder = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null)
            return purchaseOrderListService.purchaseOrderExistingProcessInvoice(authKey, wrapperPurchaseOrderRO);
        log.info("Entering If block :::: PurchaseOrderTransactionController::::  method :::: purchaseOrderProcessInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR TEMPLATE FUNCTIONALITY
     *
     *
     **/
    @PostMapping("/templatefunctionatlity")
    public ResponseEntity<?> templateFunctionality(@RequestHeader("Authorization") String authKey,@RequestParam(name = "reference", required = true) String reference, @RequestParam(name = "action", required = true) String action) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController :::: method :::: templateFunctionality :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        if (reference != null && action != null)
            return purchaseOrderListService.templateFunctionality(reference, action);
        log.info("Exiting Controller Class :::: PurchaseOrderTransactionController :::: method :::: templateFunctionality :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR MAIL DOCUMENT REFERENCE
     *
     *
     **/
    @PostMapping("/maildocument/{reference}")
    public ResponseEntity<?> mailSender(@RequestHeader("Authorization") String authKey,@RequestBody MailSender mailData, @PathVariable String reference) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController :::: method :::: templateFunctionality :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        if (mailData != null && reference != null)
            return purchaseOrderListService.templateMailer(mailData, reference);
        log.info("Exiting Controller Class :::: SalesOrderTransactionController :::: method :::: templateFunctionality  :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR NEW RECEIVE STOCK
     *
     *
     **/
    @PostMapping("/purchaseorderreceivestock")
    public ResponseEntity<?> purchaseOrderReceiveStock(@RequestHeader("Authorization") String authKey, @RequestBody WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionController:::: method :::: purchaseOrderReceiveStock :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        Order placePurchaseOrder = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null )
            return purchaseOrderListService.existingReceiveStock(authKey, wrapperPurchaseOrderRO);
        log.info("Exiting Controller Class :::: PurchaseOrderTransactionController:::: method :::: purchaseOrderReceiveStock :: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);

    }

    /**
     *
     * API TO UPDATE EXISTING PURCHASE ORDER INTO RECEIVE STOCK
     *
     *
     **/
    @PostMapping("/existingreceivestock")
    public ResponseEntity<?> existingReceiveStock(@RequestHeader("Authorization") String authKey, @RequestBody WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Controller Class :::: PurchaseOrderListController:::: method :::: existingReceiveStock :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        ExistingOrder placePurchaseOrder = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder();
        if (placePurchaseOrder != null)
            return purchaseOrderListService.purchaseOrderReceiveStock(authKey, WrapperExistingPuchaseOrder);
        log.info("Exiting Controller Class :::: PurchaseOrderListController:::: method :::: existingReceiveStock :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API TO FETCH GL DROPDOWN LIST
     *
     *
     **/
    @GetMapping("/gldropdown")
    public ResponseEntity<?> fetchingGLDropDownValue(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionsController:::: method :::: fetchingGLDropDownValue  :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return purchaseOrderListService.purchaseOrderGLDropdown();
    }

    /**
     *
     * API TO FETCH STOCK DROPDOWN LIST
     *
     *
     **/
    @GetMapping("/stkdropdown")
    public ResponseEntity<?> fetchingSTKDropDownValue(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionsController:::: method :::: fetchingSTKDropDownValue   :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return purchaseOrderListService.purchaseOrderSTKDropdown();
    }

    /**
     *
     * API TO FETCH SUPPLIERS DETAILS LIST
     *
     *
     **/
    @GetMapping("/supplierdetails")
    public ResponseEntity<?> fetchSupplierDetails(@RequestHeader("Authorization") String authKey,@RequestParam(value = "name",required = true) String name,@RequestParam(value = "docId",required = false) String docId) {
        log.info("Entering Controller Class :::: PurchaseOrderTransactionsController:::: method :::: fetchCustomerDetails   :::: Agent :::: "+jwtTokenUtil.getUsernameFromToken(authKey));
        return purchaseOrderListService.fetchingSupplierDetails(name,docId);
    }
}
