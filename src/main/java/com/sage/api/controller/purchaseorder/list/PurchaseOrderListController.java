package com.sage.api.controller.purchaseorder.list;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.purchaseorder.list.PurchaseOrderList;
import com.sage.api.service.purchaseorder.list.PurchaseOrderListService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/purchaseorder/list")
public class PurchaseOrderListController {

    private static final Logger log = LogManager.getLogger(PurchaseOrderListController.class);
    @Autowired
    private PurchaseOrderListService purchaseOrderListService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    /**
     *
     * API FOR FETCHING ALL SUPPLIERS
     *
     *
     **/
    @GetMapping("/suppliers")
    ResponseEntity<?> fetchingSuppliers(@RequestHeader("Authorization") String authKey) {
        log.info("Entering controller class :::: PurchaseOrderListController :::: method :::: fetchingSuppliers :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return purchaseOrderListService.fetchingSuppliers();
    }

    /**
     *
     * API FOR FETCHING PURCHASE ORDER DATA
     *
     *
     **/
    @PostMapping("/data")
    ResponseEntity<?> purchaseOrderListData(@RequestHeader("Authorization") String authKey, @RequestBody PurchaseOrderList list) {
        log.info("Entering controller class :::: PurchaseOrderListController :::: method :::: purchaseOrderListData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (list != null)
            return purchaseOrderListService.fetchingListData(list);
        log.info("Exiting controller class :::: PurchaseOrderListController :::: method :::: purchaseOrderListData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return new ResponseEntity<>(new ResponseDomain(true, "Request Body is Empty"), HttpStatus.BAD_REQUEST);
    }

}
