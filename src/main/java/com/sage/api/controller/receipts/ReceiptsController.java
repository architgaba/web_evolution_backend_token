package com.sage.api.controller.receipts;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.domain.receipts.ReceiptsBody;
import com.sage.api.domain.receipts.Transactions;
import com.sage.api.service.receipts.ReceiptsService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/receipts")
public class ReceiptsController {
    private static final Logger log = LogManager.getLogger(ReceiptsController.class);
    @Autowired
    private ReceiptsService receiptsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR FETCHING ALL CUSTOMER ACCOUNTS
     *
     *
     **/
    @GetMapping("/accounts")
    public ResponseEntity<?> customerAccounts() {
        log.info("Entering Controller Class :::: ReceiptsController :::: method :::: customerAccounts");
        return receiptsService.customerAccounts();
    }

    /**
     *
     * API FOR INVOICE PAYMENT
     *
     *
     **/
    @PostMapping("/payment")
    public ResponseEntity<?> payInvoice(@RequestHeader("Authorization") String authKey, @RequestBody ReceiptsBody receiptsBody) {
        log.info("Entering Controller Class :::: ReceiptsController :::: method :::: payInvoice :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (receiptsBody != null) {
            Transactions transactions = new Transactions();
            transactions.setTransac(receiptsBody.getTransac());
            return receiptsService.invoicePay(transactions, receiptsBody.getPayMode(), receiptsBody.getAmount(), authKey, receiptsBody.getPosChange(), receiptsBody.getPosTendered());
        }
        log.info("Exiting Controller Class :::: ReceiptsController :::: method :::: payInvoice");
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR FETCHING RECEIPTS DATA
     *
     *
     **/
    @PostMapping("/data")
    public ResponseEntity<?> receiptsData(@RequestHeader("Authorization") String authKey, @RequestBody Accounts accounts) {
        String agent = jwtTokenUtil.getUsernameFromToken(authKey);
        log.info("Entering Controller Class :::: ReceiptsController :::: method :::: receiptsData :::: Agent :::: " + agent);
        if (accounts != null) {
            return receiptsService.agentReceipts(accounts, agent);
        }
        log.info("Exiting Controller Class :::: ReceiptsController :::: method :::: receiptsData :::: Agent :::: " + agent);
        return ResponseDomain.badRequest("Request Boduy Missing", false);
    }
}
