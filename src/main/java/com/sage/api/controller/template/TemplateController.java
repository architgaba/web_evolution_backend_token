package com.sage.api.controller.template;

import com.sage.api.domain.template.MailSender;
import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.template.SavedTemplate;
import com.sage.api.service.template.TemplateService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class TemplateController {
    private static final Logger log = LogManager.getLogger(TemplateController.class);
    @Autowired
    private TemplateService templateService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR UPDATING TEMPLATE
     *
     *
     **/
    @PostMapping("/updatetemplate")
    public ResponseEntity<?> updateTemplate(@RequestHeader("Authorization") String auth, @RequestBody SavedTemplate template) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: updateTemplate :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        if (template.getTemplate() == null) {
            log.info("Exiting Controller Class :::: TemplateController :::: method :::: updateTemplate :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
            return new ResponseEntity<>(new ResponseDomain(false, "Kindly Create a template"), HttpStatus.BAD_REQUEST);
        }
        return templateService.updateTemplate(template);
    }

    /**
     *
     * API FOR EMAILING INVOICE TEMPLATE (SALES ORDER)
     *
     *
     **/
    @PostMapping("/maildocument/{docId}")
    public ResponseEntity<?> mailDocument(@RequestHeader("Authorization") String auth, @RequestBody MailSender mailData, @PathVariable String docId) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: mailDocument :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        if (mailData != null && docId != null)
            return templateService.mailSender(mailData, docId);
        log.info("Exiting Controller Class :::: TemplateController :::: method :::: mailDocument :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        return new ResponseEntity<>(new ResponseDomain(true, "Request Body / Parameters Missing"), HttpStatus.BAD_REQUEST);
    }

    /**
     *
     * API FOR DOWNLOADING TEMPLATE (SALES ORDER)
     *
     *
     **/
    @PostMapping("/downloadpdf/{docId}/{docState}")
    public ResponseEntity<?> downloadPDF(@RequestHeader("Authorization") String auth, @PathVariable String docId, @PathVariable String docState) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: downloadPDF :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        if (docId != null && docState != null)
            return templateService.downloadPdf(docId, docState);
        log.info("Exiting Controller Class :::: TemplateController :::: method :::: downloadPDF :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        return new ResponseEntity<>(new ResponseDomain(true, "Request Parameters Missing"), HttpStatus.BAD_REQUEST);
    }

    /**
     *
     * API FOR VIEWING TEMPLATE (SALES ORDER)
     *
     *
     **/
    @GetMapping("/viewtemplate/{docId}/{docState}")
    public ResponseEntity<?> viewTemplateHtml(@RequestHeader("Authorization") String auth, @PathVariable String docId, @PathVariable String docState) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: viewTemplateHtml :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        if (docId != null && docState != null)
            return templateService.viewTemplate(docId, docState);
        log.info("Exiting Controller Class :::: TemplateController :::: method :::: viewTemplateHtml :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(auth));
        return new ResponseEntity<>(new ResponseDomain(true, "Request Parameters Missing"), HttpStatus.BAD_REQUEST);
    }

    /**
     *
     * API FOR DEFAULT EMAIL DATA(SALES ORDER)
     *
     *
     **/
    @GetMapping(value = {"/maildata/{account}/{docState}","/maildata/{account}"})
    public ResponseEntity<?> mailData(@RequestHeader("Authorization") String token, @PathVariable String account, @PathVariable(name = "docState", required = false) String docState, @RequestParam(value = "orderNum", required = false) String orderNum) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: mailData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(token));
        if (account != null)
            return templateService.mailData(account, token, docState, orderNum);
        log.info("Exiting Controller Class :::: TemplateController :::: method :::: mailData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(token));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR VIEWING TEMPLATE (PURCHASE ORDER)
     *
     *
     **/
    @GetMapping("/viewtemplate/purchaseorder/{docId}/{docState}")
    public ResponseEntity<?> viewTemplateHtmlForPurchaseOrder(@RequestHeader("Authorization") String authKey, @PathVariable String docId, @PathVariable String docState) {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: viewTemplateHtmlForPurchaseOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (docId != null && docState != null)
            return templateService.viewTemplateHtmlForPurchaseOrder(docId, docState);
        log.info("Exiting Controller Class :::: TemplateController :::: method :::: viewTemplateHtmlForPurchaseOrder :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return new ResponseEntity<>(new ResponseDomain(true, "Request Parameters Missing"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/downloadpdf/purchaseorder/{docId}/{docState}")
    public ResponseEntity<?> downloadPDFForPurchaseOrder(@PathVariable String docId,@PathVariable String docState)
    {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: downloadPDFForPurchaseOrder "+docState);
        return templateService.downloadPdfForPurchaseOrder(docId,docState);
    }

    @GetMapping("/maildata/purchaseorder/{account}")
    public ResponseEntity<?> mailDataForPurchaseOrder(@RequestHeader("Authorization") String token,@PathVariable String account)
    {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: maildataForPurchaseOrder");
        return templateService.mailDataForPurchaseOrder(account,token);
    }

    @PostMapping("/maildocument/purchaseorder/{docId}")
    public ResponseEntity<?> mailDocumentForPurchaseOrder(@RequestBody MailSender mailData, @PathVariable String docId)
    {
        log.info("Entering Controller Class :::: TemplateController :::: method :::: mailDocumentForPurchaseOrder");
        return templateService.mailSenderForPurchaseOrder(mailData,docId);
    }
}
