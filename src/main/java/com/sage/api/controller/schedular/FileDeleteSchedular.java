/*
package com.sage.api.controller.schedular;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.File;
import java.time.LocalDateTime;

*/
/**
 *
 * SCHEDULAR FOR DELETING TEMPLATES FROM SERVER
 *
 *
 **//*

@Component
public class FileDeleteSchedular {
    private static final Logger logger = LogManager.getLogger(FileDeleteSchedular.class);
    private JdbcTemplate jdbcTemplate;

   // @Scheduled(cron = "0 0 14 * * ?")
    @Scheduled(cron = "10 * * * * ?")
    public void scheduleTaskWithFixedRate() {
        logger.info("Executing delete file Schedular "+ (LocalDateTime.now()));
        String htmlFilepath = System.getProperty("user.dir") + "\\generatedHTMl";
        File folder = new File(htmlFilepath);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile())
                    listOfFiles[i].delete();
            }
        }
        String pdfFilepath = System.getProperty("user.dir") + "\\generatedPDF";
        File pdfFolder = new File(pdfFilepath);
        File[] listOfPDFFiles = pdfFolder.listFiles();
        if (listOfPDFFiles != null) {
            for (int i = 0; i < listOfPDFFiles.length; i++) {
                if (listOfPDFFiles[i].isFile())
                    System.out.println("File " + listOfPDFFiles[i].delete());
            }
        }
        jdbcTemplate.execute("truncate table template");
    }
}


*/
