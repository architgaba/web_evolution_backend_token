package com.sage.api.controller.agentprofile;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.service.agentprofile.AgentProfileService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/agentprofile")
public class AgentProfileController {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AgentProfileService agentProfileService;
    private static final Logger log = LogManager.getLogger(AgentProfileController.class);

    /*
     *
     * API FOR UPLOADING AGENT PROFILE IMAGE
     *
     *
     */
    @PostMapping("/uploadprofileimage")
    private ResponseEntity<?> uploadProfileImage(@RequestHeader("Authorization") String auth, @RequestBody String imageData) {
        String userName = jwtTokenUtil.getUsernameFromToken(auth);
        log.info("Entering Controller Class :::: AgentProfileController :::: method :::: uploadProfileImage :::: Agent :::: " + userName);
        if (imageData != null && imageData.length() > 16)
            return agentProfileService.uploadingProfileImage(imageData, userName);
        log.info("Exiting Controller Class :::: AgentProfileController :::: method :::: uploadProfileImage :::: Agent :::: " + userName);
        return ResponseDomain.badRequest("Request Body Missing",false);
    }

    /*
     *
     * API FOR FETCHING AGENT PROFILE IMAGE
     *
     *
     */
    @GetMapping("/fetchingprofileimage")
    private ResponseEntity<?> fetchingProfileImage(@RequestHeader("Authorization") String authKey) {
        String userName = jwtTokenUtil.getUsernameFromToken(authKey);
        log.info("Entering Controller Class :::: AgentProfileController :::: method :::: fetchingProfileImage :::: Agent :::: "+userName);
        return agentProfileService.fetchingProfileImage(userName);
    }
}
