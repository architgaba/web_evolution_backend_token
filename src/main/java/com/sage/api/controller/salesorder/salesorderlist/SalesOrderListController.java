package com.sage.api.controller.salesorder.salesorderlist;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.salesorder.salesorderlist.SalesOrderList;
import com.sage.api.service.salesorder.salesorderlist.SalesOrderListService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/salesorder")
public class SalesOrderListController {
    private static final Logger log = LogManager.getLogger(SalesOrderListController.class);
    @Autowired
    private SalesOrderListService salesOrderListService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    /**
     *
     * API FOR FETCHING SALES ORDER DATA
     *
     *
     **/
    @PostMapping("/salesorderlistdata")
    public ResponseEntity<?> salesOrderListData(@RequestHeader("Authorization") String authKey, @RequestBody SalesOrderList salesOrderList) {
        log.info("Entering Controller Class ::::  SalesOrderListController :::: method :::: salesOrderListData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (salesOrderList != null)
            return salesOrderListService.fetchingSalesOrderListData(salesOrderList);
        log.info("Exiting Controller Class ::::  SalesOrderListController :::: method :::: salesOrderListData :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

    /**
     *
     * API FOR FETCHING ALL CUSTOMER ACCOUNTS
     *
     *
     **/
    @GetMapping("/salesorderlist")
    public ResponseEntity<?> salesOrderList(@RequestHeader("Authorization") String authKey) {
        log.info("Entering Controller Class ::::  SalesOrderListController :::: method :::: salesOrderList :::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return salesOrderListService.fetchingAccounts();
    }
}
