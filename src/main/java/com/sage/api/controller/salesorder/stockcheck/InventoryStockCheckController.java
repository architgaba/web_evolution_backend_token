package com.sage.api.controller.salesorder.stockcheck;

import com.sage.api.domain.salesorder.stockcheck.InventoryCheck;
import com.sage.api.service.salesorder.stockcheck.StockCheckService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/salesordertransaction")
public class InventoryStockCheckController {
    private static final Logger log = LogManager.getLogger(InventoryStockCheckController.class);
    @Autowired
    private StockCheckService inventoryStockCheck;

    /**
     *
     * API FOR FETCHING INVENTORY STORE DETAIL
     *
     *
     **/
    @GetMapping("/inventorystore")
    ResponseEntity<?> getInventoryStock() {
        log.info("Entering Controller Class :::: InventoryStockCheckController :::: method :::: inventorystore");
        return inventoryStockCheck.getInventoryStock();
    }

    /**
     *
     * API FOR STOCK CHECKING
     *
     *
     **/
    @PostMapping("/stockcheck")
    ResponseEntity<?> stockCheck(@RequestBody InventoryCheck inventoryCheck) {
        log.info("Entering Controller Class :::: InventoryStockCheckController :::: method :::: stockcheck");
        return inventoryStockCheck.stockCheck(inventoryCheck);
    }

}
