package com.sage.api.controller.enquiries.inventory;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.enquiries.Inventory;
import com.sage.api.service.enquiries.inventory.InventoryService;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/enquiries")
public class InventoryController {
    private static final Logger log = LogManager.getLogger(InventoryController.class);
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     *
     * API FOR FETCHING ALL INVENTORY ACCOUNTS
     *
     *
     **/
    @GetMapping("/inventory")
    ResponseEntity<?> getInventoryAccounts() {
        log.info("Entering Controller Class :::: InventoryController :::: method :::: getInventoryAccounts");
        return inventoryService.fetchingAllInventoryAccounts();
    }

    /**
     *
     * API FOR FETCHING INVENTORY DATA
     *
     *
     **/
    @PostMapping("/gettinginventorydata")
    ResponseEntity<?> inventoryData(@RequestHeader("Authorization") String authKey, @RequestBody Inventory inventoryData) {
        log.info("Entering Controller Class :::: InventoryController :::: method :::: inventoryData ::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        if (inventoryData != null)
            return inventoryService.inventoryData(inventoryData);
        log.info("Exiting Controller Class :::: InventoryController :::: method :::: getInventoryAccounts ::: Agent :::: " + jwtTokenUtil.getUsernameFromToken(authKey));
        return ResponseDomain.badRequest("Request Body Missing", false);
    }

}
