package com.sage.api.service.template;

import com.sage.api.domain.template.MailSender;
import com.sage.api.domain.template.SavedTemplate;
import org.springframework.http.ResponseEntity;

public interface TemplateService {

    ResponseEntity<?> updateTemplate(SavedTemplate savedTemplate);

    ResponseEntity<?> downloadPdf(String docId,String docState);

    ResponseEntity<?> downloadPdfForPurchaseOrder(String docId,String docState);

    ResponseEntity<?> viewTemplate(String docId,String docState);

    ResponseEntity<?> viewTemplateHtmlForPurchaseOrder(String docId,String docState);

    ResponseEntity<?> mailData(String account,String authKey, String docState, String orderNum);

    ResponseEntity<?> mailDataForPurchaseOrder(String account,String authKey);

    ResponseEntity<?> mailSender(MailSender sender,String docId);

    ResponseEntity<?> mailSenderForPurchaseOrder(MailSender sender,String docId);

}
