package com.sage.api.service.template;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.template.*;
import com.sage.api.repository.template.TemplateCreator;
import com.sage.api.repository.template.TemplateRepository;
import com.sage.api.utils.JwtConstants;
import com.sage.api.utils.PDFConverter;
import com.sage.api.utils.ResponseDomain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.*;
import java.util.*;

@Service
public class TemplateServiceImpl implements TemplateService {

    private static final Logger log = LogManager.getLogger(TemplateServiceImpl.class);
    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private PDFConverter pdfConverter;
    @Autowired
    private TemplateCreator templateCreator;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private ResourceLoader resourceLoader;


    /**
     *
     * UPDATING TEMPLATE
     *
     *
     **/
    @Override
    public ResponseEntity<?> updateTemplate(SavedTemplate savedTemplate) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate");
        try {
            List<SavedTemplate> templateList = templateRepository.findByDocId(savedTemplate.getDocId());
            SavedTemplate templateObj = templateList.get(0);
            templateObj.setTemplate(savedTemplate.getTemplate());
            templateRepository.save(templateObj);
            pdfConverter.generateHTML(savedTemplate.getTemplate(), generatePOJO(savedTemplate.getDocId().toString()), savedTemplate.getDocId().toString());
            pdfConverter.convertTOPDF(savedTemplate.getTemplate(), savedTemplate.getDocId().toString());
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate");
            return new ResponseEntity<>(new ResponseDomain(true, "template updated successfully"), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            templateRepository.save(savedTemplate);
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: updateTemplate :::: Error :::: " + e);
            return new ResponseEntity<>(new ResponseDomain(true, "Error Occurred in updating template"), HttpStatus.OK);
        }
    }

    /**
     *
     * DOWNLOADING TEMPLATE (SALES ORDER)
     *
     *
     **/
    @Override
    public ResponseEntity<?> downloadPdf(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf");
        String currentDirectory = System.getProperty("user.dir");
        File f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        if (!f.exists()) {
            viewTemplate(docId, docState);
            f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        }
        try {
            byte[] data = pdfConverter.downloadPdf(f, docId);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf");
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } catch (Exception e1) {
            e1.printStackTrace();
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: downloadPdf :::: Error :::: "+e1);
            return new ResponseEntity<>(new ResponseDomain(true, "" + e1), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> downloadPdfForPurchaseOrder(String docId, String docState) {
        log.info("********" + System.getProperty("user.dir") + "$$$$$$$$$$ " + docState);
        String currentDirectory = System.getProperty("user.dir");
        File f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        if (!f.exists()) {
            viewTemplateHtmlForPurchaseOrder(docId, docState);
            f = new File(currentDirectory + "\\generatedPDF\\" + docId + ".pdf");
        }
        try {
            byte[] data = pdfConverter.downloadPdf(f, docId);
            log.info("-pdf file path--->>" + f.getPath());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            String filename = "output.pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } catch (Exception e1) {
            e1.printStackTrace();
            return new ResponseEntity<>(new ResponseDomain(true, "" + e1), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     *
     * GENERATING POJO FOR TEMPLATE (SALES ORDER)
     *
     *
     **/
    public TemplatePOJO generatePOJO(String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: generatePOJO");
        TemplatePOJO templatePOJO = templateCreator.templateData(docId);
        TemplateOrderTotals orderTotals = templateCreator.templateOrderData(templatePOJO.getDocState(), docId);
        List<TemplateLines> templateLines = templateCreator.templateLinesData(docId, templatePOJO.getRepCode(), templatePOJO.getDocState());
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        List<String> tableHeader = new ArrayList<String>();
        Lines lines = new Lines();
        lines.setTableRow(templateLines);
        templatePOJO.setTotalIncl(orderTotals.getTotalIncl());
        templatePOJO.setTotalExc(orderTotals.getTotalExc());
        templatePOJO.setTax(orderTotals.getTax());
        templatePOJO.setDiscount(orderTotals.getDiscount());
        templatePOJO.setTotalInclFinal(orderTotals.getTotalInclFinal());
        String docStateCheck = templatePOJO.getDocState();
        switch (docStateCheck) {
            case "Quotation":
                templatePOJO.setDocState("Quotation");
                break;
            case "Archived":
                templatePOJO.setDocState("Tax Invoice");
                break;
            case "Archived Quotation":
                templatePOJO.setDocState("Archived Quotation");
                break;
            case "Partially Processed":
                templatePOJO.setDocState("Sales Order Confirmation");
                break;
            case "Unprocessed":
                templatePOJO.setDocState("Sales Order Confirmation");
                break;
        }
        if ("Quotation".equals(templatePOJO.getDocState()) || "Tax Invoice".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Units");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        } else if ("Sales Order Confirmation".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Qty Ordered");
            tableHeader.add("Qty Processed");
            tableHeader.add("Qty To Invoice");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        }
        lines.setFieldNames(tableHeader);
        templatePOJO.setLines(lines);
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        templatePOJO.setTotalIncl(orderTotals.getTotalIncl());
        templatePOJO.setTotalExc(orderTotals.getTotalExc());
        templatePOJO.setTax(orderTotals.getTax());
        templatePOJO.setDiscount(orderTotals.getDiscount());
        templatePOJO.setTotalInclFinal(orderTotals.getTotalInclFinal());
        if(templatePOJO.getRepCode()==null)
            templatePOJO.setRepCode("");

        List<TemplateLines> tableRow = templatePOJO.getLines().getTableRow();

        tableRow.forEach(line->{
            if(line.getRep()==null)
                line.setRep("");
        });
        if(templatePOJO.getTelephone()==null)
            templatePOJO.setTelephone("");

        if(templatePOJO.getContactPerson()==null)
            templatePOJO.setContactPerson("");

        if(templatePOJO.getFax()==null)
            templatePOJO.setFax("");

        if(templatePOJO.getEmail1()==null)
            templatePOJO.setEmail1("");


        if(templatePOJO.getPo1()==null)
            templatePOJO.setPo1("");

        if(templatePOJO.getPo2()==null)
            templatePOJO.setPo2("");

        if(templatePOJO.getPo3()==null)
            templatePOJO.setPo3("");

        if(templatePOJO.getPa1()==null)
            templatePOJO.setPa1("");

        if(templatePOJO.getPa2()==null)
            templatePOJO.setPa2("");

        if(templatePOJO.getPa3()==null)
            templatePOJO.setPa3("");

        if(templatePOJO.getPa4()==null)
            templatePOJO.setPa4("");

        return templatePOJO;

    }

    /**
     *
     * GENERATING POJO FOR TEMPLATE (PURCHASE ORDER)
     *
     *
     **/
    public TemplatePOJO generatePOJOForPurchaseOrder(String docId) {

        TemplatePOJO templatePOJO = templateCreator.templateSupplierData(docId);
        TemplateOrderTotals orderTotals = templateCreator.templateOrderData(templatePOJO.getDocState(), docId);
        List<TemplateLines> templateLines = templateCreator.templateLinesData(docId, templatePOJO.getRepCode(), templatePOJO.getDocState());
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        List<String> tableHeader = new ArrayList<String>();
        Lines lines = new Lines();
        lines.setTableRow(templateLines);
        templatePOJO.setTotalIncl(orderTotals.getTotalIncl());
        templatePOJO.setTotalExc(orderTotals.getTotalExc());
        templatePOJO.setTax(orderTotals.getTax());
        templatePOJO.setDiscount(orderTotals.getDiscount());
        templatePOJO.setTotalInclFinal(orderTotals.getTotalInclFinal());
        String docStateCheck = templatePOJO.getDocState();
        switch (docStateCheck) {
            case "Quotation":
                templatePOJO.setDocState("Quotation");
                break;
            case "Archived":
                templatePOJO.setDocState("Tax Invoice");
                break;
            case "Partially Processed":
                templatePOJO.setDocState("Purchase Order Confirmation");
                break;
            case "Unprocessed":
                templatePOJO.setDocState("Purchase Order Confirmation");
                break;
        }
        if ("Quotation".equals(templatePOJO.getDocState()) || "Tax Invoice".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Units");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        } else if ("Purchase Order Confirmation".equals(templatePOJO.getDocState())) {
            tableHeader.add("Code");
            tableHeader.add("Item Description");
            tableHeader.add("Rep");
            tableHeader.add("Qty Ordered");
            tableHeader.add("Qty Processed");
            tableHeader.add("Qty To Invoice");
            tableHeader.add("Price(Exc)");
            tableHeader.add("Disc(%)");
            tableHeader.add("Tax");
            tableHeader.add("Total(Incl)");

        }
        lines.setFieldNames(tableHeader);
        templatePOJO.setLines(lines);
        orderTotals.setTotalInclFinal(orderTotals.getTotalIncl());
        templatePOJO.setTotalIncl(orderTotals.getTotalIncl());
        templatePOJO.setTotalExc(orderTotals.getTotalExc());
        templatePOJO.setTax(orderTotals.getTax());
        templatePOJO.setDiscount(orderTotals.getDiscount());
        templatePOJO.setTotalInclFinal(orderTotals.getTotalInclFinal());

        if(templatePOJO.getRepCode()==null)
            templatePOJO.setRepCode("");

        List<TemplateLines> tableRow = templatePOJO.getLines().getTableRow();

        tableRow.forEach(line->{
            if(line.getRep()==null)
                line.setRep("");
        });
        if(templatePOJO.getTelephone()==null)
            templatePOJO.setTelephone("");

        if(templatePOJO.getContactPerson()==null)
            templatePOJO.setContactPerson("");

        if(templatePOJO.getFax()==null)
            templatePOJO.setFax("");

        if(templatePOJO.getEmail1()==null)
            templatePOJO.setEmail1("");


        if(templatePOJO.getPo1()==null)
            templatePOJO.setPo1("");

        if(templatePOJO.getPo2()==null)
            templatePOJO.setPo2("");

        if(templatePOJO.getPo3()==null)
            templatePOJO.setPo3("");

        if(templatePOJO.getPa1()==null)
            templatePOJO.setPa1("");

        if(templatePOJO.getPa2()==null)
            templatePOJO.setPa2("");

        if(templatePOJO.getPa3()==null)
            templatePOJO.setPa3("");

        if(templatePOJO.getPa4()==null)
            templatePOJO.setPa4("");


        return templatePOJO;

    }

    /**
     *
     * VIEWING TEMPLATE (SALES ORDER)
     *
     *
     **/
    @Override
    public ResponseEntity<?> viewTemplate(String docId, String docState) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate");
        List<SavedTemplate> savedTemplate = templateRepository.findByDocId(Long.parseLong(docId));
        SavedTemplate template = null;
        if (savedTemplate.isEmpty()) {
            try {
                String path = System.getProperty("user.dir");
                path = path + "\\src\\main\\resources\\GenericTemplate.html";
                String GenericTemplate = new String(Files.readAllBytes(Paths.get
                        (path)));
               /* Resource resource = resourceLoader.getResource("classpath:GenericTemplate.html");
                String GenericTemplate = new String(Files.readAllBytes(Paths.get
                        (resource.getURL().toString().substring(resource.getURL().toString().indexOf("/")+1))));*/
                TemplatePOJO temp=generatePOJO(docId);
                String convertedTemplate = pdfConverter.generateHTML(GenericTemplate, generatePOJO(docId), docId);
                SavedTemplate savedTemplateObj = new SavedTemplate();
                savedTemplateObj.setTemplate(convertedTemplate);
                savedTemplateObj.setDocId(Long.parseLong(docId));
                savedTemplateObj.setDocState(docState);
                templateRepository.save(savedTemplateObj);
                pdfConverter.convertTOPDF(convertedTemplate, docId);
                viewTemplate(docId, docState);
                template = savedTemplateObj;
                log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate");
                return new ResponseEntity<>(new ResponseDomain(true, "" + template.getTemplate()), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate :::: Error :::: "+e);
                return new ResponseEntity<>(new ResponseDomain(false, "" + e), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            template = savedTemplate.get(0);
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplate");
            return new ResponseEntity<>(new ResponseDomain(true, "" + template.getTemplate()), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<?> viewTemplateHtmlForPurchaseOrder(String docId, String docState) {
        log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: viewTemplateHtmlForPurchaseOrder");
        List<SavedTemplate> savedTemplate = templateRepository.findByDocId(Long.parseLong(docId));
        SavedTemplate template = null;
        if (savedTemplate.isEmpty()) {
            try {
                String path = System.getProperty("user.dir");
                path = path + "\\src\\main\\resources\\GenericTemplate.html";
                log.info("path is----->" + path);
                String GenericTemplate = new String(Files.readAllBytes(Paths.get
                        (path)));

              /*  Resource resource = resourceLoader.getResource("classpath:GenericTemplate.html");
                String GenericTemplate = new String(Files.readAllBytes(Paths.get
                        (resource.getURL().toString().substring(resource.getURL().toString().indexOf("/")+1))));*/
                String convertedTemplate = pdfConverter.generateHTML(GenericTemplate, generatePOJOForPurchaseOrder(docId), docId);
                SavedTemplate savedTemplateObj = new SavedTemplate();
                savedTemplateObj.setTemplate(convertedTemplate);
                savedTemplateObj.setDocId(Long.parseLong(docId));
                savedTemplateObj.setDocState(docState);
                templateRepository.save(savedTemplateObj);
                pdfConverter.convertTOPDF(convertedTemplate, docId);
                viewTemplate(docId, docState);
                template = savedTemplateObj;
                return new ResponseEntity<>(new ResponseDomain(true, "" + template.getTemplate()), HttpStatus.OK);

            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(new ResponseDomain(false, "" + e), HttpStatus.INTERNAL_SERVER_ERROR);

            }
        } else {
            template = savedTemplate.get(0);
            return new ResponseEntity<>(new ResponseDomain(true, "" + template.getTemplate()), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<?> mailDataForPurchaseOrder(String account, String authKey) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailDataForPurchaseOrder");
        String token = authKey.replace(JwtConstants.TOKEN_PREFIX, "");
        return templateCreator.defaultSupplierMail(jwtTokenUtil.getUsernameFromToken(token), account);
    }

    /**
     *
     * DEFAULT EMAIL DATA(SALES ORDER)
     *
     *
     **/
    @Override
    public ResponseEntity<?> mailData(String account, String authKey, String docState, String orderNum) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailData");
        String token = authKey.replace(JwtConstants.TOKEN_PREFIX, "");
        return templateCreator.defaultMail(jwtTokenUtil.getUsernameFromToken(token), account,docState,orderNum);
    }

    @Override
    public ResponseEntity<?> mailSenderForPurchaseOrder(MailSender sender, String docId) {
        String pdfDirectory = System.getProperty("user.dir") + "\\generatedPDF\\";
        if (!new File(pdfDirectory + docId + ".pdf").exists()) {
            String docState = templateCreator.docState(docId);
            viewTemplateHtmlForPurchaseOrder(docId, docState);
        }
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(mimeMessage, "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(sender.getTo());
            //helper.setFrom(sender.getFrom());
            helper.setFrom("omendra.singh44@gmail.com");
            helper.setSubject(sender.getSubject());
            if (sender.getBcc() != null && sender.getBcc().length > 0) {
                helper.setBcc(sender.getBcc());
            }
            if (sender.getCc() != null && sender.getCc().length > 0) {
                helper.setCc(sender.getCc());
            }
            helper.setText(sender.getBody());
            FileSystemResource temp = new FileSystemResource(new File(pdfDirectory + docId + ".pdf"));
            helper.setFrom("omendra.singh44@gmail.com");
            helper.addAttachment(sender.getFileName() + ".pdf", temp);
            log.info(helper);
            javaMailSender.send(mimeMessage);
            return new ResponseEntity<>(new ResponseDomain(true, "mail Sent Successfully"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender :::: error occurred :::: " + e);
            return new ResponseEntity<>(new ResponseDomain(false, "" + e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * EMAILING TEMPLATE(SALES ORDER)
     *
     *
     **/
    @Override
    public ResponseEntity<?> mailSender(MailSender sender, String docId) {
        log.info("Entering Service Class :::: TemplateServiceImpl :::: method :::: mailSender");
        String pdfDirectory = System.getProperty("user.dir") + "\\generatedPDF\\";
        if (!new File(pdfDirectory + docId + ".pdf").exists()) {
            String docState = templateCreator.docState(docId);
            viewTemplate(docId, docState);
        }
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(mimeMessage, "text/html");
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(sender.getTo());
            helper.setFrom(sender.getFrom());
            helper.setSubject(sender.getSubject());
            if (sender.getBcc() != null && sender.getBcc().length > 0) {
                helper.setBcc(sender.getBcc());
            }
            if (sender.getCc() != null && sender.getCc().length > 0) {
                helper.setCc(sender.getCc());
            }
            helper.setText(sender.getBody());
            FileSystemResource temp = new FileSystemResource(new File(pdfDirectory + docId + ".pdf"));
            helper.addAttachment(sender.getFileName() + ".pdf", temp);
            javaMailSender.send(mimeMessage);
            log.info("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender");
            return new ResponseEntity<>(new ResponseDomain(true, "mail Sent Successfully"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Service Class :::: TemplateServiceImpl :::: method :::: mailSender :::: Error :::: " + e);
            return new ResponseEntity<>(new ResponseDomain(false, "" + e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

