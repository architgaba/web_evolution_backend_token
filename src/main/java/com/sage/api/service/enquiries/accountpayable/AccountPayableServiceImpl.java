package com.sage.api.service.enquiries.accountpayable;

import com.sage.api.domain.enquiries.Accounts;
import com.sage.api.repository.enquiries.accountpayable.AccountPayableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class AccountPayableServiceImpl implements AccountPayableService {

    private static final Logger log = LogManager.getLogger(AccountPayableServiceImpl.class);
    @Autowired
    private AccountPayableRepository accountPayableRepo;

    /**
     *
     * FETCHING ALL PAYABLE(CUSTOMER) ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllAccounts() {
        log.info("Entering Service Class :::: AccountPayableServiceImpl :::: method :::: fetchingAllAccounts");
        return accountPayableRepo.fetchingAllAccounts();
    }

    /**
     *
     * FETCHING ACCOUNT(CUSTOMER) PAYABLE DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAccountData(Accounts accounts) {
        log.info("Entering Service Class :::: AccountPayableServiceImpl :::: method :::: fetchingAccountData");
        LocalDate startDate = LocalDate.parse(accounts.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(accounts.getToDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return accountPayableRepo.fetchingAccountData(startDate, endDate, accounts);
    }
}
