package com.sage.api.service.enquiries.accountreceivable;

import com.sage.api.domain.enquiries.Accounts;
import org.springframework.http.ResponseEntity;

public interface AccountReceivableService {

    ResponseEntity<?> fetchingAllAccounts();

    ResponseEntity<?> accountReceivableData(Accounts accounts);

 }
