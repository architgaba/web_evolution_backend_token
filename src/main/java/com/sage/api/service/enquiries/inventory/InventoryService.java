package com.sage.api.service.enquiries.inventory;

import com.sage.api.domain.enquiries.Inventory;
import org.springframework.http.ResponseEntity;

public interface InventoryService {

    ResponseEntity<?> fetchingAllInventoryAccounts();

    ResponseEntity<?>  inventoryData(Inventory inventoryData);
 }
