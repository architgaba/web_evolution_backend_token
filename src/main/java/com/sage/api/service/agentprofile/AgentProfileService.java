package com.sage.api.service.agentprofile;

import org.springframework.http.ResponseEntity;

public interface AgentProfileService {

    ResponseEntity<?> uploadingProfileImage(String image,String agentName);

    ResponseEntity<?> fetchingProfileImage(String agentName);
}

