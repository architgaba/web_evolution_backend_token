package com.sage.api.service.salesorder.stockcheck;

import com.sage.api.domain.salesorder.stockcheck.InventoryCheck;
import org.springframework.http.ResponseEntity;

public interface StockCheckService {
    ResponseEntity<?> getInventoryStock();
    ResponseEntity<?> stockCheck(InventoryCheck inventoryCheck);
}
