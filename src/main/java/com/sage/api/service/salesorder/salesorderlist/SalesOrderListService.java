package com.sage.api.service.salesorder.salesorderlist;

import com.sage.api.domain.salesorder.salesorderlist.SalesOrderList;
import org.springframework.http.ResponseEntity;

public interface SalesOrderListService {

    ResponseEntity<?> fetchingSalesOrderListData(SalesOrderList salesOrderList);

    ResponseEntity fetchingAccounts();
}
