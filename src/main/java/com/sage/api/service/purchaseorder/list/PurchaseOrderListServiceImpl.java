package com.sage.api.service.purchaseorder.list;

import com.sage.api.domain.purchaseorder.list.PurchaseOrderList;
import com.sage.api.repository.enquiries.accountpayable.AccountPayableRepository;
import com.sage.api.repository.purchaseorder.list.PurchaseOrderListRepository;
import com.sage.api.repository.salesorder.salesordertransaction.SalesOrderTransactionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderListServiceImpl implements PurchaseOrderListService {
    private static final Logger log = LogManager.getLogger(PurchaseOrderListServiceImpl.class);
    @Autowired
    private AccountPayableRepository accountPayableRepository;
    @Autowired
    private PurchaseOrderListRepository purchaseOrderListRepository;
    @Autowired
    private SalesOrderTransactionRepository salesOrderTransactionRespository;

    /**
     *
     * FETCHING ALL SUPPLIERS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingSuppliers() {
        log.info("Entering service class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingSuppliers");
        ResponseEntity<?> responseEntity = accountPayableRepository.fetchingAllAccounts();
        if (responseEntity != null) {
            log.info("Exiting Service Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingAccounts");
            return responseEntity;
        }
        log.info("Entering Service Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingAccounts");
        return new ResponseEntity<>("No Supplier Found", HttpStatus.OK);
    }

    /**
     *
     * FETCHING PURCHASE ORDER DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingListData(PurchaseOrderList salesOrderList) {
        log.info("Entering Service Class :::: PurchaseOrderListRepositoryImpl :::: method :::: fetchingListData");
        return purchaseOrderListRepository.fetchingPurchaseOrderListData(salesOrderList);
    }
}
