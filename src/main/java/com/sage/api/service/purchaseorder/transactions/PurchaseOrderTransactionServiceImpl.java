package com.sage.api.service.purchaseorder.transactions;

import com.google.gson.Gson;
import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.ExistingPurchaseOrderRO;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.PurchaseOrderRO;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperExistingPuchaseOrder;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.WrapperPurchaseOrder;
import com.sage.api.domain.salesorder.salesordertransaction.TemplateFunctionality;
import com.sage.api.domain.template.MailSender;
import com.sage.api.repository.purchaseorder.transactions.PurchaseOrderTransactionRepository;
import com.sage.api.repository.salesorder.salesordertransaction.SalesOrderTransactionRepository;
import com.sage.api.utils.Error;
import com.sage.api.utils.FreedomServicesConstants;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.ValidationErrors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class PurchaseOrderTransactionServiceImpl implements PurchaseOrderTransactionService {

    private static final Logger log = LogManager.getLogger(PurchaseOrderTransactionServiceImpl.class);
    @Autowired
    private PurchaseOrderTransactionRepository purchaseOrderListRepository;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private SalesOrderTransactionRepository salesOrderTransactionRespository;

    /**
     *
     * ORDER/PLACE PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> placePurchaseOrder(String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        try {
            synchronized (this) {
                ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
                String obj = (String) response.getBody();
                if (!obj.isEmpty()) {
                    String startPoint = obj.substring(1, 17);
                    if (startPoint.contains("true")) {
                        log.info("Error occurred in Creating Purchase Order");
                        Gson g = new Gson();
                        Error apiResponseError = g.fromJson(obj, Error.class);
                        HttpHeaders responseHeaders = new HttpHeaders();
                        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                        String Message = apiResponseError.getMessage();
                        if (!Message.equalsIgnoreCase("Validation Failed")) {
                            log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                            return ResponseDomain.badRequest(""+Message, false);
                        } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                            for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                                Message= Arrays.toString(err.getErrors());
                            }
                            log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                            return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                        }
                        log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                        return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                    } else {
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                        salesOrderTransactionRespository.saveSignature( wrapperPurchaseOrderRO.getSignature(), documentId);
                        List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                        String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                        if (lineNotesArray != null && lineNotesArray.length > 0)
                            salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                        return new ResponseEntity<>(new ResponseDomain(true, "" + purchaseOrderNumber), HttpStatus.OK);
                    }
                } else {
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder");
                    return new ResponseEntity<>(new ResponseDomain(true, "Order Not Placed by Freedom Services"), HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placePurchaseOrder :::: Error :::: "+e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * UPDATE EXISTING PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> placeExistingPurchaseOrder(String authKey, WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingPlaceOrder";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        Integer lineNotesUpdate;
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        log.info(entity.getBody());
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            String obj = (String) response.getBody();
            if (!obj.isEmpty()) {
                String startPoint = obj.substring(1, 17);
                if (startPoint.contains("true")) {
                    log.info("Error occurred in Updating existing Purchase Order");
                    Gson g = new Gson();
                    Error apiResponseError = g.fromJson(obj, Error.class);
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    String Message = apiResponseError.getMessage();
                    if (!Message.equalsIgnoreCase("Validation Failed")) {
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                        return ResponseDomain.badRequest(""+Message, false);
                    } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                        for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                            Message=Arrays.toString(err.getErrors());
                        }
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                        return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                    }
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                    return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                }else {
                    log.info("No error found");
                    Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                    String docId = "" + documentId;
                    salesOrderTransactionRespository.saveSignature(WrapperExistingPuchaseOrder.getSignature(), documentId);
                    List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                    String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                    if (lineNotesArray != null && lineNotesArray.length > 0)
                        lineNotesUpdate = salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                    String docState = purchaseOrderListRepository.docState(docId);
                    if (docState.equals("2")) {
                        String orderNumber = purchaseOrderListRepository.purchaseQuoteNumber(docId, supplierCode);
                        return new ResponseEntity<>(new ResponseDomain(true, "" + orderNumber), HttpStatus.OK);
                    } else {
                        String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                        return new ResponseEntity<>(new ResponseDomain(true, "" + purchaseOrderNumber), HttpStatus.OK);
                    }
                }
            } else {
                log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder");
                return new ResponseEntity<>(new ResponseDomain(true, "Order Not Updated"), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: placeExistingPurchaseOrder :::: Error :::: "+e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * CREATING NEW INVOICE
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderExistingProcessInvoice(String authKey, WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        Integer lineNotesUpdate;
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            String obj = (String) response.getBody();
            if (!obj.isEmpty()) {
                String startPoint = obj.substring(1, 17);
                if (startPoint.contains("true")) {
                    log.info("Error occurred in creating New Invoice");
                    Gson g = new Gson();
                    Error apiResponseError = g.fromJson(obj, Error.class);
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    String Message = apiResponseError.getMessage();
                    if (!Message.equalsIgnoreCase("Validation Failed")) {
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                        return ResponseDomain.badRequest(""+Message, false);
                    } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                        for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                            Message=Arrays.toString(err.getErrors());
                        }
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                        return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                    }
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                    return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                } else {
                    String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                    Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                    salesOrderTransactionRespository.saveSignature(wrapperPurchaseOrderRO.getSignature(), documentId);
                    List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                    String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                    if (lineNotesArray != null && lineNotesArray.length > 0)
                        lineNotesUpdate = salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                    return new ResponseEntity<>(new ResponseDomain(true, "" + purchaseOrderNumber), HttpStatus.OK);
                }
            } else {
                log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice");
                return new ResponseEntity<>(new ResponseDomain(true, "No Invoice Generated"), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderExistingProcessInvoice :::: Error :::: "+e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }
    /**
     *
     * EXISTING PURCHASE ORDER INTO INVOICE
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderProcessInvoice(String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingProcessInvoice";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        Integer lineNotesUpdate;
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            String obj = (String) response.getBody();
            if (!obj.isEmpty()) {
                String startPoint = obj.substring(1, 17);
                if (startPoint.contains("true")) {
                    log.info("Error occurred in Invoice existing Purchase Order");
                    Gson g = new Gson();
                    Error apiResponseError = g.fromJson(obj, Error.class);
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    String Message = apiResponseError.getMessage();
                    if (!Message.equalsIgnoreCase("Validation Failed")) {
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                        return ResponseDomain.badRequest(""+Message, false);
                    } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                        for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                            Message=Arrays.toString(err.getErrors());
                        }
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                        return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                    }
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                    return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                } else {
                    String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                    Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                    String signature = WrapperExistingPuchaseOrder.getSignature();
                    Integer signStatus = salesOrderTransactionRespository.saveSignature(signature, documentId);
                    List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                    String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                    if(lineNotesArray!=null && lineNotesArray.length>0)
                    lineNotesUpdate = salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                    log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                    return new ResponseEntity<>(new ResponseDomain(true, "" + purchaseOrderNumber), HttpStatus.OK);
                }
            } else {
                log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice");
                return new ResponseEntity<>(new ResponseDomain(true, "No Invoice Processed"), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderProcessInvoice :::: Error :::: "+e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * NEW RECEIVE STOCK
     *
     *
     **/
    @Override
    public ResponseEntity<?> existingReceiveStock(String authKey,WrapperPurchaseOrder wrapperPurchaseOrderRO) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderReceiveStock";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = wrapperPurchaseOrderRO.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        PurchaseOrderRO order = wrapperPurchaseOrderRO.getPurchaseOrderRO();
        Integer lineNotesUpdate;
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        log.info(entity.getBody());
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            String obj = (String) response.getBody();
            if (!obj.isEmpty()) {
                String startPoint = obj.substring(1, 17);
                log.info("startPoint::::::::::::::::" + startPoint);
                if (startPoint.contains("true")) {
                    log.info("Error occurred in new receive stock in Purchase Order");
                    Gson g = new Gson();
                    Error apiResponseError = g.fromJson(obj, Error.class);
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    String Message = apiResponseError.getMessage();
                    if (!Message.equalsIgnoreCase("Validation Failed")) {
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                        return ResponseDomain.badRequest(""+Message, false);
                    } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                        for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                            Message=Arrays.toString(err.getErrors());
                        }
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                        return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                    }
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                    return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                }else {
                    String purchaseOrderNumber = purchaseOrderListRepository.purchaseOrderNumber(supplierCode);
                    Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                    String signature = wrapperPurchaseOrderRO.getSignature();
                    Integer signStatus = salesOrderTransactionRespository.saveSignature(signature, documentId);
                    List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);

                    String[] lineNotesArray = wrapperPurchaseOrderRO.getLineNotes();
                    if(lineNotesArray!=null && lineNotesArray.length>0)
                     lineNotesUpdate = salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);

                    return new ResponseEntity<>(new ResponseDomain(true, "" + purchaseOrderNumber), HttpStatus.OK);
                }
            } else {
                log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
                return new ResponseEntity<>(new ResponseDomain(true, "No data Available"), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: existingReceiveStock");
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * EXISTING RECEIVE STOCK IN PURCHASE ORDER
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderReceiveStock(String authKey,WrapperExistingPuchaseOrder WrapperExistingPuchaseOrder) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
        String apiUrl = FreedomServicesConstants.baseUrl + "/PurchaseOrderExistingReceiveStock";
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic");
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Username", jwtTokenUtil.getUsernameFromToken(authKey));
        headers.add("Password", jwtTokenUtil.getPasswordFromToken(authKey));
        String supplierCode = WrapperExistingPuchaseOrder.getPurchaseOrderRO().getOrder().getSupplierAccountCode();
        ExistingPurchaseOrderRO order = WrapperExistingPuchaseOrder.getPurchaseOrderRO();
        HttpEntity<Object> entity = new HttpEntity<>(order, headers);
        log.info(entity.getBody());
        try {
            ResponseEntity<?> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
            String obj = (String) response.getBody();
            if (!obj.isEmpty()) {
                String startPoint = obj.substring(1, 17);
                if (startPoint.contains("true")) {
                    log.info("Error occurred in Existing Receive Stock into Purchase Order");
                    Gson g = new Gson();
                    Error apiResponseError = g.fromJson(obj, Error.class);
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentType(MediaType.APPLICATION_JSON);
                    String Message = apiResponseError.getMessage();
                    if (!Message.equalsIgnoreCase("Validation Failed")) {
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                        return ResponseDomain.badRequest(""+Message, false);
                    } else if (!apiResponseError.getValidationErrors().isEmpty()) {
                        for (ValidationErrors err : apiResponseError.getValidationErrors() ){
                            Message=Arrays.toString(err.getErrors());
                        }
                        log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                        return ResponseDomain.badRequest(""+Message.substring(1,Message.length()-1), false);
                    }
                    log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                    return ResponseDomain.badRequest("Object reference not set to an instance of an objectOrder Not Created.", false);
                }else {

                    Integer documentId = purchaseOrderListRepository.fetchingDocumentId(supplierCode);
                    String grvNumber = purchaseOrderListRepository.grvNumber(documentId,supplierCode);
                    String signature = WrapperExistingPuchaseOrder.getSignature();
                    salesOrderTransactionRespository.saveSignature(signature, documentId);
                    List<Integer> lineList = salesOrderTransactionRespository.fetchLinesDetails(documentId);
                    String[] lineNotesArray = WrapperExistingPuchaseOrder.getLineNotes();
                    if(lineNotesArray!=null && lineNotesArray.length>0)
                    salesOrderTransactionRespository.lineNotesUpdate(lineList, lineNotesArray);
                    return new ResponseEntity<>(new ResponseDomain(true, "" + grvNumber), HttpStatus.OK);
                }
            } else {
                log.info("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
                return new ResponseEntity<>(new ResponseDomain(true, "No data Available"), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderReceiveStock");
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * FETCHING GL DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderGLDropdown() {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderGLDropdown");
        return purchaseOrderListRepository.glAccountDropDown();
    }
    /**
     *
     * TEMPLATE FUNCTIONALITY(EMAIL)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateMailer(MailSender sender, String reference) {
        log.info("Entering Service Class :::: SalesOrderTransactionServiceImpl :::: method :::: templateMailer");
        return purchaseOrderListRepository.mailSender(sender, reference);
    }

    /**
     *
     * TEMPLATE FUNCTIONALITY(DOWNLOAD/PREVIEW)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(String reference, String action) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: templateFunctionality");
        TemplateFunctionality templateFunctionality = new TemplateFunctionality();
        templateFunctionality.setAction(action);
        templateFunctionality.setReference(reference);
        return purchaseOrderListRepository.templateFunctionality(templateFunctionality);
    }

    /**
     *
     * FETCHING STOCK DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> purchaseOrderSTKDropdown() {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: purchaseOrderSTKDropdown");
        return purchaseOrderListRepository.STKDropDown();
    }

    /**
     *
     * FETCHING SUPPLIER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingSupplierDetails(String name, String docId) {
        log.info("Entering Service Class :::: PurchaseOrderTransactionServiceImpl :::: method :::: fetchingSupplierDetails");
        return purchaseOrderListRepository.supplierDetails(name, docId);
    }
}