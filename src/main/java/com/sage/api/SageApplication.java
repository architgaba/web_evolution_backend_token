package com.sage.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
public class SageApplication {//extends SpringBootServletInitializer{
    private static final Logger log = LogManager.getLogger(SageApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SageApplication.class, args);
        log.info(" ------------------------------------------------------");
        log.info("|               SAGE SERVER STARTED                   |");
        log.info(" ------------------------------------------------------");

	}

}
