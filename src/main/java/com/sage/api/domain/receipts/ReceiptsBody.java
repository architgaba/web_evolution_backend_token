package com.sage.api.domain.receipts;

import com.sage.api.domain.salesorder.salesordertransaction.Receipts;
import lombok.Data;

import java.io.Serializable;

@Data
public class ReceiptsBody implements Serializable{
    private Receipts transac;
    private String payMode[];
    private Float[] amount;
    private String posTendered;
    private String posChange;
}
