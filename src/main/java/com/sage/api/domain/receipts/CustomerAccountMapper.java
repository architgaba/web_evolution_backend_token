package com.sage.api.domain.receipts;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerAccountMapper implements RowMapper<CustomerAccount> {
    @Override
    public CustomerAccount mapRow(ResultSet resultSet, int i) throws SQLException {
        CustomerAccount account = new CustomerAccount();
        account.setAccount(resultSet.getString("Account"));
        account.setBalance(resultSet.getFloat("DCBalance"));
        account.setName(resultSet.getString("Name"));
        return account;
    }
}
