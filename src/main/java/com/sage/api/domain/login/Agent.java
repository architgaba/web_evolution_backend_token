package com.sage.api.domain.login;

import lombok.Data;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

@Data
public class Agent implements Serializable {

    private int idAgents;

    private byte[] _rtblAgents_Checksum;

    private Timestamp _rtblAgents_dCreatedDate;

    private Timestamp _rtblAgents_dModifiedDate;

    private int _rtblAgents_iBranchID;

    private int _rtblAgents_iChangeSetID;

    private int _rtblAgents_iCreatedAgentID;

    private int _rtblAgents_iCreatedBranchID;

    private int _rtblAgents_iModifiedAgentID;

    private int _rtblAgents_iModifiedBranchID;

    private boolean bAgentActive;

    private boolean bAgentIsBuyer;

    private boolean bAgentOutOffice;

    private boolean bApplyAccessProjectsToReports;

    private boolean bApplyAccessRepsToReports;

    private boolean bApplyAPGroupsToEnqRep;

    private boolean bApplyARGroupsToEnqRep;

    private boolean bAutoSpellCheck;

    private boolean bCanAssign;

    private boolean bCanChangeSessionDate;

    private boolean bCanSetOutOfOffice;

    private boolean bCBAgAllVisible;

    private boolean bCBAgNoneVisible;

    private boolean bCBAgOwnVisible;

    private boolean bCBUseGrpDefaults;

    private boolean bExitWarning;

    private boolean bForceThisWarehouse;

    private boolean bIncludeAPNoGroups;

    private boolean bIncludeARNoGroups;

    private boolean bJRAgAllVisible;

    private boolean bJRAgNoneVisible;

    private boolean bJRAgOwnVisible;

    private boolean bJRUseGrpDefaults;

    private boolean bKnowledgeBaseWarning;

    private boolean bLockedOut;

    private boolean bNewIncidentNotification;

    private boolean bPOExclusive;

    private boolean bPOUseGrpDefaults;

    private boolean bPwdCanChange;

    private boolean bPwdChangeEvery;

    private boolean bPwdMustChange;

    private boolean bSupervisorAgent;

    private boolean bSysAccount;

    private boolean bUseBiometric;

    private boolean bUseDefaultTree;

    private String cAccessAPGroupChkLstInd;

    private String cAccessAPGroupIDLst;

    private String cAccessARGroupChkLstInd;

    private String cAccessARGroupIDLst;

    private String cAccessBranchChkLstInd;

    private String cAccessBranchIDLst;

    private String cAccessDocCatChkLstInd;

    private String cAccessDocCatGroupChkLstInd;

    private String cAccessDocCatGroupIDLst;

    private String cAccessDocCatIDLst;

    private String cAccessIncidentTypeChkLstInd;

    private String cAccessIncidentTypeGroupChkLstInd;

    private String cAccessIncidentTypeGroupIDLst;

    private String cAccessIncidentTypeIDLst;

    private String cAccessOtherTxWhChkLstInd;

    private String cAccessOtherTxWhIDList;

    private String cAccessProcessFlowChkLstInd;

    private String cAccessProcessFlowIDLst;

    private String cAccessProjectChkLstInd;

    private String cAccessProjectIDLst;

    private String cAccessPurchaseWhChkLstInd;

    private String cAccessPurchaseWhIDLst;

    private String cAccessRepChkLstInd;

    private String cAccessRepIDLst;

    private String cAccessSalesWhChkLstInd;

    private String cAccessSalesWhIDLst;

    private String cAddressCity;

    private String cAddressCountry;

    private String cAddressPOBox;

    private String cAddressState;

    private String cAddressStreet;

    private String cAddressZip;

    private String cAgentName;

    private String cComments;

    private String cDescription;

    private String cDisplayName;

    private String cEFTOperatorCode;

    private String cEmail;

    private String cFirstName;

    private String cInitials;

    private String cLastName;

    private String cOperatorCode;

    private String cOperatorCodePOS;

    private String cOperatorNewPassword;

    private String cOperatorNewPasswordPOS;

    private String cOperatorPassword;

    private String cOperatorPasswordPOS;

    private String cPassword;

    private String cPwdRemind;

    private String cSagePayPassword;

    private String cSagePayPIN;

    private String cSagePayUserName;

    private String cTelFax;

    private String cTelHome;

    private String cTelMobile;

    private String cTelWork;

    private String cTitle;

    private Object cVisibleBranchesLst;

    private Object cVisibleBranchesLstInd;

    private Object cVisibleWarehousesLst;

    private Object cVisibleWarehousesLstInd;

    private String cWebPage;

    private Time dPwdLastChange;

    private double fDefMax_Disc;

    private double fDefMax_LDisc;

    private int fiscalDeviceId;

    private int fiscalPrinterId;

    private double fPOLimit;

    private int iAgentLoginScreen;

    private int iDefCashAccount;

    private int iDefDocCatID;

    private int iDefIncidentTypeGroupID;

    private int iDefProjectID;

    private int iDefRepID;

    private int iDefTillId;

    private int iDefWhseId;

    private int iDocketInputMode;

    private int idPOSMenuSetup;

    private int iNotifyDueMinutes;

    private int iNotifyEscalateMinutes;

    private int iPOAuthType;

    private int iPOIncidentTypeID;

    private int iPwdChangeDays;

    private double max_Disc;

    private double max_LDisc;

    private String vbBiometric;
}
