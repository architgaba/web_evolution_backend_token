package com.sage.api.domain.login;

import lombok.Data;

import java.io.Serializable;

@Data
public class ForgotPassword implements Serializable{
    private String agentName;
    private String mangerEmail;
    private String agentEmail;
}
