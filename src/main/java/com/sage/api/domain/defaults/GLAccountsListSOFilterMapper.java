package com.sage.api.domain.defaults;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GLAccountsListSOFilterMapper implements RowMapper<GLAccountsListSOFilter> {
    @Override
    public GLAccountsListSOFilter mapRow(ResultSet resultSet, int i) throws SQLException {
        GLAccountsListSOFilter soFilter= new GLAccountsListSOFilter();
        soFilter.setMasterSubAccount(resultSet.getString("Master_Sub_Account"));
        soFilter.setDescription(resultSet.getString("Description"));
        return soFilter;
    }
}
