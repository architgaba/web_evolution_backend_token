package com.sage.api.domain.defaults;

import lombok.Data;

import java.io.Serializable;

@Data
public class GLAccountsListSOFilter implements Serializable{
    private String masterSubAccount;
    private String Description ;
}

