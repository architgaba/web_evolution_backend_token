package com.sage.api.domain.defaults;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class AgentDefaults implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;
    private String agentName;
    private String whCode;
    private boolean whFilter;
    private String project;
    private boolean projectFilter;
    private boolean repFilter;
    private String repCode;
    private String salesOrderAccCode;
    private boolean salesOrderAccFilter;
    private String purchaseOrderAccCode;
    private boolean purchaseOrderAccFilter;
    private String till;
    private boolean tillFilter;
    private String pspmCode;
    private boolean pspmFilter;
    private String[] glAccountsSOList;
    private boolean glAccountsSOListFilter;
    private String[] glAccountsPOList;
    private boolean glAccountsPOListFilter;
    private String pspmDescription;
}

