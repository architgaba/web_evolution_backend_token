package com.sage.api.domain.defaults;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPMMapper implements RowMapper<POSPaymentTransactionCode>{
    @Override
    public POSPaymentTransactionCode mapRow(ResultSet resultSet, int i) throws SQLException {
            POSPaymentTransactionCode paymentTransactionCode = new POSPaymentTransactionCode();
            paymentTransactionCode.setCode(resultSet.getString("Code"));
            paymentTransactionCode.setDescription(resultSet.getString("Description"));
            return paymentTransactionCode;
        }
}
