package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class DefaultMail implements Serializable{

    private String sentTo;

    private String sentFrom;

    private String body;
}
