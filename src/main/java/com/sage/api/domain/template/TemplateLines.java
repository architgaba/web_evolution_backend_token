package com.sage.api.domain.template;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TemplateLines implements Serializable{
    private String code;
    private String itemDescription;
    private String rep;
    private Integer qtyOrdered;
    private Integer qtyProcessed;
    private Integer qtyToInvoice;
    private float exclPrice;
    private float inclPrice;
    private float tax;
    private float discount;
    private String lineNotes;
    private String docState;


    @Override
    public String toString() {
        return "TemplateLines{" +
                "code='" + code + '\'' +
                ", itemDescription='" + itemDescription + '\'' +
                ", rep='" + rep + '\'' +
                ", qtyOrdered=" + qtyOrdered +
                ", qtyProcessed=" + qtyProcessed +
                ", qtyToInvoice=" + qtyToInvoice +
                ", exclPrice=" + exclPrice +
                ", inclPrice=" + inclPrice +
                ", tax=" + tax +
                ", discount=" + discount +
                ", lineNotes='" + lineNotes + '\'' +
                '}';
    }
}
