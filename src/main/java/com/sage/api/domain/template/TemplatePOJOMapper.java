package com.sage.api.domain.template;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TemplatePOJOMapper implements RowMapper<TemplatePOJO> {

    @Override
    public TemplatePOJO mapRow(ResultSet resultSet, int i) throws SQLException {
        TemplatePOJO templatePOJO = new TemplatePOJO();
        templatePOJO.setContactPerson(resultSet.getString("cContact"));
        templatePOJO.setCustomerName(resultSet.getString("name"));
        templatePOJO.setPa1(resultSet.getString("Address1") +" "+ resultSet.getString("Address2"));
        templatePOJO.setPa2(resultSet.getString("Address3")  +" "+ resultSet.getString("Address4"));
        templatePOJO.setPa3(resultSet.getString("Address5"));
        templatePOJO.setPa4(resultSet.getString("Address6"));
        templatePOJO.setPo1(resultSet.getString("PAddress1") + resultSet.getString("PAddress2"));
        templatePOJO.setPo2(resultSet.getString("PAddress3") + resultSet.getString("PAddress4")+resultSet.getString("PAddress5"));
        templatePOJO.setPo3(resultSet.getString("PAddress6"));
        templatePOJO.setEmail1(resultSet.getString("cEMail"));
        templatePOJO.setTelephone(resultSet.getString("cTelephone"));
        templatePOJO.setFax(resultSet.getString("Fax1"));
        templatePOJO.setRepCode(resultSet.getString("RepCode"));
        templatePOJO.setDocState(resultSet.getString("DocStateDescription"));
        templatePOJO.setReference(resultSet.getString("InvNumber"));
        templatePOJO.setOrderNo(resultSet.getString("OrderNum"));
        templatePOJO.setAccount(resultSet.getString("cAccountName"));
        templatePOJO.setDate(resultSet.getDate("OrderDate").toString());
        return templatePOJO;
    }
}
