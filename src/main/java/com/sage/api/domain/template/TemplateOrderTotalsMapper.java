package com.sage.api.domain.template;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TemplateOrderTotalsMapper implements RowMapper<TemplateOrderTotals> {

    @Override
    public TemplateOrderTotals mapRow(ResultSet resultSet, int i) throws SQLException {
        TemplateOrderTotals orderTotals = new TemplateOrderTotals();
        orderTotals.setTax(resultSet.getString("OrdTotTax"));
        orderTotals.setTotalIncl(resultSet.getString("OrdTotIncl"));
        orderTotals.setTotalExc(resultSet.getString("OrdTotExcl"));
        orderTotals.setDiscount(resultSet.getString("OrdDiscAmnt"));
        return orderTotals;
    }
}
