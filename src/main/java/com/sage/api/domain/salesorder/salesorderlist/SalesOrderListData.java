package com.sage.api.domain.salesorder.salesorderlist;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SalesOrderListData implements Serializable{

    private String docState;
    private String reference;
    private String clientAccount;
    private String clientName;
    private String orderNo;
    private String extOrderNo;
    private Date orderDate;
    private Date invDate;
    private Date dueDate;
    private String repCode;
    private String user;
    private String OrderStatus;
    private float amount;
    private Long docId;


}

