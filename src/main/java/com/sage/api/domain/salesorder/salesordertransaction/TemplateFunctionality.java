package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateFunctionality implements Serializable {

    private String reference;

    private String action;

}
