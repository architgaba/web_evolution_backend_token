package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class MoreDocumentDetails implements Serializable{

    private  String repCode;

    private String projectCode;

    private String image;

    private String lotNumber;

    private String serialNumber;

    private String module_ST_OR_GL;
}
