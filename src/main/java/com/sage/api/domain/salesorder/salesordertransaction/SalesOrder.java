package com.sage.api.domain.salesorder.salesordertransaction;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class SalesOrder implements Serializable {

    @JsonProperty("DocumentID")
    private int DocumentID;
    @JsonProperty
    private String CustomerAccountCode;
    @JsonProperty
    private String OrderDate;
    @JsonProperty
    private String InvoiceDate;
    @JsonProperty
    private String ExternalOrderNo;
    @JsonProperty
    private String OrderPriority;
    @JsonProperty
    private String OrderStatus;
    @JsonProperty
    private String ProjectCode;
    @JsonProperty
    private String RepresentativeCode;
    @JsonProperty
    private String SettlementTerm;
    @JsonProperty
    List<com.sage.api.domain.salesorder.salesordertransaction.Lines> Lines;
    @JsonProperty
    List<com.sage.api.domain.salesorder.salesordertransaction.Hash> Hash;
    @JsonProperty
    ArrayList<com.sage.api.domain.salesorder.salesordertransaction.FinancialLines> FinancialLines;

}


