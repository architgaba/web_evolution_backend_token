package com.sage.api.domain.salesorder.salesordertransaction.receipts;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReceiptsData implements Serializable {

    private String orderNum;
    private String amount;
    private String customerCode;
    private String customerName;
}
