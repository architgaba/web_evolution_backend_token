package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class LineNotes implements Serializable {
     private  Integer qty;
     private  String lineNotes;
     private  float fUnitPriceIncl;
}
