package com.sage.api.domain.salesorder.salesorderlist;

import lombok.Data;

import java.io.Serializable;

@Data
public class SalesOrderList implements Serializable {

    private String fromDate;

    private String toDate;

    private String docState;

    private String customerName;

    private String account;

}
