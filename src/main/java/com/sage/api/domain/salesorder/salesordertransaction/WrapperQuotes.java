package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class WrapperQuotes implements Serializable{
    private Quote quotes;
    private String [] linenotes;
    private String signature;
}
