package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

/*
Need TO Delete
*/

@Data
public class StockDetailViewObj implements Serializable{
    private String description;
    private Integer taxCode;
    private String inclusivePrice;
    private String exclusivePrice;
}

