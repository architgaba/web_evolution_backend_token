package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class WrapperSalesorder implements Serializable{
    private SalesOrderRO orders;
    private String [] linenotes;
    private String signature;
}
