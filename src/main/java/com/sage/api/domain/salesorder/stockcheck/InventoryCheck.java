package com.sage.api.domain.salesorder.stockcheck;

import lombok.Data;

import java.io.Serializable;


@Data
public class InventoryCheck implements Serializable{
        private String itemCode;
        private String description;
}
