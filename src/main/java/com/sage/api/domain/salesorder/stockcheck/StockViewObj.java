package com.sage.api.domain.salesorder.stockcheck;


import lombok.Data;

import java.io.Serializable;

@Data
public class StockViewObj implements Serializable{

    private String whcode;
    private String whdescription;
    private String Qty_SO;
    private String Qty_PO;
    private String Qty_Hand;

}
