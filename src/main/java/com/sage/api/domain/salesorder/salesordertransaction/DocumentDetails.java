package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DocumentDetails implements Serializable{

    private Date dueDate;

    private String deliveryMethod;

    private String projectCode;

    private String repCode;

    private String orderStatus;
}
