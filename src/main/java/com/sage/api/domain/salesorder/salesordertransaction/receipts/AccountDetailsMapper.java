package com.sage.api.domain.salesorder.salesordertransaction.receipts;

import com.sage.api.domain.salesorder.salesordertransaction.AccountDetails;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDetailsMapper implements RowMapper<AccountDetails> {
    @Override
    public AccountDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        AccountDetails  accountDetails = new AccountDetails();
        accountDetails.setContactPerson(resultSet.getString("Contact_Person"));
        accountDetails.setAddress1(resultSet.getString("Physical1"));
        accountDetails.setAddress2(resultSet.getString("Physical2"));
        accountDetails.setAddress3(resultSet.getString("Physical3"));
        accountDetails.setAddress4(resultSet.getString("Physical4"));
        accountDetails.setAddress5(resultSet.getString("Physical5"));
        accountDetails.setAddress6(resultSet.getString("PhysicalPC"));
        accountDetails.setPAddress1(resultSet.getString("Post1"));
        accountDetails.setPAddress2(resultSet.getString("Post2"));
        accountDetails.setPAddress3(resultSet.getString("Post3"));
        accountDetails.setPAddress4(resultSet.getString("Post4"));
        accountDetails.setPAddress5(resultSet.getString("Post5"));
        accountDetails.setPAddress6(resultSet.getString("PostPC"));
        accountDetails.setTelephone(resultSet.getString("Telephone"));
        accountDetails.setEmail(resultSet.getString("EMail"));
        return accountDetails;
    }
}
