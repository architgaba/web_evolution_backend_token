package com.sage.api.domain.salesorder.salesorderlist;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SalesOrderViewObj implements Serializable{

    private String docState;
    private String reference;
    private String orderNo;
    private String extOrderNo;
    private Date orderDate;
    private Date invDate;
    private Date dueDate;
    private String repCode;
    private String user;
    private String OrderStatus;
    private String amount;
    private Long docId;
}
