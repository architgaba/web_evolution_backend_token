package com.sage.api.domain.salesorder.salesordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StockDetailsDataMapper implements RowMapper<StockDetailsData> {

    @Override
    public StockDetailsData mapRow(ResultSet resultSet, int i) throws SQLException {
        StockDetailsData stockDetailsData = new StockDetailsData();
        stockDetailsData.setItemCode(resultSet.getString(1));
        stockDetailsData.setItemDescription(resultSet.getString(2));
        stockDetailsData.setWarehouseCode(resultSet.getString(3));
        stockDetailsData.setPriceListName(resultSet.getString(4));
        stockDetailsData.setExclusivePrice(resultSet.getFloat(5));
        stockDetailsData.setInclusivePrice(resultSet.getFloat(6));
        stockDetailsData.setTaxCode(resultSet.getInt(7));
        stockDetailsData.setTaxRate(resultSet.getInt(8));
        return stockDetailsData;
    }
}
