package com.sage.api.domain.salesorder.stockcheck;

import lombok.Data;

import java.io.Serializable;

@Data
public class StockCheck implements Serializable{

    private String whcode;
    private String whdescription;
    private float Qty_SO;
    private float Qty_PO;
    private float Qty_Hand;

}
