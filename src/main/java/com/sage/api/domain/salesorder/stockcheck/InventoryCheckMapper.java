package com.sage.api.domain.salesorder.stockcheck;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryCheckMapper implements RowMapper<InventoryCheck> {


    @Override
    public InventoryCheck mapRow(ResultSet resultSet, int i) throws SQLException {
        InventoryCheck inventoryCheck =new InventoryCheck();
        inventoryCheck.setItemCode(resultSet.getString("Code"));
        inventoryCheck.setDescription(resultSet.getString("Description_1"));
        return inventoryCheck;
    }


}
