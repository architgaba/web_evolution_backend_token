package com.sage.api.domain.salesorder.salesordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class DocDetails implements Serializable {
    private String docId;
    private String docState;
}


