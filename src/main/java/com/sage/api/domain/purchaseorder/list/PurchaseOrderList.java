package com.sage.api.domain.purchaseorder.list;

import lombok.Data;

import java.io.Serializable;

@Data
public class PurchaseOrderList implements Serializable{

    private String fromDate;
    private String toDate;
    private String docState;
    private String customerName;
    private String account;
}
