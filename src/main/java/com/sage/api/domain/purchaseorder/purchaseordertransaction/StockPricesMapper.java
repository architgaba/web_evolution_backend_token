package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StockPricesMapper implements RowMapper<StockPrices> {

    @Override
    public StockPrices mapRow(ResultSet resultSet, int i) throws SQLException {
        StockPrices stockPrices = new StockPrices();
        stockPrices.setStkCode(resultSet.getString("Code"));
        stockPrices.setStkDescription(resultSet.getString("Description_1"));
        stockPrices.setStkCost(resultSet.getFloat("LastGRVCost"));
        return stockPrices;
    }
}
