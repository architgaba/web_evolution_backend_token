package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class PurchaseOrderRO implements Serializable{
    private Order order;
}
