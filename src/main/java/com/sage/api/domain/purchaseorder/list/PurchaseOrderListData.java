package com.sage.api.domain.purchaseorder.list;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PurchaseOrderListData implements Serializable{

    private String docState;
    private String reference;
    private String orderNo;
    private Date orderDate;
    private Date invDate;
    private Date dueDate;
    private String user;
    private String OrderStatus;
    private float amount;
    private Long docId;
}
