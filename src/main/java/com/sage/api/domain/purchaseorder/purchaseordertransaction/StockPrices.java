package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class StockPrices implements Serializable{

    private String stkCode;
    private String stkDescription;
    private float stkCost;
}
