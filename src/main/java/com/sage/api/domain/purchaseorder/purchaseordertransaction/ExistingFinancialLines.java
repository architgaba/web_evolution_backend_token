package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sage.api.domain.salesorder.salesordertransaction.Hash;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ExistingFinancialLines implements Serializable {
    @JsonProperty
    private int LineID;
    @JsonProperty
    private String AccountCode;
    @JsonProperty
    private String TaxCode;
    @JsonProperty
    private String ProjectCode;
    @JsonProperty
    private String RepresentativeCode;
    @JsonProperty
    private float Quantity;
    @JsonProperty
    private float ToProcess;
    @JsonProperty
    private float UnitPrice;
    @JsonProperty
    List<com.sage.api.domain.salesorder.salesordertransaction.Hash> Hash;
}
