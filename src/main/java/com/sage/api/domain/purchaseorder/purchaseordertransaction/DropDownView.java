package com.sage.api.domain.purchaseorder.purchaseordertransaction;

import lombok.Data;

import java.io.Serializable;

@Data
public class DropDownView implements Serializable {

    private String viewValue;
    private String value;
    private String price;
}
