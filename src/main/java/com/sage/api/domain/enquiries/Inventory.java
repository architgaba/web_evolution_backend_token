package com.sage.api.domain.enquiries;


import lombok.Data;

import java.io.Serializable;

@Data
public class Inventory implements Serializable{

    private String Account;
    private String Name;
    private String fromDate;
    private String endDate;
}
