package com.sage.api.domain.enquiries;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDataMapper implements RowMapper<AccountsData> {

    @Override
    public AccountsData mapRow(ResultSet resultSet, int i) throws SQLException {
        AccountsData accountData= new AccountsData();
        accountData.setOutstanding(resultSet.getFloat("Outstanding"));
        accountData.setDate(resultSet.getDate("TxDate"));
        accountData.setTrCode(resultSet.getString("TrCode"));
        accountData.setReference(resultSet.getString("Reference"));
        accountData.setDescription(resultSet.getString("Description"));
        accountData.setExtOrderNo(resultSet.getString("ExtOrderNum"));
        accountData.setAmount(resultSet.getFloat("debit")-resultSet.getFloat("Credit"));
        accountData.setOrderNo(resultSet.getString("Order_No"));
        return accountData;
    }
}
