package com.sage.api.domain.enquiries;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryDataMapper implements RowMapper<InventoryData> {

    @Override
    public InventoryData mapRow(ResultSet resultSet, int i) throws SQLException {
        InventoryData inventoryData = new InventoryData();

        inventoryData.setDate(resultSet.getDate("TxDate"));
        inventoryData.setAccount(resultSet.getString("Account"));
        inventoryData.setQty(resultSet.getFloat("ActualQuantity"));
        inventoryData.setAmount(resultSet.getFloat("ActualValue"));
        inventoryData.setDescription(resultSet.getString("Description"));
        inventoryData.setExtOrderNo(resultSet.getString("ExtOrderNum"));
        inventoryData.setOrderNo(resultSet.getString("Order_No"));
        inventoryData.setReference(resultSet.getString("Reference"));
        inventoryData.setRepCode(resultSet.getString("RepCode"));
        inventoryData.setTxCode(resultSet.getString("TrCode"));
        inventoryData.setWH(resultSet.getString("WarehouseCode"));
        return inventoryData;
    }
}