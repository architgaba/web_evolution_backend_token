package com.sage.api.domain.enquiries;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryMapper implements RowMapper<Inventory> {

    @Override
    public Inventory mapRow(ResultSet resultSet, int i) throws SQLException {
        Inventory inventory =new Inventory();
        inventory.setAccount(resultSet.getString("Code"));
        inventory.setName(resultSet.getString("Description_1"));
        return inventory;
    }
}
