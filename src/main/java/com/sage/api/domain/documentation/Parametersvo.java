package com.sage.api.domain.documentation;


import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class Parametersvo implements Serializable{
    public ArrayList<ResponseTable> getResponseTable() {
        return responseTable;
    }
    public void setResponseTable(ArrayList<ResponseTable> responseTable) {
        this.responseTable = responseTable;
    }
    ArrayList<ResponseTable> responseTable;

}
