package com.sage.api.domain.dashboard;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoggedAgentMapper implements RowMapper<LoggedAgent> {
    @Override
    public LoggedAgent mapRow(ResultSet resultSet, int i) throws SQLException {
        LoggedAgent loggedAgent=new LoggedAgent();
        loggedAgent.setFirstName(resultSet.getString(2));
        loggedAgent.setInitial(resultSet.getString(1));
        loggedAgent.setLastName(resultSet.getString(3));
        loggedAgent.setEmail(resultSet.getString(4));
        return loggedAgent;
    }
}
