package com.sage.api.domain.dashboard;

import lombok.Data;

import java.io.Serializable;

@Data
public class MonthlyCount implements Serializable{
    private String monthName;
    private Float total;
}
