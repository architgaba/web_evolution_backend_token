package com.sage.api.domain.dashboard;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ViewObject implements Serializable {
    private float monthlyCount;
    private float DailyCount;
    private List<DailyCount> dailyGraphData ;
    private List<MonthlyCount> monthlyGraphData;
}
