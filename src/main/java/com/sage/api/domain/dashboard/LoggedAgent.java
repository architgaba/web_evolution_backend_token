package com.sage.api.domain.dashboard;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoggedAgent implements Serializable {
    private String initial;
    private String firstName;
    private String lastName;
    private String email;
}
