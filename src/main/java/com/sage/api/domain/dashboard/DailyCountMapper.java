package com.sage.api.domain.dashboard;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DailyCountMapper implements RowMapper<DailyCount> {

    @Override
    public DailyCount mapRow(ResultSet resultSet, int i) throws SQLException {
        DailyCount total= new DailyCount();
        total.setDay(resultSet.getInt("Day"));
        total.setTotal(resultSet.getFloat("Total"));
        return total;
    }
}
