package com.sage.api.domain.dashboard;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MonthlyCountMapper implements RowMapper<MonthlyCount>{
    @Override
    public MonthlyCount mapRow(ResultSet resultSet, int i) throws SQLException {
        MonthlyCount total= new MonthlyCount();
        total.setMonthName(resultSet.getString("MonthName"));
        total.setTotal(resultSet.getFloat("Total"));
        return total;
    }
}
