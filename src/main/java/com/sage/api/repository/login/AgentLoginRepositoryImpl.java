package com.sage.api.repository.login;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.login.Agent;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
@Transactional
public class AgentLoginRepositoryImpl implements AgentLoginRepository {
    private static final Logger log = LogManager.getLogger(AgentLoginRepositoryImpl.class);
    private ResponseDomain responseDomain;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    /**
     *
     * FETCHING ALL AGENTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingAllAgents() {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingAllAgents");
        JSONArray agentNames = new JSONArray();
        RowMapper<Agent> rowMapper = new BeanPropertyRowMapper(Agent.class);
        try {
            List<Agent> agents = jdbcTemplate.query(SageQueries.Q_FETCHING_ALL_AGENTS, rowMapper);
            agents.forEach(agent1 ->
            {
                agentNames.add(agent1.getCAgentName());
            });
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agents", agentNames);
            log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingAllAgents");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Error Occurred :::: Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingAllAgents :::: Error :::: " + e);
            return ResponseDomain.badRequest("No Agent Available", false);
        }catch (Exception e) {
            log.error("Error Occurred :::: Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingAllAgents :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error", false);
        }
    }

    /**
     *
     * AGENT AUTHENTICATION(LOGIN)
     *
     *
     **/
    @Override
    public ResponseEntity<?> agentAuthentication (String name, String deviceType, String password)   {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
        Integer agentExists = jdbcTemplate.queryForObject(SageQueries.Q_CHECKING_AGENT, new Object[]{name}, Integer.class);
        if (agentExists == 0)
            jdbcTemplate.update(SageQueries.Q_ADDING_AGENT, new Object[]{name});
        if (deviceType.equalsIgnoreCase("desktop")) {
            String status = jdbcTemplate.queryForObject(SageQueries.Q_CHECKING_DESKTOP_DEVICE, new Object[]{name}, String.class);
            if (!status.equals("0")) {
                log.info("Agent Logged in from Other Desktop Device");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return ResponseDomain.badRequest("You have already logged in",false);
            } else {
                Agent user = (Agent) jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT, new Object[]{name}, new BeanPropertyRowMapper(Agent.class));
                final String desktopToken = jwtTokenUtil.generateToken(user, deviceType, password);
                final String hashedToken = hashToken(desktopToken);
                jdbcTemplate.update(SageQueries.Q_UPDATING_DESKTOP_DEVICE, hashedToken, name);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", true);
                jsonObject.put("token", desktopToken);
                jsonObject.put("message", "User logged in successfully");
                log.info("Agent logged in successfully into desktop");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } else if (deviceType.equalsIgnoreCase("android")) {
            String status = jdbcTemplate.queryForObject(SageQueries.Q_CHECKING_ANDROID_DEVICE, new Object[]{name}, String.class);
            if (!status.equals("0")) {
                log.info("Agent logged in from other android device");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return ResponseDomain.badRequest("You have already logged in",false);
            } else {
                Agent user = (Agent) jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT, new Object[]{name}, new BeanPropertyRowMapper(Agent.class));
                final String androidToken = jwtTokenUtil.generateToken(user, deviceType, password);
                final String hashedToken = hashToken(androidToken);
                jdbcTemplate.update(SageQueries.Q_UPDATING_ANDROID_DEVICE, hashedToken, name);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", true);
                jsonObject.put("token", androidToken);
                jsonObject.put("message", "User logged in successfully");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } else if (deviceType.equalsIgnoreCase("ios")) {
            String status =  jdbcTemplate.queryForObject(SageQueries.Q_CHECKING_IOS_DEVICE, new Object[]{name}, String.class);
            if (!status.equals("0")) {
                log.info("Agent logged in from some other ios");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return ResponseDomain.badRequest("You have already logged in",false);
            } else {
                Agent user = (Agent) jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT, new Object[]{name}, new BeanPropertyRowMapper(Agent.class));
                final String iosToken = jwtTokenUtil.generateToken(user, deviceType, password);
                final String hashedToken = hashToken(iosToken);
                jdbcTemplate.update(SageQueries.Q_UPDATING_IOS_DEVICE, hashedToken, name);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", true);
                jsonObject.put("token", iosToken);
                jsonObject.put("message", "User logged in successfully");
                log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        }
        log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: agentAuthentication");
        return ResponseDomain.badRequest("You have already logged in",false);
    }

    /**
     *
     * FETCHING AGENT DETAILS
     *
     *
     **/
    @Override
    public Agent findByUserName(String name) {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: findByUserName");
        try {
            Agent agent = (Agent) jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT, new Object[]{name}, new BeanPropertyRowMapper(Agent.class));
            log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: findByUserName");
            return agent;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: findByUserName :::: Error  :::: " + e);
            return null;
        }
    }

    //Encrypting token before saving
    private static String hashToken(String plainTextPassword) {
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }

    /**
     *
     * FETCHING LICENSE
     *
     *
     **/
    @Override
    public String fetchingLicense() {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingLicense");
        try {
            log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingLicense");
            return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_LICENSE, String.class);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingLicense :::: Error :::: " + e);
            return null;
        }catch (Exception e){
            log.error("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingLicense :::: Error :::: " + e);
            return null;
        }
    }

    /**
     *
     * FETCHING ACTIVE AGENT COUNT
     *
     *
     **/
    @Override
    public Integer activeAgentCount() {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: activeAgentCount");
        try {
            Integer count=jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ACTIVE_AGENT_COUNT, Integer.class);
            if(count==null)
                return 0;
            else
                return count;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: activeAgentCount :::: Error :::: " + e);
            return 0;
        }
    }


    /**
     *
     * FETCHING DB NAME
     *
     *
     **/
    @Override
    public List<String> fetchingDBName(String name) {
        log.info("Entering Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingDBName");
        try{
            List<String> dbName=jdbcTemplate.queryForList(SageQueries.Q_FETCHING_AGENTS_EMAIl_CURRENT_DB,new Object[]{name},String.class);
            log.info("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingDBName");
            return dbName;
        }catch (Exception e){
            log.error("Exiting Repository Class :::: AgentLoginRepositoryImpl :::: method :::: fetchingDBName :::: Error :::: " + e);
            return new ArrayList<>(Arrays.asList("Error"));
        }
    }
}
