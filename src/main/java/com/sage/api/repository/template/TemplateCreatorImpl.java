package com.sage.api.repository.template;

import com.sage.api.domain.salesorder.salesordertransaction.DocDetails;
import com.sage.api.domain.salesorder.salesordertransaction.DocDetailsMapper;
import com.sage.api.domain.template.*;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.List;

@Service
public class TemplateCreatorImpl implements TemplateCreator {

    private static final Logger log =  LogManager.getLogger(TemplateCreatorImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * GENERATING DATA FOR TEMPLATE POJO (SALES ORDER)
     *
     *
     **/
    @Override
    public TemplatePOJO templateData(String docId) {
        log.info("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: templateData");
        try {
            TemplatePOJO customer = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_DETAILS_EXISTING, new Object[]{docId}, new TemplatePOJOMapper());
            log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: templateData");
            return customer;
        } catch (Exception e) {
            log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: templateData ::: Error :::: "+e);
            return null;
        }
    }

    /**
     *
     * GENERATING DATA FOR TEMPLATE POJO (PURCHASE ORDER)
     *
     *
     **/
    @Override
    public TemplatePOJO templateSupplierData(String docId) {
        if (docId != null) {
            TemplatePOJO supplier = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_DETAILS_SUPPLIER_EXISTING, new Object[]{docId}, new TemplatePOJOMapper());
            log.info(supplier);
            return supplier;
        }
        return null;
    }

    /**
     *
     * FETCHING ORDER TOTALS FOR TEMPLATE
     *
     *
     **/
    @Override
    public TemplateOrderTotals templateOrderData(String docState, String docId) {

        if (docState != null && docState.equalsIgnoreCase("Archived")) {

            TemplateOrderTotals orderTotals =  jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_ORDER_TOTAL_INVOICE, new Object[]{docId}, new TemplateInvoiceOrderTotalsMapper());
            return orderTotals;
        } else {
            TemplateOrderTotals orderTotals = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_ORDER_TOTAL, new Object[]{docId}, new TemplateOrderTotalsMapper());
            return orderTotals;
        }
    }

    public String linesItemCode(String description) {
        log.info("getting lines item code");
        try {
            return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_LINES_ITEM_CODE, new Object[]{description}, String.class);
        } catch (EmptyResultDataAccessException e) {
            log.info("getting lines item code For Purchase Order" + e);
            try {
                return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_LINES_GL_CODE, new Object[]{description}, String.class);
            } catch (EmptyResultDataAccessException e1) {
                log.info("getting lines item code for Sales Order" + e);
                return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_TEMPLATE_LINES_GL_CODE_SALES_ORDER, new Object[]{description}, String.class);
            }
        }
    }

    /**
     *
     * FETCHING LINES DATA OF DOCUMENT FOR TEMPLATE
     *
     *
     **/
    @Override
    public List<TemplateLines> templateLinesData(String docId, String repCode, String docState) {
        log.info("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: templateLinesData");
        try {
            List<TemplateLines> templateLines = jdbcTemplate.query(SageQueries.Q_FETCHING_TEMPLATE_LINES, new Object[]{docId}, new TemplateLinesMapper());
            for (TemplateLines templateLine : templateLines) {
                templateLine.setRep(repCode);
                templateLine.setDocState(docState);
                templateLine.setCode(this.linesItemCode(templateLine.getItemDescription()));
                log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: templateLinesData");
            }
            return templateLines;
        } catch (Exception e) {
            log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: templateLinesData");
            return null;
        }
    }

    @Override
    public ResponseEntity<?> defaultSupplierMail(String agentName, String customer) {
        log.info("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultSupplierMail");
        try {
            DefaultMail mail = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_SUPPLIER_MAIL_TO, new Object[]{customer}, new DefaultMailMapper());
            mail.setSentFrom(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MAIL_FROM_DEFAULT, new Object[]{agentName}, String.class));
            StringBuilder builder = new StringBuilder();
            builder.append("Good Day \n\n");
            builder.append("Please find attached Purchase Order.\n\n");
            builder.append("Please do not hesitate to contact me should you require anything further.\n");
            builder.append("Thank you very much for your support.\n\n");
            builder.append("Kind Regards,\n");
            builder.append(agentName);
            builder.append("\nBrilliant Link (Pty) Ltd\n");
            builder.append("011-792 9521");
            mail.setBody(builder.toString());
            log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
            return new ResponseEntity<>(mail, HttpStatus.OK);
        }catch (EmptyResultDataAccessException e) {
            log.error("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Supplier/Admin Details Not Available",false);
        }catch (Exception e){
            log.error("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * DEFAULT EMAIL DATA(SALES ORDER)
     *
     *
     **/
    @Override
    public ResponseEntity<?> defaultMail(String agentName, String customer, String docState, String orderNum) {
        log.info("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
        try {
            DefaultMail mail = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MAIL_TO, new Object[]{customer}, new DefaultMailMapper());
            mail.setSentFrom(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MAIL_FROM_DEFAULT, new Object[]{agentName}, String.class));
            StringBuilder builder = new StringBuilder();
            if (docState != null) {
                if (docState.equalsIgnoreCase("Partially Processed") || docState.equalsIgnoreCase("Unprocessed")) {
                    builder.append("Good Day \n\n");
                    builder.append("Thank you very much for accepting our proposal. \n");
                    builder.append("Your order is being processed and you will receive a tax invoice in due course.\n\n");
                    builder.append("Please do not hesitate to contact me should you require anything further. \n");
                    builder.append("Thank you very much for your support.\n\n");
                    builder.append("Kind Regards,\n");
                    builder.append(agentName);
                    builder.append("\nBrilliant Link (Pty) Ltd\n");
                    builder.append("011-792 9521");
                    mail.setBody(builder.toString());
                    log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                    return new ResponseEntity<>(mail, HttpStatus.OK);
                } else if (docState.equalsIgnoreCase("Quotation") || docState.equalsIgnoreCase("Archived Quotation")) {
                    builder.append("Good Day \n\n");
                    builder.append("Thank you very much for giving us the opportunity to provide you with the attached proposal.\n");
                    builder.append("On acceptance of the proposal kindly sign and return to admin@brilliantlink.co.za.\n\n");
                    builder.append("Please do not hesitate to contact me should you require anything further. \n");
                    builder.append("Thank you very much for your support.\n\n");
                    builder.append("Kind Regards,\n");
                    builder.append(agentName);
                    builder.append("\nBrilliant Link (Pty) Ltd\n");
                    builder.append("011-792 9521");
                    mail.setBody(builder.toString());
                    log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                    return new ResponseEntity<>(mail, HttpStatus.OK);
                } else if (docState.equalsIgnoreCase("Archived")) {
                    builder.append("Good Day \n\n");
                    builder.append("Please find attached tax invoice for your confirmed order.\n");
                    builder.append("Kindly remit payment of the required amount indicated on attached invoice in order for us to proceed with the necessary arrangements.\n\n");
                    builder.append("Thank you very much for your support and have a Brilliant day.\n\n");
                    builder.append("Kind Regards,\n");
                    builder.append(agentName);
                    builder.append("\nBrilliant Link (Pty) Ltd\n");
                    builder.append("011-792 9521");
                    mail.setBody(builder.toString());
                    log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                    return new ResponseEntity<>(mail, HttpStatus.OK);
                } else {
                    log.error("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: Unknown Doc State");
                    return ResponseDomain.badRequest("Unknown Doc State", false);
                }
            } else {
                try {
                    DocDetails details1 = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_INV, new Object[]{orderNum}, new DocDetailsMapper());
                    if (details1.getDocState().equalsIgnoreCase("Partially Processed") || details1.getDocState().equalsIgnoreCase("Unprocessed")) {
                        builder.append("Good Day \n\n");
                        builder.append("Thank you very much for accepting our proposal. \n");
                        builder.append("Your order is being processed and you will receive a tax invoice in due course.\n\n");
                        builder.append("Please do not hesitate to contact me should you require anything further. \n");
                        builder.append("Thank you very much for your support.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else if (details1.getDocState().equalsIgnoreCase("Quotation") || details1.getDocState().equalsIgnoreCase("Archived Quotation")) {
                        builder.append("Good Day \n\n");
                        builder.append("Thank you very much for giving us the opportunity to provide you with the attached proposal.\n");
                        builder.append("On acceptance of the proposal kindly sign and return to admin@brilliantlink.co.za.\n\n");
                        builder.append("Please do not hesitate to contact me should you require anything further. \n");
                        builder.append("Thank you very much for your support.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else if (details1.getDocState().equalsIgnoreCase("Archived")) {
                        builder.append("Good Day \n\n");
                        builder.append("Please find attached tax invoice for your confirmed order.\n");
                        builder.append("Kindly remit payment of the required amount indicated on attached invoice in order for us to proceed with the necessary arrangements.\n\n");
                        builder.append("Thank you very much for your support and have a Brilliant day.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else {
                        log.error("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: Unknown Doc State");
                        return ResponseDomain.badRequest("Unknown Doc State", false);
                    }
                } catch (EmptyResultDataAccessException e) {
                    DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_ORD, new Object[]{orderNum}, new DocDetailsMapper());
                    if (details.getDocState().equalsIgnoreCase("Partially Processed") || details.getDocState().equalsIgnoreCase("Unprocessed")) {
                        builder.append("Good Day \n\n");
                        builder.append("Thank you very much for accepting our proposal. \n");
                        builder.append("Your order is being processed and you will receive a tax invoice in due course.\n\n");
                        builder.append("Please do not hesitate to contact me should you require anything further. \n");
                        builder.append("Thank you very much for your support.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else if (details.getDocState().equalsIgnoreCase("Quotation") || details.getDocState().equalsIgnoreCase("Archived Quotation")) {
                        builder.append("Good Day \n\n");
                        builder.append("Thank you very much for giving us the opportunity to provide you with the attached proposal.\n");
                        builder.append("On acceptance of the proposal kindly sign and return to admin@brilliantlink.co.za.\n\n");
                        builder.append("Please do not hesitate to contact me should you require anything further. \n");
                        builder.append("Thank you very much for your support.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else if (details.getDocState().equalsIgnoreCase("Archived")) {
                        builder.append("Good Day \n\n");
                        builder.append("Please find attached tax invoice for your confirmed order.\n");
                        builder.append("Kindly remit payment of the required amount indicated on attached invoice in order for us to proceed with the necessary arrangements.\n\n");
                        builder.append("Thank you very much for your support and have a Brilliant day.\n\n");
                        builder.append("Kind Regards,\n");
                        builder.append(agentName);
                        builder.append("\nBrilliant Link (Pty) Ltd\n");
                        builder.append("011-792 9521");
                        mail.setBody(builder.toString());
                        log.info("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail");
                        return new ResponseEntity<>(mail, HttpStatus.OK);
                    } else {
                        log.error("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: Unknown Doc State");
                        return ResponseDomain.badRequest("Unknown Doc State", false);
                    }
                } catch (Exception e) {
                    log.error("Exiting Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: " + e);
                    return ResponseDomain.internalServerError("Internal Server Error"+e, false);
                }
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Error Fetching Client/Admin Details",false);
        }catch (Exception e){
            log.error("Entering Repository Class :::: TemplateCreatorImpl :::: method :::: defaultMail :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    @Override
    public String docState(String docId) {
        log.info("Entering Repository Class :::: TemplateCreatorImp :::: method :::: docState");
        Long id=Long.parseLong(docId);
        String docState= jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_STATE_DESCRIPTION, new Object[]{id}, String.class);
        return docState;
    }
}
