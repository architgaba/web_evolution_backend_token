package com.sage.api.repository.salesorder.salesordertransaction;

import com.sage.api.domain.defaults.GLAccountsListSOFilter;
import com.sage.api.domain.defaults.GLAccountsListSOFilterMapper;
import com.sage.api.domain.enquiries.*;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.DropDownView;
import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.domain.salesorder.salesordertransaction.receipts.AccountDetailsMapper;
import com.sage.api.domain.salesorder.salesordertransaction.receipts.ReceiptsData;
import com.sage.api.domain.salesorder.salesordertransaction.receipts.ReceiptsDataMapper;
import com.sage.api.domain.template.MailSender;
import com.sage.api.service.template.TemplateService;
import com.sage.api.service.template.TemplateServiceImpl;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.Types;
import java.util.*;
import java.util.stream.Stream;

@Repository
public class SalesOrderTransactionRepositoryImpl implements SalesOrderTransactionRepository {
    private static final Logger log = LogManager.getLogger(SalesOrderTransactionRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private TemplateServiceImpl templateServiceimpl;

    /**
     *
     *FETCHING WAREHOUSE LIST
     *
     *
     **/
    @Override
    public ResponseEntity fetchWarehouse() {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchWarehouse");
        try {
            List<WareHouseListData> getWarehouseList = jdbcTemplate.query(SageQueries.Q_FETCHING_WAREHOUSE_LIST, new WareHouseListDataMapper());
            JSONArray name = new JSONArray();
            getWarehouseList.forEach(getWarehouseList1 -> {
                name.add(getWarehouseList1.getName());
            });
            JSONObject obj = new JSONObject();
            obj.put("warehouse", name);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchWarehouse");
            return new ResponseEntity<>(obj, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchWarehouse :::: Error :::: " + e);
            return ResponseDomain.badRequest("No warehouse exists"+e, false);

        }
    }

    /**
     *
     * FETCHING ITEM CODE AND DESCRIPTION LIST OF STOCK
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingItemCode() {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingItemCode");
        JSONObject jsonObject = new JSONObject();
        List<ViewObj> accountData = new ArrayList<>();
        List<ViewObj> accounts = new ArrayList<>();
        try {
            List<Inventory> inventories = jdbcTemplate.query(SageQueries.Q_FETCHING_INVENTORY_ACCOUNT, new InventoryMapper());
            inventories.forEach(inventory1 -> {
                ViewObj jsonMapClass = new ViewObj();
                ViewObj jsonMapClass1 = new ViewObj();
                jsonMapClass.setValue(inventory1.getAccount());
                jsonMapClass.setViewValue(inventory1.getName());
                accountData.add(jsonMapClass);
                jsonMapClass1.setValue(inventory1.getName());
                jsonMapClass1.setViewValue(inventory1.getAccount());
                accounts.add(jsonMapClass1);
            });
            jsonObject.put("accounts", accountData);
            jsonObject.put("names", accounts);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingItemCode");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (Exception e) {
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingItemCode :::: Error :::: " + e);
            return ResponseDomain.internalServerError("No Stock Exists / "+e,false);

        }
    }

    /**
     *
     * FETCHING ALL CUSTOMER ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingCustomerNames() {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerNames");
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            List<Accounts> accounts = jdbcTemplate.query(SageQueries.Q_FETCHING_ALL_ACCOUNTS_RECEIVABLE, new AccountsMapper());
            if (accounts.isEmpty()) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerNames");
                return new ResponseEntity<>(new ResponseDomain(true, "No Customer Available"), HttpStatus.OK);
            } else {
                accounts.forEach(data ->
                {
                    jsonArray.add(data.getAccount());
                });
                jsonObject.put("customers", jsonArray);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerNames");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerNames :::: Error :::: "+e);
            return ResponseDomain.internalServerError("No Client Exists /"+e,false);
        }
    }

    /**
     *
     * FETCHING CUSTOMER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingCustomerDetails(String name, String docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerDetails");
        JSONObject jsonObject = new JSONObject();
        try {
            if (name != null && docId == null) {
                AccountDetails customer = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_CUSTOMER_DETAILS_NEW, new Object[]{name}, new AccountDetailsMapper());
                jsonObject.put("customer", customer);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerDetails");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else {
                AccountDetails customer = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_CUSTOMER_DETAILS_EXISTING, new Object[]{ Long.parseLong(docId)}, new AccountDetailsExistingMapper());
                jsonObject.put("customer", customer);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerDetails");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingCustomerDetails :::: Error :::: " + e);
            return ResponseDomain.internalServerError(""+e, false);
        }
    }

    /**
     *
     * FETCHING DOCUMENT DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingDocumentDetails(String name, String docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentDetails");
        try {
            if (docId != null) {
                DocumentDetails documentDetails = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCUMENT_DETAILS, new Object[]{docId}, new DocumentDetailsMapper());
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentDetails");
                return new ResponseEntity<>(documentDetails, HttpStatus.OK);
            } else if (name != null) {
                DocumentDetails documentDetails = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_NEW_DOCUMENT_DETAILS, new Object[]{name}, new DocumentDetailsMapper());
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentDetails");
                return new ResponseEntity<>(documentDetails, HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentDetails :::: Error :::: " + e);
            return ResponseDomain.internalServerError("No such document exists /"+e, false);
        }
        return ResponseDomain.badRequest("No document Id found", false);
    }

    /**
     *
     * FETCHING DOCUMENT MORE DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingDocumentMoreDetails(String docID) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentMoreDetails");
        if (docID != null) {
            try {
                MoreDocumentDetails moreDocumentDetails = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MORE_DOCUMENT_DETAILS, new Object[]{docID}, new MoreDocumentDetailsMapper());
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentMoreDetails");
                return new ResponseEntity<>(moreDocumentDetails, HttpStatus.OK);
            } catch (EmptyResultDataAccessException e) {
                log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentMoreDetails :::: Error :::: "+e);
                return ResponseDomain.internalServerError("No such document exists"+e,false);
            }
        }
        return ResponseEntity.badRequest().body("No document Id found");
    }

    /**
     *
     * FETCHING STOCK DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingStockDetails(StockData data) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingStockDetails");
        if (data.getCustomerName() == null || data.getWhCode() == null || data.getStockCode() == null) {
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingStockDetails");
            return ResponseDomain.badRequest("Please select a stock code or description.",false);
        } else {
            try {
                String priceListName = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_PRICE_LIST_NAME, new Object[]{data.getCustomerName()}, String.class);
                StockDetailsData stockDetailsData = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_STOCK_DETAILS, new Object[]{priceListName,data.getStockCode(),data.getWhCode()}, new StockDetailsDataMapper());
                if(stockDetailsData== null)
                    return ResponseDomain.badRequest("No Prices Available For Selected Stock",false);
                else
                    return new ResponseEntity<>(stockDetailsData,HttpStatus.OK);
            } catch (Exception e) {
                log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingStockDetails :::: Error :::: " + e);
                return ResponseDomain.internalServerError("No price List Assigned to Customer",false);
            }
        }
    }

    /**
     *
     * DELETING LINE OF A DOCUMENT
     *
     *
     **/
    @Override
    public ResponseEntity<?> deleteLine(String lineId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: deleteLine");
        try {
            jdbcTemplate.update(SageQueries.Q_FETCHING_DELETE_LINE, lineId);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: deleteLine");
            return new ResponseEntity<>(new ResponseDomain(true, "Deleted Successfully"), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: deleteLine :::: Error :::: " + e);
            return ResponseDomain.internalServerError("No such line exists"+e,false);
        }
    }

    /**
     *
     * FETCHING QTY PROCESSED AND LINE NOTE OF A LINE
     *
     *
     **/
    @Override
    public LineNotes getQtyProcessed(String lineId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: getQtyProcessed");
        Long id = Long.parseLong(lineId);
        try {
            LineNotes lineNotes = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_QTY_PROCESSED, new Object[]{id}, new LioneNotesMapper());
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: getQtyProcessed");
            return lineNotes;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: getQtyProcessed :::: Error :::: " + e);
            return null;
        }
    }

    /**
     *
     * FETCHING INVOICE NUM / ORDER NUM OF DOCUMENT
     *
     *
     **/
    @Override
    public String invoiceNumber(String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber");
        try {
            String invoiceNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_INVOICE_NUM, new Object[]{accountCode}, String.class);
            if (invoiceNum == "" || invoiceNum.equals("") || invoiceNum == null) {
                String orderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM, new Object[]{accountCode}, String.class);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber");
                return orderNum;
            }
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber");
            return invoiceNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber :::: Error :::: " + e);
            return "Error" + e;
        }
    }

    /**
     *
     * FETCHING ORDER NUM OF NEWLY CREATED SALES ORDER
     *
     *
     **/
    @Override
    public String orderNumber(String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: orderNumber");
        try {
            String orderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM, new Object[]{accountCode}, String.class);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: orderNumber");
            return orderNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber" + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING LINE NOTES OF DOCUMENT
     *
     *
     **/
    @Override
    public List<?> fetchLinesDetails(Integer documentId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchLinesDetails");
        try {
            List<String> linesDetails = jdbcTemplate.queryForList(SageQueries.Q_FETCHING_LINES_NOTES, new Object[]{documentId}, String.class);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchLinesDetails");
            return linesDetails;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchLinesDetails :::: Error :::: " + e);
            List<String> stringList = new ArrayList<>();
            stringList.add("" + e);
            return stringList;
        }
    }

    /**
     *
     * SAVING/UPDATING SIGNATURE OF ORDER
     *
     *
     **/
    @Override
    public Integer saveSignature(String signature, Integer documentId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveSignature");
        try {
            int[] var = {Types.VARCHAR, Types.INTEGER};
            int i = jdbcTemplate.update(SageQueries.Q_UPDATING_SIGNATURE, new Object[]{signature, documentId}, var);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveSignature");
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveSignature :::: Error :::: " + e);
            return 0;
        }
    }

    /**
     *
     * FETCHING SIGNATURE OF A ORDER
     *
     *
     **/
    @Override
    public String fetchingsignaturedetails(String docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingsignaturedetails");
        try {
            String signature = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_SIGNATURE, new Object[]{docId}, String.class);
            String value = "";
            if (signature != null)
                value = signature.replaceAll(String.valueOf((char) 0), "");
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: orderNumber");
            return value;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: invoiceNumber :::: Error :::: " + e);
            return "No such order exists " + e;
        }
    }

    /**
     *
     * SAVING/UPDATING LINE NOTES OF DOCUMENT
     *
     *
     **/
    @Override
    public Integer lineNotesUpdate(List<Integer> lineList, String[] lineNotesArray) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: lineNotesUpdate");
        Integer lineNoteUpdateStatus = 0;
        try {
            for (int i = 0; i < lineNotesArray.length; i++) {
                int[] var = {Types.VARCHAR, Types.INTEGER};
                jdbcTemplate.update(SageQueries.Q_UPDATING_LINENOTE, new Object[]{lineNotesArray[i], lineList.get(i)}, var);
                lineNoteUpdateStatus++;
            }
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: lineNotesUpdate");
            return lineNoteUpdateStatus;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: lineNotesUpdate :::: Error :::: " + e);
            return lineNoteUpdateStatus;
        }
    }

    /**
     *
     * FETCHING DOCUMENT ID OF NEWLY CREATED SALES ORDER
     *
     *
     **/
    @Override
    public Integer fetchingDocumentId(String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
        try {
            if (accountCode != null) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AUTO_INDEX, new Object[]{accountCode}, Integer.class);
            } else {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return Integer.parseInt("No such Document exist");
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId :::: Error :::: " + e);
            return Integer.parseInt("" + e);
        }
    }

    /**
     *
     * UPDATING DOC STATE TO ARCHIVED QUOTATION
     *
     *
     **/
    @Override
    public void updateDocState(Integer docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateDocState");
        try {
            if (docId != null)
                jdbcTemplate.update(SageQueries.Q_UPDATING_DOC_STATE_ARCHIVED_QUOTATION, new Object[]{10, docId});
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateDocState");
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateDocState :::: Error :::: " + e);
        }
    }

    /**
     *
     * DOWNLOADING / VIEWING TEMPLATE
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(TemplateFunctionality functionality) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
        try {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_INV, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download")) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return templateService.downloadPdf(details.getDocId(), details.getDocState());
            } else if (functionality.getAction().equalsIgnoreCase("View")) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return templateService.viewTemplate(details.getDocId(), details.getDocState());
            } else {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return new ResponseEntity<>(new ResponseDomain(true, "Undefined Action"), HttpStatus.BAD_REQUEST);
            }
        } catch (EmptyResultDataAccessException e) {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_ORD, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download")) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return templateService.downloadPdf(details.getDocId(), details.getDocState());
            }
            else if (functionality.getAction().equalsIgnoreCase("View")) {
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return templateService.viewTemplate(details.getDocId(), details.getDocState());
            }
            else{
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
                return ResponseDomain.badRequest("Undefined Action", false);
               }
        }
    }

    /**
     *
     * MAILING TEMPLATE
     *
     *
     **/
    @Override
    public ResponseEntity<?> mailSender(MailSender mailSender, String reference) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: mailSender");
        String docId = "";
        try {
            docId = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_ORDER_BASIS, new Object[]{reference}, String.class);
            if (docId == null)
                docId = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_INV_BASIS, new Object[]{reference}, String.class);
            log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: mailSender");
            return templateService.mailSender(mailSender, docId);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: mailSender :::: Error :::: "+e);
            return ResponseDomain.badRequest("Bad Request No Doc ID Found related to that Document", false);
        }
    }


    /**
     *
     * UPDATING AGENT ID AGAINST NEWLY CREATED ORDER
     *
     **/
    @Override
    public void updateAgentIDAgainstOrder(Integer docID, Integer agentID) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateAgentIDAgainstOrder");
        try {
            jdbcTemplate.update(SageQueries.Q_UPDATING_AGENT_ID_AGAINST_ORDER, new Object[]{agentID, docID});
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateAgentIDAgainstOrder");
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updateAgentIDAgainstOrder :::: Error :::: " + e);
        }
    }

    /**
     *
     * FETCHING RECEIPTS DEFAULT DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> receiptsData(String InvoiceNum) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: receiptsData");
        try {
            ReceiptsData receiptsData = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_RECEIPTS_DATA, new Object[]{InvoiceNum}, new ReceiptsDataMapper());
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: receiptsData");
            return new ResponseEntity<>(receiptsData, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: receiptsData :::" + e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     *FETCHING FINANCIAL LINES DROPDOWN(GL)
     *
     *
     **/
    @Override
    public ResponseEntity<?> glAccountDropDown() {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
        try {
            List<GLAccountsListSOFilter> glFilter = jdbcTemplate.query(SageQueries.Q_FETCHING_GL_ACCOUNTS_IN_LIST_SO_FILTER, new GLAccountsListSOFilterMapper());
            List<DropDownView> code = new ArrayList<>();
            List<DropDownView> description = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();
            glFilter.forEach(data -> {
                DropDownView codeView = new DropDownView();
                DropDownView desc = new DropDownView();
                codeView.setValue(data.getMasterSubAccount());
                codeView.setViewValue(data.getDescription());
                codeView.setPrice("0.00");
                desc.setValue(data.getDescription());
                desc.setViewValue(data.getMasterSubAccount());
                desc.setPrice("0.00");
                code.add(codeView);
                description.add(desc);
            });
            jsonObject.put("itemCode", code);
            jsonObject.put("itemDescription", description);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown :::: Error :::: "+e);
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * FETCHING INVOICE NUMBER OF DOCUMENT
     *
     **/
    @Override
    public String fetchingInvoiceNum(Integer docId, String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingInvoiceNum");
        try {
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingInvoiceNum");
            return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_INVOICE_NUM, new Object[]{docId, accountCode}, String.class);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingInvoiceNum :::: Error :::: " + e);
            return "" + e;
        }
    }

    /**
     *
     * FETCHING INVOICE/QUOTE NUMBER OF  A NEW DOCUMENT
     *
     **/
    @Override
    public String fetchingOrderNumInvoice(String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingOrderNumInvoice");
        try {
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingOrderNumInvoice");
            return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_ORDER_NUM_INVOICE, new Object[]{accountCode}, String.class);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: fetchingOrderNumInvoice :::: Error :::: " + e);
            return "" + e;
        }
    }


    /**
     *
     * FETCHING DOCSTATE OF DOCUMENT
     *
     **/
    @Override
    public String docState(String docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: docState");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_STATE, new Object[]{docId}, String.class);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: docState");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: docState :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING QUOTE NUMBER OF DOCUMENT
     *
     **/
    @Override
    public String quoteNumber(String docId, String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: quoteNumber");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_QUOTE_NUM, new Object[]{docId, accountCode}, String.class);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: quoteNumber");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: quoteNumber :::: Error :::: " + e);
            return "Error" + e;
        }
    }

    /**
     *
     * FETCHING UPDATED ORDER NUMBER OF DOCUMENT
     *
     **/
    @Override
    public String updatedOrderNum(String docId, String accountCode) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updatedOrderNum");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_ORDER_NUM, new Object[]{docId, accountCode}, String.class);
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updatedOrderNum");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: updatedOrderNum :::: Error :::: " + e);
            return "Error" + e;
        }
    }

    /**
     *
     * SAVING SUPPLIER/CLIENT PERSONAL DETAILS
     *
     **/
    @Override
    public ResponseEntity<?> updateCustomFields(String orderNum, String docId, AccountDetails accountDetails) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveCustomFields");
        int i = 0;
        try {
            if (orderNum != null) {
                    i = jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                if (i != 0) {
                    jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                    jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                } else {
                    jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                    jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(),accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                    jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), orderNum});
                }
            } else {
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_INVNUM, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), docId});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_bvInvNumAPFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), docId});
                jdbcTemplate.update(SageQueries.Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_bvInvNumARFull, new Object[]{accountDetails.getContactPerson(), accountDetails.getAddress1(), accountDetails.getAddress2(), accountDetails.getAddress3(), accountDetails.getAddress4(), accountDetails.getAddress5(), accountDetails.getAddress6(), accountDetails.getPAddress1(), accountDetails.getPAddress2(), accountDetails.getPAddress3(), accountDetails.getPAddress4(), accountDetails.getPAddress5(), accountDetails.getPAddress6(), accountDetails.getTelephone(), accountDetails.getEmail(), docId});
            }
            log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveCustomFields");
            return new ResponseEntity(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "CustomFields Saved Successfully", true), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: saveCustomFields :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error in Saving Custom Fields / " + e, false);
        }
    }
}

