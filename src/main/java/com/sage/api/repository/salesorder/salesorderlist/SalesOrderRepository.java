package com.sage.api.repository.salesorder.salesorderlist;

import com.sage.api.domain.salesorder.salesorderlist.SalesOrderList;
import org.springframework.http.ResponseEntity;

public interface SalesOrderRepository {

    ResponseEntity<?> fetchingSalesOrderListData(SalesOrderList salesOrderList);
}
