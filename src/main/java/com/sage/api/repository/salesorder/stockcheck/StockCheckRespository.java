package com.sage.api.repository.salesorder.stockcheck;

import com.sage.api.domain.salesorder.stockcheck.InventoryCheck;
import org.springframework.http.ResponseEntity;

public interface StockCheckRespository {
    ResponseEntity<?> getInventoryStock();
    ResponseEntity<?> stockCheck(InventoryCheck inventoryCheck);
}
