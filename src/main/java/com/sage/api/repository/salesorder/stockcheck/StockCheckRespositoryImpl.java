package com.sage.api.repository.salesorder.stockcheck;

import com.sage.api.domain.enquiries.ViewObj;
import com.sage.api.domain.salesorder.stockcheck.*;
import com.sage.api.repository.enquiries.inventory.InventoryRepositoryImpl;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class StockCheckRespositoryImpl implements StockCheckRespository {

    private static final Logger log = LogManager.getLogger(InventoryRepositoryImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private ResponseDomain responseDomain;

    /**
     *
     * FETCHING INVENTORY STOCK DETAIL
     *
     *
     **/
    @Override
    public ResponseEntity<?> getInventoryStock() {
        log.info("Entering Repository Class :::: StockCheckRespositoryImpl :::: method :::: getInventoryStock");
        List<ViewObj> descriptionView = new ArrayList<>();
        List<ViewObj> itemCodeView = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        List<InventoryCheck> inventoriesCheck = jdbcTemplate.query(SageQueries.Q_FETCHING_INVENTORY_ACCOUNT, new InventoryCheckMapper());
        inventoriesCheck.forEach(data -> {
            ViewObj viewObj = new ViewObj();
            viewObj.setValue(data.getDescription());
            viewObj.setViewValue(data.getItemCode());
            descriptionView.add(viewObj);
            ViewObj viewObj1 = new ViewObj();
            viewObj1.setValue(data.getItemCode());
            viewObj1.setViewValue(data.getDescription());
            itemCodeView.add(viewObj);
        });
        jsonObject.put("description", descriptionView);
        jsonObject.put("itemCode", itemCodeView);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    /**
     *
     * INVENTORY STOCK CHECK
     *
     *
     **/
    @Override
    public ResponseEntity<?> stockCheck(InventoryCheck inventoryCheck) {
        log.info("Entering Repository Class :::: StockCheckRespositoryImpl :::: method :::: stockcheck");
        try {
            Integer accountLink = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_INVENTORY_ACCOUNT_LINK, new Object[]{inventoryCheck.getItemCode(), inventoryCheck.getDescription()}, Integer.class);
            List<StockCheck> inventoryStoreCheck = jdbcTemplate.query(SageQueries.Q_FETCHING_INVENTORY_STORE_CHECK_DATA, new Object[]{accountLink}, new StockCheckMapper());
            List<StockViewObj> stockViewObjs = new ArrayList<>();
            if (!inventoryStoreCheck.isEmpty()) {
                inventoryStoreCheck.forEach(data ->
                {
                    StockViewObj viewObj = new StockViewObj();
                    viewObj.setWhcode(data.getWhcode());
                    viewObj.setWhdescription(data.getWhdescription());
                    viewObj.setQty_Hand(String.format("%.2f", data.getQty_Hand()));
                    viewObj.setQty_SO(String.format("%.2f", data.getQty_SO()));
                    viewObj.setQty_PO(String.format("%.2f", data.getQty_PO()));
                    stockViewObjs.add(viewObj);
                });
                log.info("Exiting Repository Class :::: StockCheckRespositoryImpl :::: method :::: stockcheck");
                return new ResponseEntity<>(stockViewObjs, HttpStatus.OK);
            } else {
                log.info("Exiting Repository Class :::: StockCheckRespositoryImpl :::: method :::: stockcheck :::: no data found");
                return new ResponseEntity<>(new ResponseDomain(true, "No Data Available"), HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Error Occurred in Fetching Account Link :::: No Such account exists :::: " + e);
            log.info("Exiting Repository Class :::: StockCheckRespositoryImpl :::: method :::: stockCheck ");
            return new ResponseEntity<>(new ResponseDomain(false, "Invalid Combination"), HttpStatus.BAD_REQUEST);
        }
    }
}
