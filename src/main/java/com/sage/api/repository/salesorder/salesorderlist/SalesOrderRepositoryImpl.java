package com.sage.api.repository.salesorder.salesorderlist;

import com.sage.api.domain.salesorder.salesorderlist.SalesOrderList;
import com.sage.api.domain.salesorder.salesorderlist.SalesOrderListData;
import com.sage.api.domain.salesorder.salesorderlist.SalesOrderListDataMapper;
import com.sage.api.domain.salesorder.salesorderlist.SalesOrderViewObj;
import com.sage.api.service.salesorder.salesorderlist.SalesOrderListServiceImpl;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SalesOrderRepositoryImpl implements SalesOrderRepository {

    private static final Logger log = LogManager.getLogger(SalesOrderListServiceImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * FETCHING SALES ORDER DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingSalesOrderListData(SalesOrderList salesOrderList) {
        log.info("Entering Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData");
        LocalDate startDate = LocalDate.parse(salesOrderList.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(salesOrderList.getToDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String docState = salesOrderList.getDocState();
        float total = 0.0f;
        try {
            Integer account_link = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DCLINK_CLIENT, new Object[]{salesOrderList.getCustomerName(), salesOrderList.getAccount()}, Integer.class);
            if (docState.equalsIgnoreCase("All")) {
                List<SalesOrderListData> salesOrderListData = jdbcTemplate.query(SageQueries.Q_FETCHING_SALES_ORDER_LIST_DATA, new Object[]{account_link, startDate, endDate}, new SalesOrderListDataMapper());
                if (salesOrderListData.isEmpty()) {
                    log.info("Exiting Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData ");
                    return new ResponseEntity<>(new ResponseDomain(true, "No data Available"), HttpStatus.OK);
                } else {
                    List<SalesOrderViewObj> viewObjList = new ArrayList<>();
                    for (SalesOrderListData data : salesOrderListData) {
                        SalesOrderViewObj orderViewObj = new SalesOrderViewObj();
                        orderViewObj.setDocState(data.getDocState());
                        orderViewObj.setReference(data.getReference());
                        orderViewObj.setRepCode(data.getRepCode());
                        orderViewObj.setOrderNo(data.getOrderNo());
                        orderViewObj.setExtOrderNo(data.getExtOrderNo());
                        orderViewObj.setInvDate(data.getInvDate());
                        orderViewObj.setDueDate(data.getDueDate());
                        orderViewObj.setOrderDate(data.getOrderDate());
                        orderViewObj.setUser(data.getUser());
                        orderViewObj.setOrderStatus(data.getOrderStatus());
                        orderViewObj.setAmount(String.format("%.2f", data.getAmount()));
                        orderViewObj.setDocId(data.getDocId());
                        viewObjList.add(orderViewObj);
                        total = total + data.getAmount();
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("totalAmount", String.format("%.2f", total));
                    jsonObject.put("salesOrderListData", viewObjList);
                    log.info("Exiting Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData ");
                    return new ResponseEntity<>(jsonObject, HttpStatus.OK);
                }

            } else if (docState.equalsIgnoreCase("Archived") || docState.equalsIgnoreCase("Quotation") || docState.equalsIgnoreCase("Partially Processed") || docState.equalsIgnoreCase("Unprocessed") || docState.equalsIgnoreCase("Archived Quotation")) {
                List<SalesOrderListData> salesOrderListData = jdbcTemplate.query(SageQueries.Q_FETCHING_SALES_ORDER_LIST_DATA_ON_DOCSTATE, new Object[]{account_link, docState, startDate, endDate}, new SalesOrderListDataMapper());
                if (salesOrderListData.isEmpty()) {
                    log.info("Exiting Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData ");
                    return new ResponseEntity<>(new ResponseDomain(true, "No data Available"), HttpStatus.OK);
                } else {
                    List<SalesOrderViewObj> viewObjList = new ArrayList<>();
                    for (SalesOrderListData data : salesOrderListData) {
                        SalesOrderViewObj orderViewObj = new SalesOrderViewObj();
                        orderViewObj.setDocState(data.getDocState());
                        orderViewObj.setReference(data.getReference());
                        orderViewObj.setRepCode(data.getRepCode());
                        orderViewObj.setOrderNo(data.getOrderNo());
                        orderViewObj.setExtOrderNo(data.getExtOrderNo());
                        orderViewObj.setInvDate(data.getInvDate());
                        orderViewObj.setDueDate(data.getDueDate());
                        orderViewObj.setOrderDate(data.getOrderDate());
                        orderViewObj.setUser(data.getUser());
                        orderViewObj.setOrderStatus(data.getOrderStatus());
                        orderViewObj.setAmount(String.format("%.2f", data.getAmount()));
                        viewObjList.add(orderViewObj);
                        orderViewObj.setDocId(data.getDocId());
                        total = total + data.getAmount();
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("totalAmount", String.format("%.2f", total));
                    jsonObject.put("salesOrderListData", viewObjList);
                    log.info("Exiting Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData ");
                    return new ResponseEntity<>(jsonObject, HttpStatus.OK);
                }
            } else {
                log.info("Unknown Doc State ");
                return new ResponseEntity<>(new ResponseDomain(false, "Unknown Doc State"), HttpStatus.BAD_REQUEST);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: SalesOrderRepositoryImpl :::: method :::: fetchingSalesOrderListData :::: Error" + e);
            return new ResponseEntity<>(new ResponseDomain(false, "Invalid Combination"), HttpStatus.BAD_REQUEST);
        }
    }
}
