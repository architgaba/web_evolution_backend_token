package com.sage.api.repository.defaults;

import com.sage.api.domain.defaults.*;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.Arrays;
import java.util.Date;

@Repository
public class DefaultsRepositoryImpl implements DefaultsRepository {
    private static final Logger log = LogManager.getLogger(DefaultsRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*
     *
     * FETCHING DEFAULTS DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> defaultsData() {
        log.info("Entering Repository Class :::: DefaultsRepositoryImpl :::: method :::: defaultsData");
        JSONObject jsonObject = new JSONObject();
        DataViewObj viewObj = new DataViewObj();
        try {
            viewObj.setWarehouseCode(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_WAREHOUSE_CODE, String.class));
            viewObj.setRepCode(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_REP_CODE, String.class));
            viewObj.setProjectCode(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_PROJECT_CODE, String.class));
            viewObj.setSalesOrderAcc(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_SALES_ORDER_ACC, String.class));
            viewObj.setPurchaseOrderAcc(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_PURCHASE_ORDER_ACC, String.class));
            viewObj.setTill(jdbcTemplate.queryForList(SageQueries.Q_FETCHING_TILL, String.class));
            viewObj.setPspm(jdbcTemplate.query(SageQueries.Q_FETCHING_PSPM, new PSPMMapper()));
            viewObj.setGlAccountsSOListFilter(jdbcTemplate.query(SageQueries.Q_FETCHING_GL_ACCOUNTS_IN_LIST_SO_FILTER, new GLAccountsListSOFilterMapper()));
            viewObj.setGlAccountsPOListFilter(jdbcTemplate.query(SageQueries.Q_FETCHING_GL_ACCOUNTS_IN_LIST_PO_FILTER, new GLAccountsListPOFilterMapper()));
            jsonObject.put("defaults", viewObj);
            log.info("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: defaultsData");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: defaultsData :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Defaults Not Available / Exists", false);
        }
    }

    /*
     *
     * SAVING/UPDATING AGENTS DASHBOARD DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> savingDefaultsInfo(AgentDefaults defaults) {
        log.info("Entering Repository Class :::: DefaultsRepositoryImpl :::: method :::: savingDefaultsInfo");
        String GlAccountsSOList = "",soList = "",glAccountsPOList = "",poList = "";
        try {
            if (defaults.getGlAccountsSOList() != null) {
                GlAccountsSOList = Arrays.toString(defaults.getGlAccountsSOList());
                soList = GlAccountsSOList.substring(1, GlAccountsSOList.length() - 1);
            }
            if (defaults.getGlAccountsPOList() != null) {
                glAccountsPOList = Arrays.toString(defaults.getGlAccountsPOList());
                poList = glAccountsPOList.substring(1, glAccountsPOList.length() - 1);
            }
            Long i = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENTS_DEFAULTS_ID, new Object[]{defaults.getAgentName()}, Long.class);
            jdbcTemplate.update(SageQueries.Q_UPDATING_AGENT_DEFAULT_DETAILS, new Object[]{defaults.getAgentName(), defaults.getWhCode(), defaults.isWhFilter(), defaults.getRepCode(), defaults.isRepFilter(), defaults.getProject(), defaults.isProjectFilter(),
                    defaults.getSalesOrderAccCode(), defaults.isSalesOrderAccFilter(), defaults.getTill(), defaults.isTillFilter(), defaults.getPurchaseOrderAccCode(), defaults.isPurchaseOrderAccFilter(), defaults.getPspmCode(), defaults.isPspmFilter(),
                    soList, defaults.isGlAccountsSOListFilter(), poList, defaults.isGlAccountsPOListFilter(), i});
            log.info("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: savingDefaultsInfo");
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"Defaults Updated Successfully",true),HttpStatus.OK);

        } catch (EmptyResultDataAccessException e) {
            log.info("Entering Catch Block :::: DefaultsRepositoryImpl :::: method :::: savingDefaultsInfo");
            jdbcTemplate.update(SageQueries.Q_SAVING_AGENT_DEFAULT_DETAILS, new Object[]{defaults.getAgentName(), defaults.getWhCode(), defaults.isWhFilter(), defaults.getRepCode(), defaults.isRepFilter(), defaults.getProject(), defaults.isProjectFilter()
                    , defaults.getPurchaseOrderAccCode(), defaults.isSalesOrderAccFilter(), defaults.getTill(), defaults.isTillFilter(), defaults.getPurchaseOrderAccCode(), defaults.isPurchaseOrderAccFilter(), defaults.getPspmCode(), defaults.isPspmFilter(),
                    soList, defaults.isGlAccountsSOListFilter(), poList, defaults.isGlAccountsPOListFilter()});
            log.info("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: savingDefaultsInfo");
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"Defaults Saved Successfully",true),HttpStatus.OK);
        }catch (Exception e){
            log.error("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: savingDefaultsInfo :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /*
     *
     * RETRIEVING AGENTS DEFAULTS
     *
     *
     */
    @Override
    public ResponseEntity<?> fetchingAgentDefaults(String defaults) {
        log.info("Entering Repository Class :::: DefaultsRepositoryImpl :::: method :::: fetchingAgentDefaults");
        try {
            AgentDefaults agentDefaults = (AgentDefaults) jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT_DEFAULTS, new Object[]{defaults}, new BeanPropertyRowMapper(AgentDefaults.class));
            String code = agentDefaults.getPspmCode();
            String pspmDescription = "";
            if (agentDefaults.getPspmCode() != null) {
                pspmDescription = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT_DEFAULTS_PSPM_DESC, new Object[]{code}, String.class);
                agentDefaults.setPspmDescription(code + " " + pspmDescription);
            }
            log.info("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: fetchingAgentDefaults");
            return new ResponseEntity<>(agentDefaults, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: DefaultsRepositoryImpl :::: method :::: fetchingAgentDefaults");
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK, new Date(System.currentTimeMillis()), "Agent Defaults Not Exits", true), HttpStatus.OK);
        }
    }
}
