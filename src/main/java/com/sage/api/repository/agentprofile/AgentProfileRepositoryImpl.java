package com.sage.api.repository.agentprofile;

import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;

@Repository
@Transactional
public class AgentProfileRepositoryImpl implements  AgentProfileRepository {
    private static final Logger log = LogManager.getLogger(AgentProfileRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*
     *
     * UPLOADING AGENT PROFILE IMAGE
     *
     *
     */
    @Override
    public ResponseEntity<?> uploadingAgentProfileImage(String image,String agentName) {
        log.info("Entering Repository Class :::: AgentProfileRepositoryImpl :::: method :::: uploadingAgentProfileImage :::: Agent :::: "+agentName);
        try {
            jdbcTemplate.update(SageQueries.Q_UPLOADING_AGENT_PROFILE_PIC, new Object[]{image,agentName});
            log.info("Exiting Repository Class :::: AgentProfileRepositoryImpl :::: method :::: uploadingAgentProfileImage");
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"Profile Image Uploaded Successfully",true),HttpStatus.OK);
        }catch (Exception e){
            log.error("Exiting Repository Class :::: AgentProfileRepositoryImpl :::: method :::: uploadingAgentProfileImage :::: Error :::: "+e);
            return ResponseDomain.internalServerError("Server Error in Uploading Image",false);
        }
    }

    /*
     *
     *FETCHING AGENT PROFILE IMAGE
     *
     *
     */
    @Override
    public ResponseEntity<?> fetchingAgentProfileImage(String agentName) {
        log.info("Entering Repository Class :::: AgentProfileRepositoryImpl :::: method :::: fetchingAgentProfileImage :::: Agent :::: "+agentName);
        String value = "";
        try {
            String image = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT_PROFILE_PIC, new Object[]{agentName}, String.class);
            if (image != null)
                value = image.replaceAll(String.valueOf((char) 0), "");
                value=value.substring(14,value.length()-4);
            log.info("Exiting Repository Class :::: AgentProfileRepositoryImpl :::: method :::: fetchingAgentProfileImage");
            return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),""+value,true),HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: AgentProfileRepositoryImpl :::: Error :::: " + e);
            return new ResponseEntity<>("false",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
