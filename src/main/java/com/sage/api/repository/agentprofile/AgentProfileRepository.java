package com.sage.api.repository.agentprofile;

import org.springframework.http.ResponseEntity;

public interface AgentProfileRepository {

     ResponseEntity<?> uploadingAgentProfileImage(String image,String agentName);

     ResponseEntity<?> fetchingAgentProfileImage(String agentName);
}
