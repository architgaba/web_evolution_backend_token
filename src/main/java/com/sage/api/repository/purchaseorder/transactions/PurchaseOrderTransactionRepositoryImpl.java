package com.sage.api.repository.purchaseorder.transactions;

import com.sage.api.domain.defaults.GLAccountsListPOFilter;
import com.sage.api.domain.defaults.GLAccountsListPOFilterMapper;
import com.sage.api.domain.enquiries.ViewObj;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.DropDownView;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.StockPrices;
import com.sage.api.domain.purchaseorder.purchaseordertransaction.StockPricesMapper;
import com.sage.api.domain.salesorder.salesordertransaction.*;
import com.sage.api.domain.salesorder.salesordertransaction.receipts.AccountDetailsMapper;
import com.sage.api.domain.template.MailSender;
import com.sage.api.service.template.TemplateService;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class PurchaseOrderTransactionRepositoryImpl implements PurchaseOrderTransactionRepository {

    private static final Logger log = LogManager.getLogger(PurchaseOrderTransactionRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TemplateService templateService;

    /**
     *
     * FETCHING PURCHASE ORDER NUMBER
     *
     *
     **/
    @Override
    public String purchaseOrderNumber(String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber");
        try {
            String orderNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_PURCHASE_ORDER_NUM, new Object[]{supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber");
            return orderNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseOrderNumber :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING GRV NUMBER
     *
     *
     **/
    @Override
    public String grvNumber(Integer docId, String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber");
        try {
            String grvNum = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_GRV_NUM, new Object[]{docId,supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber");
            return grvNum;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: grvNumber :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING DOCUMENT ID
     *
     *
     **/
    @Override
    public Integer fetchingDocumentId(String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
        try {
            if (supplierCode != null) {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AUTO_INDEX_PURCHASE_ORDER, new Object[]{supplierCode}, Integer.class);
            } else {
                log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId ");
                return Integer.parseInt("No such Document exist");
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: fetchingDocumentId :::: Error :::: " + e);
            return Integer.parseInt("" + e);
        }
    }

    /**
     *
     * FETCHING DOC STATE
     *
     *
     **/
    @Override
    public String docState(String docId) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_STATE, new Object[]{docId}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: docState :::: Error :::: " + e);
            return "Error " + e;
        }
    }

    /**
     *
     * FETCHING QUOTATION NUMBER
     *
     *
     **/
    @Override
    public String purchaseQuoteNumber(String docId, String supplierCode) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber");
        try {
            String docState = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_UPDATE_QUOTE_NUM, new Object[]{docId, supplierCode}, String.class);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber");
            return docState;
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: purchaseQuoteNumber :::: Error :::: " + e);
            return "Error" + e;
        }
    }

    /**
     *
     * FETCHING GL DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> glAccountDropDown() {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
        try {
            List<GLAccountsListPOFilter> glFilter=jdbcTemplate.query(SageQueries.Q_FETCHING_GL_ACCOUNTS_IN_LIST_PO_FILTER, new GLAccountsListPOFilterMapper());
            List<DropDownView> code = new ArrayList<>();
            List<DropDownView> description = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();
            glFilter.forEach(data -> {
                DropDownView codeView = new DropDownView();
                DropDownView desc = new DropDownView();
                codeView.setValue(data.getMasterSubAccount());
                codeView.setViewValue(data.getDescription());
                codeView.setPrice("0.00");
                desc.setValue(data.getDescription());
                desc.setViewValue(data.getMasterSubAccount());
                desc.setPrice("0.00");
                code.add(codeView);
                description.add(desc);
            });
            jsonObject.put("itemCode",code);
            jsonObject.put("itemDescription",description);
            log.info("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
      catch (Exception e)
      {
          log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
          return ResponseDomain.internalServerError(""+e,false);
      }
    }

    /**
     *
     * FETCHING STOCK DROPDOWN LIST
     *
     *
     **/
    @Override
    public ResponseEntity<?> STKDropDown() {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
        JSONObject jsonObject = new JSONObject();
        List<DropDownView> code = new ArrayList<>();
        List<DropDownView> description = new ArrayList<>();;
        try {
            List<StockPrices> stockPrices=jdbcTemplate.query(SageQueries.Q_FETCHING_STOCK_PRICES_PO,new StockPricesMapper());
            stockPrices.forEach(data -> {
                DropDownView codeView = new DropDownView();
                DropDownView descView = new DropDownView();
                codeView.setValue(data.getStkCode());
                codeView.setViewValue(data.getStkDescription());
                codeView.setPrice(String.format("%.2f",data.getStkCost()));
                descView.setValue(data.getStkDescription());
                descView.setViewValue(data.getStkCode());
                descView.setPrice(String.format("%.2f",data.getStkCost()));
                code.add(codeView);
                description.add(descView);
            });
            jsonObject.put("itemCode",code);
            jsonObject.put("itemDescription",description);
            log.info("Exiting Repository Class ::::  PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
        catch (Exception e)
        {
            log.error("Exiting Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: glAccountDropDown");
            return ResponseDomain.internalServerError(""+e,false);
        }
    }

    /**
     *
     * FETCHING DEFAULT DATA FOR MAIL
     *
     *
     **/
    @Override
    public ResponseEntity<?> mailSender(MailSender mailSender, String reference) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: mailSender");
        String docId="";
        try
        {
            docId= jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_ORDER_BASIS, new Object[]{reference}, String.class);
            if (docId==null)
                docId = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOC_ID_ON_INV_BASIS, new Object[]{reference}, String.class);
            return templateService.mailSenderForPurchaseOrder(mailSender,docId);
        }
        catch (EmptyResultDataAccessException e)
        {
           return ResponseDomain.badRequest("Bad Request No Doc ID Found related to that Document", false);
        }
    }

    /**
     *
     * TEMPLATE FUNCTIONALITY(DOWNLOAD/PREVIEW)
     *
     *
     **/
    @Override
    public ResponseEntity<?> templateFunctionality(TemplateFunctionality functionality) {
        log.info("Entering Repository Class :::: PurchaseOrderTransactionRepositoryImpl :::: method :::: templateFunctionality");
        try {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_INV, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download")) {
                return templateService.downloadPdfForPurchaseOrder(details.getDocId(), details.getDocState());
            } else if (functionality.getAction().equalsIgnoreCase("View")) {
                return templateService.viewTemplateHtmlForPurchaseOrder(details.getDocId(), details.getDocState());
            } else {
                return ResponseDomain.badRequest("Undefined Action", false);
            }
        } catch (EmptyResultDataAccessException e) {
            DocDetails details = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_ORD, new Object[]{functionality.getReference()}, new DocDetailsMapper());
            if (functionality.getAction().equalsIgnoreCase("Download"))
                return templateService.downloadPdfForPurchaseOrder(details.getDocId(), details.getDocState());
            else if (functionality.getAction().equalsIgnoreCase("View"))
                return templateService.viewTemplateHtmlForPurchaseOrder(details.getDocId(),details.getDocState());
            else
            return ResponseDomain.badRequest("Undefined Action", false);
        }
    }

    /**
     *
     * FETCHING SUPPLIER DETAILS
     *
     *
     **/
    @Override
    public ResponseEntity<?> supplierDetails(String name, String docId) {
        log.info("Entering Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: supplierDetails" + docId);
        JSONObject jsonObject = new JSONObject();
        try {
            if (name != null && docId == null || docId.equals("0")) {
                AccountDetails supplier =  jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_SUPPLIER_DETAILS_NEW, new Object[]{name}, new AccountDetailsMapper());
                jsonObject.put("supplier", supplier);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else if (docId != null) {
                AccountDetails supplier =  jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_EXISTING_SUPPLIER_DETAILS, new Object[]{docId}, new AccountDetailsExistingMapper());
                jsonObject.put("supplier", supplier);
                log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
                return new ResponseEntity<>(supplier, HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
            return ResponseDomain.badRequest("No such customer exists", false);
        }
        log.info("Exiting Repository Class :::: SalesOrderTransactionRepositoryImpl :::: method :::: supplierDetails");
        return ResponseDomain.badRequest("No name is Available", false);
    }
}
