package com.sage.api.repository.purchaseorder.list;

import com.sage.api.domain.purchaseorder.list.PurchaseOrderList;
import org.springframework.http.ResponseEntity;

public interface PurchaseOrderListRepository {

    ResponseEntity<?> fetchingPurchaseOrderListData(PurchaseOrderList purchaseOrderList);
}
