package com.sage.api.repository.dashboard;

import com.sage.api.configuration.JwtTokenUtil;
import com.sage.api.domain.dashboard.*;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DashboardRepositoryImpl implements DashboardRepository {

    private static final Logger log = LogManager.getLogger(DashboardRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /*
     *
     * FETCHING DASHBOARD DATA
     *
     *
     */
    @Override
    public ResponseEntity<?> data(Integer id, String agentName) {
        log.info("Entering Repository Class :::: DashboardRepositoryImpl :::: method :::: data :::: Agent :::: " + agentName);
        JSONObject jsonObject = new JSONObject();
        String name = "";
        try {
            ViewObject salesViewObj = new ViewObject();
            ViewObject purchasesViewObj = new ViewObject();
            ViewObject receiptsViewObj = new ViewObject();
            salesViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_SALES_ORDER, new Object[]{id}, Float.class));
            salesViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_SALES_ORDER, new Object[]{id}, Float.class));
            salesViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_SALES_ORDER_GRAPH_DATA, new Object[]{id}, new DailyCountMapper()));
            salesViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_SALES_ORDER_GRAPH_DATA, new Object[]{id}, new MonthlyCountMapper()));
            purchasesViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_PURCHASE_ORDER_COUNT, new Object[]{id}, Float.class));
            purchasesViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_PURCHASES_COUNT, new Object[]{id}, Float.class));
            purchasesViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_PURCHASES_GRAPH_DATA, new Object[]{id}, new DailyCountMapper()));
            purchasesViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_PURCHASES_GRAPH_DATA, new Object[]{id}, new MonthlyCountMapper()));
            LoggedAgent agent = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DASHBOARD_AGENT, new Object[]{agentName}, new LoggedAgentMapper());
            if (agent.getInitial() != null)
                name += agent.getInitial() + " ";
            if (agent.getFirstName() != null)
                name += agent.getFirstName() + " ";
            if (agent.getLastName() != null)
                name += agent.getLastName();
            Long agentTrCode=jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_AGENT_TR_CODE,new Object[]{agentName},Long.class);
            receiptsViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_RECEIPTS_COUNT, new Object[]{agentTrCode, id}, Float.class));
            receiptsViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_RECEIPTS_COUNT, new Object[]{agentTrCode, id}, Float.class));
            receiptsViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_RECEIPTS_GRAPH_DATA, new Object[]{agentTrCode, id}, new DailyCountMapper()));
            receiptsViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_RECEIPTS_GRAPH_DATA, new Object[]{agentTrCode, id}, new MonthlyCountMapper()));
            jsonObject.put("totalSales", salesViewObj);
            jsonObject.put("totalPurchases", purchasesViewObj);
            jsonObject.put("totalReceipts", receiptsViewObj);
            jsonObject.put("agentName", name);
            jsonObject.put("agentEmail", agent.getEmail());
            log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: data");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            name="";
            ViewObject salesViewObj = new ViewObject();
            ViewObject purchasesViewObj = new ViewObject();
            ViewObject receiptsViewObj = new ViewObject();
            salesViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_SALES_ORDER, new Object[]{id}, Float.class));
            salesViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_SALES_ORDER, new Object[]{id}, Float.class));
            salesViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_SALES_ORDER_GRAPH_DATA, new Object[]{id}, new DailyCountMapper()));
            salesViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_SALES_ORDER_GRAPH_DATA, new Object[]{id}, new MonthlyCountMapper()));
            purchasesViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_PURCHASE_ORDER_COUNT, new Object[]{id}, Float.class));
            purchasesViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_PURCHASES_COUNT, new Object[]{id}, Float.class));
            purchasesViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_PURCHASES_GRAPH_DATA, new Object[]{id}, new DailyCountMapper()));
            purchasesViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_PURCHASES_GRAPH_DATA, new Object[]{id}, new MonthlyCountMapper()));
            LoggedAgent agent = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DASHBOARD_AGENT, new Object[]{agentName}, new LoggedAgentMapper());
            if (agent.getInitial() != null)
                name += agent.getInitial() + " ";
            if (agent.getFirstName() != null)
                name += agent.getFirstName() + " ";
            if (agent.getLastName() != null)
                name += agent.getLastName();
            receiptsViewObj.setDailyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_DAILY_RECEIPTS_COUNT, new Object[]{15, id}, Float.class));
            receiptsViewObj.setMonthlyCount(jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_MONTHLY_RECEIPTS_COUNT, new Object[]{15, id}, Float.class));
            receiptsViewObj.setDailyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_DAILY_RECEIPTS_GRAPH_DATA, new Object[]{15, id}, new DailyCountMapper()));
            receiptsViewObj.setMonthlyGraphData(jdbcTemplate.query(SageQueries.Q_FETCHING_MONTHLY_RECEIPTS_GRAPH_DATA, new Object[]{15, id}, new MonthlyCountMapper()));
            jsonObject.put("totalSales", salesViewObj);
            jsonObject.put("totalPurchases", purchasesViewObj);
            jsonObject.put("totalReceipts", receiptsViewObj);
            jsonObject.put("agentName", name);
            jsonObject.put("agentEmail", agent.getEmail());
            log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: data");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
        catch (Exception e){
            log.error("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: data :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /*
     *
     * LOGGING OUT AGENT
     *
     *
     */
    @Override
    public ResponseEntity<?> loggingOut(Integer id, String deviceType) {
        Boolean set = false;
        log.info("Entering Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut");
        try {
            if (deviceType.equalsIgnoreCase("desktop")) {
                jdbcTemplate.update(SageQueries.Q_UPDATING_DESKTOP_DEVICE_LOGOUT, set, id);
                log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut");
                return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"logged out successfully",true),HttpStatus.OK);
            } else if (deviceType.equalsIgnoreCase("android")) {
                jdbcTemplate.update(SageQueries.Q_UPDATING_ANDROID_DEVICE_LOGOUT, set, id);
                log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut");
                return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"logged out successfully",true),HttpStatus.OK);
            } else if (deviceType.equalsIgnoreCase("ios")) {
                jdbcTemplate.update(SageQueries.Q_UPDATING_IOS_DEVICE_LOGOUT, set, id);
                log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut");
                return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()),"logged out successfully",true),HttpStatus.OK);
            } else {
                log.info("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut");
                return ResponseDomain.badRequest("Unknown Device Type",false);
            }
        } catch (Exception e) {
            log.error("Exiting Repository Class :::: DashboardRepositoryImpl :::: method :::: loggingOut :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error ",false);
        }
    }
}
