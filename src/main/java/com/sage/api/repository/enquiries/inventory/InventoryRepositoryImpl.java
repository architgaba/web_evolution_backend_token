package com.sage.api.repository.enquiries.inventory;

import com.sage.api.domain.enquiries.*;
import com.sage.api.utils.ResponseDomain;
import com.sage.api.utils.SageQueries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Transactional
@Repository
public class InventoryRepositoryImpl implements InventoryRespository {

    private static final Logger log = LogManager.getLogger(InventoryRepositoryImpl.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private ResponseDomain responseDomain;

    /**
     *
     * FETCHING ALL INVENTORY ACCOUNTS
     *
     *
     **/
    @Override
    public ResponseEntity<?> fetchingInventoryAccounts() {
        log.info("Entering Repository Class :::: InventoryRespositoryImpl :::: method :::: fetchingInventoryAccounts");
        JSONObject jsonObject = new JSONObject();
        List<ViewObj> accountData = new ArrayList<>();
        List<ViewObj> nameData = new ArrayList<>();
        try {
            List<Inventory> inventories = jdbcTemplate.query(SageQueries.Q_FETCHING_INVENTORY_ACCOUNT, new InventoryMapper());
            inventories.forEach(inventory1 -> {
                ViewObj jsonMapClass = new ViewObj();
                jsonMapClass.setValue(inventory1.getAccount());
                jsonMapClass.setViewValue(inventory1.getName());
                accountData.add(jsonMapClass);
                ViewObj jsonMapClass1 = new ViewObj();
                jsonMapClass1.setValue(inventory1.getName());
                jsonMapClass1.setViewValue(inventory1.getAccount());
                nameData.add(jsonMapClass1);
            });
            jsonObject.put("accounts", accountData);
            jsonObject.put("names", nameData);
            log.info("Exiting Repository Class :::: InventoryRespositoryImpl :::: method :::: fetchingInventoryAccounts");
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }catch (EmptyResultDataAccessException e) {
            log.error("Exiting Repository Class :::: InventoryRespositoryImpl :::: method :::: fetchingInventoryAccounts :::: Error :::: " + e);
            return ResponseDomain.internalServerError("No Stock Available", false);
        }catch (Exception e) {
            log.error("Exiting Repository Class :::: InventoryRespositoryImpl :::: method :::: fetchingInventoryAccounts :::: Error :::: " + e);
            return ResponseDomain.internalServerError("Internal Server Error",false);
        }
    }

    /**
     *
     * FETCHING INVENTORY DATA
     *
     *
     **/
    @Override
    public ResponseEntity<?> inventoryData(Inventory inventoryData) {
        log.info("Entering Repository Class :::: InventoryRespositoryImpl :::: method :::: inventoryData");
        LocalDate startDate = LocalDate.parse(inventoryData.getFromDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse(inventoryData.getEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Float amount = 0.0f, quantity = 0.0f;
        try {
            Integer accountLink = jdbcTemplate.queryForObject(SageQueries.Q_FETCHING_INVENTORY_ACCOUNT_LINK, new Object[]{inventoryData.getAccount(), inventoryData.getName()}, Integer.class);
            List<InventoryData> inventoryReceivableData = jdbcTemplate.query(SageQueries.Q_FETCHING_INVENTORY_DATA, new Object[]{accountLink, startDate, endDate}, new InventoryDataMapper());
            List<InventoryViewObj> viewObj = new ArrayList<>();
            if (!inventoryReceivableData.isEmpty()) {
                for (InventoryData inventoryData1 : inventoryReceivableData) {
                    InventoryViewObj inventoryViewObj = new InventoryViewObj();
                    inventoryViewObj.setDate(inventoryData1.getDate());
                    inventoryViewObj.setReference(inventoryData1.getReference());
                    inventoryViewObj.setDescription(inventoryData1.getDescription());
                    inventoryViewObj.setAccount(inventoryData1.getAccount());
                    inventoryViewObj.setAmount(String.format("%.2f", inventoryData1.getAmount()));
                    inventoryViewObj.setExtOrderNo(inventoryData1.getExtOrderNo());
                    inventoryViewObj.setOrderNo(inventoryData1.getOrderNo());
                    inventoryViewObj.setTxCode(inventoryData1.getTxCode());
                    inventoryViewObj.setQty(String.format("%.2f", inventoryData1.getQty()));
                    inventoryViewObj.setWH(inventoryData1.getWH());
                    inventoryViewObj.setRepCode(inventoryData1.getRepCode());
                    viewObj.add(inventoryViewObj);
                    amount = amount + inventoryData1.getAmount();
                    quantity = quantity + inventoryData1.getQty();
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("amount", String.format("%.2f", amount));
                jsonObject.put("quantity", String.format("%.2f", quantity));
                jsonObject.put("inventoryReceivableData", viewObj);
                log.info("Exiting Repository Class :::: InventoryRespositoryImpl :::: method :::: inventoryData");
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            } else {
                log.info("Exiting Repository Class :::: InventoryRespositoryImpl :::: method :::: inventoryData");
                return new ResponseEntity<>(new ResponseDomain(HttpStatus.OK,new Date(System.currentTimeMillis()), "No Data Available",true), HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            log.info("Exiting Repository Class :::: fetchingInventoryAccounts :::: method :::: inventoryData :::: Error :::: "+e);
            return ResponseDomain.badRequest( "Invalid Stock Code/Name Combination",false);
        }catch (Exception e){
            log.info("Exiting Repository Class :::: fetchingInventoryAccounts :::: method :::: inventoryData :::: Error :::: "+e);
            return ResponseDomain.internalServerError( "Internal Server Error",false);
        }
    }
}
