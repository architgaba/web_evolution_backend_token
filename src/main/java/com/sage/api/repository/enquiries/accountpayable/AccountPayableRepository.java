package com.sage.api.repository.enquiries.accountpayable;

import com.sage.api.domain.enquiries.Accounts;
import org.springframework.http.ResponseEntity;
import java.time.LocalDate;

public interface AccountPayableRepository {

    ResponseEntity<?> fetchingAllAccounts();

    ResponseEntity<?> fetchingAccountData(LocalDate fromDate, LocalDate endDate, Accounts accounts);
}


