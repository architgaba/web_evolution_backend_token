package com.sage.api.repository.enquiries.inventory;

import com.sage.api.domain.enquiries.Inventory;
import org.springframework.http.ResponseEntity;

public interface InventoryRespository {

    ResponseEntity<?> fetchingInventoryAccounts();

    ResponseEntity<?>  inventoryData(Inventory inventoryData);
 }
