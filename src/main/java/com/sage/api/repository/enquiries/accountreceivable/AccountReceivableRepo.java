package com.sage.api.repository.enquiries.accountreceivable;

import com.sage.api.domain.enquiries.Accounts;
import org.springframework.http.ResponseEntity;

public interface AccountReceivableRepo {

    ResponseEntity<?> fetchingAllAccounts();

    ResponseEntity<?> accountReceivableData(Accounts accounts);
 }
