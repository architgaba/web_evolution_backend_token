package com.sage.api.repository.receipts;

import com.sage.api.domain.enquiries.Accounts;
import org.springframework.http.ResponseEntity;

public interface ReceiptsRepository {

     ResponseEntity<?> fetchCustomerAccount();

     String updatingPOSTAR(Integer agentId,String customerCode);

     String updatingInvNum(String tenderAmount,String changeAmount, String invoiceNum);

     ResponseEntity<?> agentReceiptsData(Accounts accounts, String agent);
}
