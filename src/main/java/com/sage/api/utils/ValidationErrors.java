package com.sage.api.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ValidationErrors {
    @JsonProperty
    private String[]  Errors;
    @JsonProperty
    private String PropertyName;
}
