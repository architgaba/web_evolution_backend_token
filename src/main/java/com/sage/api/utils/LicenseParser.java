package com.sage.api.utils;

import com.google.gson.Gson;
import com.sage.api.repository.login.AgentLoginRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Component
public class LicenseParser implements Serializable{

    private static final Logger log = LogManager.getLogger(LicenseParser.class);
    @Autowired
    private AgentLoginRepository agentLoginRepository;
    static final Gson g = new Gson();

    /**
     *
     * PARSING REGISTRATION TOKEN
     *
     *
     **/
    public String parsingToken(String token) {
        log.info("Entering Util Class :::: LicenseParser :::: method :::: parsingToken");
        TokenPayload tokenPayload = getPayload(token);
        Boolean expiryValidation = isTokenExpired(tokenPayload.getExp());
        String clientId = tokenPayload.getMachine_id();
        Integer userCount = tokenPayload.getMax_logins();
        if (expiryValidation) {
            return "Your registration has expired: Please contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za";
        }
        String serverMac = getServerMacAddress();
        if (serverMac.contains("Error")) {
            return "Unable to obtain server ID: Please contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za";
        }
        if (!clientId.equals(serverMac)) {
            return "Unauthorized server ID: Please contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za";
        }
        agentLoginRepository.activeAgentCount();
        if (userCount < agentLoginRepository.activeAgentCount()+1) {
            return "Maximum logins exceeded: Please log off an existing device or contact Brilliant Link on 011 792 9521 or at admin@brilliantlink.co.za for additional licenses";
        }
        return "Authenticated Successfully";
    }

    /**
     *
     * VALIDATING LICENSE EXPIRY
     *
     *
     **/
    private Boolean isTokenExpired(Long expiry) {
        Date expiration = new Date(expiry * 1000);
        return expiration.before(new Date());
    }

    /**
     *
     * DECODING LICENSE KEY
     *
     *
     **/
     public static String decodeToken(String JwtEncodedToken){
        try {
            String[] split = JwtEncodedToken.split("\\.");
            Base64.Decoder decoder = Base64.getUrlDecoder();
            String body = split[1];
            byte[] decodedBytes = decoder.decode(body.getBytes());
            return new String(decodedBytes, "UTF-8");
        }
        catch (Exception e){
            return null;
        }
    }


    /**
     *
     * GETTING PAYLOAD FROM TOKEN
     *
     *
     **/
    private static TokenPayload getPayload(String token) {
        log.info("Entering Util Class :::: LicenseParser :::: method :::: getPayload");
        try {
            String json = decodeToken(token);
            return g.fromJson(json, TokenPayload.class);
        } catch (Exception ex) {
            log.error("Bad Token: " + ex.getMessage());
           return null;
        }
    }

   /**
     *
     * FETCHING MAC ADDRESS OF SERVER
     *
     *
     **/
   private String getServerMacAddress() {
       InetAddress ip;
       try {
           ip = InetAddress.getLocalHost();
           NetworkInterface network = NetworkInterface.getByInetAddress(ip);
           byte[] mac = network.getHardwareAddress();
           StringBuilder sb = new StringBuilder();
           for (int i = 0; i < mac.length; i++) {
               sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
           }
           return sb.toString();
       } catch (UnknownHostException e) {
           e.printStackTrace();
           return "Error in Fetching Server MAC Address";
       } catch (SocketException e) {
           e.printStackTrace();
           return "Error in Fetching Server MAC Address";
       }
   }

    /**
     *
     * FETCHING MAC ADDRESS OF SERVER
     *
     *
     **/
    public List<String> fetchingValidator(){
        ArrayList<String > arrayList = new ArrayList<>();
        try{
            String license=agentLoginRepository.fetchingLicense();
            arrayList.add(license);
            TokenPayload tokenPayload = getPayload(license);
            arrayList.add(tokenPayload.getValidator());
            return arrayList;
        }catch (Exception e){
            return null;
        }
    }
}
