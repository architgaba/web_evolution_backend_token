package com.sage.api.utils;

import com.sage.api.domain.template.SavedTemplate;
import com.sage.api.domain.template.TemplatePOJO;
import com.sage.api.repository.template.TemplateCreatorImpl;
import com.sage.api.repository.template.TemplateRepository;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Component
public class PDFConverter implements Serializable{

    @Autowired
    private TemplateCreatorImpl templateCreator;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse httpServletResponse;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private SavedTemplate savedTemplate;
    @Autowired
    private TemplateRepository templateRepository;

    private static final int BUFFER_SIZE = 4096;

    private static final Logger log = LogManager.getLogger(PDFConverter.class);

    public static Map<String, Object> getFieldMap(TemplatePOJO templatePOJO) {
        Map<String, Object> map = new HashMap();
        map.put("invoicedata", templatePOJO);
        return map;
    }

    /**
     *
     * GENERATING PDF FROM HTML USING WKHTMLTOPDF
     *
     *
     **/
    public ResponseEntity<?> convertTOPDF(String templateData, String docId) {
        try {
            log.info("Entering CLass :::: PDFConverter :::: method :::: convertTOPDF");
            String currentDirectory = System.getProperty("user.dir");
            File htmlFileDirectory = new File(currentDirectory + "\\generatedHTML\\");
            File f = new File(currentDirectory + "\\generatedPDF");
            if (!f.exists()) {
                if (f.mkdir())
                    log.info("Directory created");
            }
            File htmlFile = new File(htmlFileDirectory.getPath() + "\\" + docId + ".html");
            File pdf = new File(f.getPath() + "\\" + docId + ".pdf");
            java.lang.Runtime rt = java.lang.Runtime.getRuntime();
            String generatedCommand = "wkhtmltopdf  \"" + htmlFile.getPath() + "\"   \"" + pdf.getPath() + "\"";
            rt.exec(generatedCommand);
            Thread.sleep(5000);
            log.info("Exiting Class :::: PDFConverter :::: method :::: convertTOPDF");
            return new ResponseEntity<>(new ResponseDomain(true, "PDF Created Successfully"), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exiting Class :::: PDFConverter :::: method :::: convertTOPDF :::: Error :::: " + e);
            return ResponseEntity.badRequest().body("Error Converting");
        }
    }

    /**
     *
     * GENERATING HTML FOR TEMPLATE
     *
     *
     **/
    public String generateHTML(String templateHTMLString, TemplatePOJO templatePOJO, String docId) throws IOException, TemplateException {
        log.info("Entering Class :::: PDFConverter :::: method :::: generateHTML");
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        stringLoader.putTemplate(templateHTMLString, templateHTMLString);
        cfg.setTemplateLoader(stringLoader);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        Template ftlTemplateObj = cfg.getTemplate(templateHTMLString);
        StringWriter stringWriter = new StringWriter();
        String currentDirectory = System.getProperty("user.dir");
        File f = new File(currentDirectory + "\\generatedHTML\\");
        if (!f.exists()) {
            if (f.mkdir())
                log.info("Directory created");
        }
        File html = new File(f.getPath() + "\\" + docId + ".html");
        String filePath = html.getPath();
        Writer file = new FileWriter(new File(filePath));
        ftlTemplateObj.process(templatePOJO, stringWriter);
        ftlTemplateObj.process(templatePOJO, file);
        String MYTEMPLATE = stringWriter.toString();
        SavedTemplate myTemplate = new SavedTemplate();
        myTemplate.setTemplate(MYTEMPLATE);
        myTemplate.setDocId(Long.parseLong(docId));
        stringWriter.close();
        log.info("Exiting Class :::: PDFConverter :::: method :::: generateHTML");
        return MYTEMPLATE;
    }

    /**
     *
     * DOWNLOADING TEMPLATE
     *
     *
     **/
    public byte[] downloadPdf(File file, String docId) {
        log.info("Entering Class :::: PDFConverter :::: method :::: downloadPdf");
        try {
            ServletContext context = request.getServletContext();
            String fullPath = file.getPath();
            File downloadFile = file;
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(downloadFile);
            } catch (FileNotFoundException e1) {
                Thread.sleep(5000);
                inputStream = new FileInputStream(downloadFile);
            }
            String mimeType = context.getMimeType(fullPath);
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            httpServletResponse.setContentType(mimeType);
            httpServletResponse.setContentLength((int) downloadFile.length());
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",
                    downloadFile.getName());
            httpServletResponse.setHeader(headerKey, headerValue);
            OutputStream outStream = httpServletResponse.getOutputStream();
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            inputStream.close();
            outStream.close();
            log.info("Exiting Class :::: PDFConverter :::: method :::: downloadPdf");
            return buffer;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exiting Class :::: PDFConverter :::: method :::: downloadPdf :::: Error :::: "+e);
            return null;
        }
    }

}
