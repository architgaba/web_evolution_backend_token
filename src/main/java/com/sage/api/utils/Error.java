package com.sage.api.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Error implements Serializable {
    @JsonProperty
    private boolean HasError;
    @JsonProperty
    private int ID;
    @JsonProperty
    private String Message;
    @JsonProperty
    private String OperationPerformed;
    @JsonProperty
    List<com.sage.api.utils.ValidationErrors> ValidationErrors;

}

