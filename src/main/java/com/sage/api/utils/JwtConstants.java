package com.sage.api.utils;

import java.io.Serializable;

public class JwtConstants implements Serializable{
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 4*60*60*60;
    public static final String SIGNING_KEY = "brilliantLink";
    public static final String LICENSE_SIGNING_KEY = "ABRACADABRA";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
