package com.sage.api.utils;

import java.io.Serializable;

public class FreedomServicesConstants implements Serializable{

    public static final String databaseName="Dev_Nile";
   // public static final String serverHost="http://OMENDRA";
    public static final String serverHost="http://localhost";
    public static final String serverPort=":5000/freedom.core/";
    public static final String loginApi=serverHost+serverPort+databaseName;
    public static final String baseUrl=serverHost+serverPort+databaseName+"/SDK/Rest";
}
