package com.sage.api.utils;

import java.io.Serializable;

public class SageQueries implements Serializable{

    public static final String Q_FETCHING_ALL_AGENTS = "SELECT cAgentName,idAgents FROM _rtblAgents";

    public static final String Q_ADDING_AGENT="INSERT INTO agent_login_status(id , name) (select idAgents, cAgentName from _rtblAgents where cAgentName=?)";

    public static final String Q_CHECKING_AGENT = "select count(*) from agent_login_status where name= ?";

    public static final String Q_CHECKING_DESKTOP_DEVICE = "Select desktop from agent_login_status where name=?";

    public static final String Q_UPDATING_DESKTOP_DEVICE = "update agent_login_status set desktop = ? ,hitCount=(hitCount + 1) where name=?";

    public static final String Q_CHECKING_ANDROID_DEVICE = "Select android from agent_login_status where name=?";

    public static final String Q_UPDATING_ANDROID_DEVICE = "update agent_login_status set android = ?,hitCount=(hitCount + 1) where name=?";

    public static final String Q_CHECKING_IOS_DEVICE = "Select ios from agent_login_status where name=?";

    public static final String Q_UPDATING_IOS_DEVICE = "update agent_login_status set ios = ?,hitCount=(hitCount + 1) where name=?";

    public  static final String Q_FETCHING_AGENT ="SELECT cAgentName,idAgents FROM _rtblAgents where cAgentName=?";

    public  static final String Q_FETCHING_DASHBOARD_AGENT ="select cInitials,cFirstName,cLastName,cEmail from _rtblAgents where cAgentName=?";

    public static final String Q_FETCHING_ACCOUNTS = "SELECT Account,Name FROM VENDOR";

    public static final String Q_UPDATING_DESKTOP_DEVICE_LOGOUT = "update agent_login_status set desktop = ?, hitCount=(hitCount - 1) where id=?";

    public static final String Q_UPDATING_ANDROID_DEVICE_LOGOUT = "update agent_login_status set android = ?, hitCount=(hitCount - 1) where id=?";

    public static final String Q_UPDATING_IOS_DEVICE_LOGOUT = "update agent_login_status set ios = ?,hitCount=(hitCount - 1) where id=?";

    public static final String Q_GETTING_ACCOUNT_LINK = "SELECT DCLink from Vendor where name = ? and account=?";

    public static final String Q_GETTING_ACCOUNT_PAYABLE_DATA = "SELECT TxDate ,TrCode ,Reference , Description, Order_No ,ExtOrderNum ,Debit,Credit,Outstanding FROM  dbo._bvAPTransactionsFull WHERE AccountLink= ? AND TxDate >= ? AND TxDate <= ? ORDER BY TxDate";

    public static final String Q_FETCHING_ALL_ACCOUNTS_RECEIVABLE = "SELECT Account,Name,DCLink FROM CLIENT";

    public static final String Q_FETCHING_ACCOUNT_RECEIVABLE_DATA = "SELECT TxDate ,TrCode ,Reference , Description, Order_No ,ExtOrderNum ,Debit,Credit,Outstanding FROM dbo._bvARTransactionsFull WHERE AccountLink= ? AND TxDate >= ? AND TxDate <= ? ORDER BY TxDate";

    public static final String Q_FETCHING_RECEIPTS_DATA_AGENT="SELECT TxDate ,TrCode ,Reference ,Description,Order_No,ExtOrderNum ,Debit,Credit,Outstanding FROM dbo._bvARTransactionsFull WHERE AccountLink=(SELECT DCLink FROM CLIENT where Name= ? AND Account=?) AND TxDate >= ? AND TxDate <= ?  AND TrCode=(select pspmCode from agents_defaults_permission where agentName=?) ORDER BY TxDate";

    public static final String Q_FETCHING_DCLINK_CLIENT = "SELECT DCLink FROM CLIENT where Name=? AND Account=?";

    public static final String Q_FETCHING_INVENTORY_ACCOUNT = "SELECT Code, Description_1 from StkItem ORDER by Code";

    public static final String Q_FETCHING_INVENTORY_ACCOUNT_LINK = "SELECT StockLink FROM StkItem WHERE Code=? AND Description_1=?";

    public static final String Q_FETCHING_INVENTORY_DATA = "SELECT TxDate,TrCode,Reference,Description,Order_No,ExtOrderNum ,ActualValue,ActualQuantity, WarehouseCode,Account,RepCode FROM dbo._bvSTTransactionsFull WHERE AccountLink=? AND TxDate >= ? AND TxDate <= ? ORDER BY TxDate";

    public static final String Q_FETCHING_INVENTORY_STORE_CHECK_DATA = "SELECT dbo._bvWarehouseStockFull.WhCode ,dbo.WhseMst.Name ,dbo._bvWarehouseStockFull.WHQtyOnSO ,dbo._bvWarehouseStockFull.WHQtyOnPO ,WHQtyOnHand ,WHJobQty , WHMFPQty , WHQtyReserved  FROM  dbo._bvWarehouseStockFull INNER JOIN dbo.WhseMst ON dbo._bvWarehouseStockFull.WhseLink = dbo.WhseMst.WhseLink WHERE (WHStockLink=?) ORDER BY WhCode";

    public static final String Q_FETCHING_SALES_ORDER_LIST_DATA = "SELECT DocStateDescription,InvNumber,Account,Name,OrderNum,ExtOrderNum,OrderDate,DueDate,InvDate,RepCode,INVNUMAgentName,StatusDescrip,OrdTotIncl,AutoIndex FROM  dbo._bvInvNumARFull WHERE DocType='4' AND AccountID=? AND InvDate >= ? AND InvDate <=? ORDER BY OrderDate";

    public static final String Q_FETCHING_SALES_ORDER_LIST_DATA_ON_DOCSTATE = "SELECT DocStateDescription,InvNumber,Account,Name,OrderNum,ExtOrderNum,OrderDate,DueDate,InvDate,RepCode,INVNUMAgentName,StatusDescrip,OrdTotIncl,AutoIndex FROM  dbo._bvInvNumARFull WHERE DocType='4' AND AccountID=? AND DocStateDescription= ? AND InvDate >= ? AND InvDate <=? ORDER BY OrderDate";

    public static final String Q_FETCHING_DAILY_SALES_ORDER = "Select IsNull(D.TotalSalesForDay, 0) as Total from _rtblAgents CROSS APPLY (SELECT N as Day, DATEADD(d, N-1, DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate  FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM OUTER APPLY  (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Day(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalSalesForDay from InvNum where IsNull(InvNum_iCreatedAgentID, 0) = idAgents AND DocType IN (0,4) AND DocState = 4 AND InvDate = DM.MonthDate GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Day(InvDate)) D WHERE idAgents = ?  AND DM.Day = DAY(GetDate())";

    public static final String Q_FETCHING_MONTHLY_SALES_ORDER = "Select IsNull(D.TotalSalesForMonth, 0) as Total from _rtblAgents CROSS APPLY (SELECT N as Month, DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth FROM dbo._efnTally(1, 12, 1))YM OUTER APPLY (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Month(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalSalesForMonth from InvNum where IsNull(InvNum_iCreatedAgentID, 0) = idAgents AND DocType IN (0,4) AND DocState = 4 AND InvDate between YM.StartOfMonth and YM.EndOfMonth GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Month(InvDate)) D WHERE idAgents = ? AND YM.Month = Month(GetDate())";

    public static final String Q_FETCHING_DAILY_SALES_ORDER_GRAPH_DATA="Select  DM.Day, IsNull(D.TotalSalesForDay, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Day, DATEADD(d, N-1, \n" +
            "                    DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate \n" +
            "\t\t\t FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Day(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalSalesForDay\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (0,4)\n" +
            "  AND DocState = 4\n" +
            "  AND InvDate = DM.MonthDate\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Day(InvDate)) D\n" +
            "WHERE idAgents = ?";

    public static final String Q_FETCHING_MONTHLY_SALES_ORDER_GRAPH_DATA="Select SubString(DATENAME(month, YM.StartOfMonth), 1, 3) as MonthName,  IsNull(D.TotalSalesForMonth, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Month, \n" +
            "                    DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, \n" +
            "                    CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth \n" +
            "             FROM dbo._efnTally(1, 12, 1))YM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Month(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalSalesForMonth\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (0,4)\n" +
            "  AND DocState = 4\n" +
            "  AND InvDate between YM.StartOfMonth and YM.EndOfMonth\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Month(InvDate)) D WHERE idAgents = ?";

    public static final String Q_FETCHING_DAILY_PURCHASE_ORDER_COUNT="Select IsNull(D.TotalPurchasesForDay, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Day, \n" +
            "                    DATEADD(d, N-1, DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate \n" +
            "\t\t\t FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Day(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalPurchasesForDay\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (5)\n" +
            "  AND DocState = 1\n" +
            "  AND InvDate = DM.MonthDate\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Day(InvDate)) D\n" +
            "WHERE idAgents = ? AND DM.Day = DAY(GetDate())";

    public  static final String Q_FETCHING_MONTHLY_PURCHASES_COUNT="Select  IsNull(D.TotalPurchasesForMonth, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Month, \n" +
            "                    DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, \n" +
            "                    CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth \n" +
            "             FROM dbo._efnTally(1, 12, 1))YM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Month(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalPurchasesForMonth\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (5)\n" +
            "  AND DocState = 1\n" +
            "  AND InvDate between YM.StartOfMonth and YM.EndOfMonth\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Month(InvDate)) D\n" +
            "WHERE idAgents = ? AND YM.Month = Month(GetDate())";

    public  static final String Q_FETCHING_DAILY_PURCHASES_GRAPH_DATA="Select  DM.Day, IsNull(D.TotalPurchasesForDay, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Day, DATEADD(d, N-1, \n" +
            "                    DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate \n" +
            "\t\t\t FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Day(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalPurchasesForDay\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (5)\n" +
            "  AND DocState = 1\n" +
            "  AND InvDate = DM.MonthDate\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Day(InvDate)) D\n" +
            "WHERE idAgents = ?";

    public  static final String Q_FETCHING_MONTHLY_PURCHASES_GRAPH_DATA="Select  SubString(DATENAME(month, YM.StartOfMonth), 1, 3) as MonthName,  IsNull(D.TotalPurchasesForMonth, 0) as Total\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Month, \n" +
            "                    DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, \n" +
            "                    CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth \n" +
            "             FROM dbo._efnTally(1, 12, 1))YM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(InvNum_iCreatedAgentID, 0) as AgentID, Month(InvDate) as InvDay, Sum(IsNull(InvTotIncl, 0)) as TotalPurchasesForMonth\n" +
            "  from InvNum\n" +
            "  where IsNull(InvNum_iCreatedAgentID, 0) = idAgents\n" +
            "  AND DocType IN (5)\n" +
            "  AND DocState = 1\n" +
            "  AND InvDate between YM.StartOfMonth and YM.EndOfMonth\n" +
            "  GROUP BY IsNull(InvNum_iCreatedAgentID, 0), Month(InvDate)) D\n" +
            "WHERE idAgents = ?";

    public static final String Q_FETCHING_AGENT_TR_CODE="select idTrcodes from TrCodes where code=(select pspmCode from agents_defaults_permission where agentName=?) AND  iModule = 5 ;";

    public static final String Q_FETCHING_DAILY_RECEIPTS_COUNT="Select IsNull(D.TotalReceiptsForDay, 0) as TotalReceiptsForDay\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Day, \n" +
            "                    DATEADD(d, N-1, DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate \n" +
            "\t\t\t FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(PostAR_iCreatedAgentID, 0) as AgentID, Day(TxDate) as InvDay, Sum(IsNull(Credit, 0)) as TotalReceiptsForDay\n" +
            "  from PostAR\n" +
            "  where IsNull(PostAR_iCreatedAgentID, 0) = idAgents\n" +
            "  AND TrCodeID = ? \n" +
            "  AND TxDate = DM.MonthDate\n" +
            "  GROUP BY IsNull(PostAR_iCreatedAgentID, 0), Day(TxDate)) D\n" +
            "WHERE idAgents = ? AND DM.Day = DAY(GetDate())";

    public static final String Q_FETCHING_MONTHLY_RECEIPTS_COUNT="Select IsNull(D.TotalReceiptsForMonth, 0) as TotalReceiptsForMonth\n" +
            "from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Month, \n" +
            "                    DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, \n" +
            "                    CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth \n" +
            "             FROM dbo._efnTally(1, 12, 1))YM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(PostAR_iCreatedAgentID, 0) as AgentID, Month(TxDate) as InvDay, Sum(IsNull(Credit, 0)) as TotalReceiptsForMonth\n" +
            "  from PostAR\n" +
            "  where IsNull(PostAR_iCreatedAgentID, 0) = idAgents\n" +
            "  AND TrCodeID = ?\n" +
            "  AND TxDate between YM.StartOfMonth and YM.EndOfMonth\n" +
            "  GROUP BY IsNull(PostAR_iCreatedAgentID, 0), Month(TxDate)) D\n" +
            "WHERE idAgents = ?\n" +
            "AND YM.Month = Month(GetDate())\n";

    public static final String Q_FETCHING_MONTHLY_RECEIPTS_GRAPH_DATA="Select SubString(DATENAME(month, YM.StartOfMonth), 1, 3) as MonthName,  IsNull(D.TotalReceiptsForMonth, 0) as Total from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Month, \n" +
            "   DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))  AS StartOfMonth, \n" +
            "   CONVERT(datetime, EOMonth(DATEADD(month, N-1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))))  AS EndOfMonth \n" +
            "   FROM dbo._efnTally(1, 12, 1))YM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(PostAR_iCreatedAgentID, 0) as AgentID, Month(TxDate) as InvDay, Sum(IsNull(Credit, 0)) as TotalReceiptsForMonth\n" +
            "  from PostAR\n" +
            "  where IsNull(PostAR_iCreatedAgentID, 0) = idAgents\n" +
            "  AND TrCodeID = ?\n" +
            "  AND TxDate between YM.StartOfMonth and YM.EndOfMonth\n" +
            "  GROUP BY IsNull(PostAR_iCreatedAgentID, 0), Month(TxDate)) D\n" +
            "WHERE idAgents = ?";

    public static final String Q_FETCHING_DAILY_RECEIPTS_GRAPH_DATA="Select DM.Day, IsNull(D.TotalReceiptsForDay, 0) as Total from _rtblAgents\n" +
            "CROSS APPLY (SELECT N as Day, DATEADD(d, N-1, \n" +
            "                    DATEADD(month, DATEDIFF(month, 0, GetDate()), 0)) as MonthDate \n" +
            "\t\t\t FROM dbo._efnTally(1, DAY(EOMONTH(GetDate())), 1)) DM\n" +
            "OUTER APPLY\n" +
            " (select IsNull(PostAR_iCreatedAgentID, 0) as AgentID, Day(TxDate) as InvDay, Sum(IsNull(Credit, 0)) as TotalReceiptsForDay\n" +
            "  from PostAR\n" +
            "  where IsNull(PostAR_iCreatedAgentID, 0) = idAgents\n" +
            "  AND TrCodeID = ?   AND TxDate = DM.MonthDate\n" +
            "  GROUP BY IsNull(PostAR_iCreatedAgentID, 0), Day(TxDate)) D\n" +
            "WHERE idAgents = ?";

    public static final String Q_FETCHING_WAREHOUSE_LIST="SELECT Code, Name from WhseMst";

    public static final String Q_FETCHING_CUSTOMER_DETAILS_NEW="Select Top 1 Contact_Person,Physical1,Physical2,Physical3,Physical4,Physical5,PhysicalPC,Post1,Post2,Post3,Post4,Post5,PostPC,Telephone,EMail FROM Client where name=?";

    public static final String Q_FETCHING_SUPPLIER_DETAILS_NEW="Select Top 1 Contact_Person,Physical1,Physical2,Physical3,Physical4,Physical5,PhysicalPC,Post1,Post2,Post3,Post4,Post5,PostPC,Telephone,EMail FROM vendor where Account=?";

    public static final String Q_FETCHING_CUSTOMER_DETAILS_EXISTING = "Select cContact,Address1,Address2,Address3,Address4,Address5,Address6,PAddress1,PAddress2,PAddress3,PAddress4,PAddress5,PAddress6,cTelephone,cEMail FROM InvNum where AutoIndex=?";

    public static final String Q_FETCHING_EXISTING_SUPPLIER_DETAILS = "Select cContact,Address1,Address2,Address3,Address4,Address5,Address6,PAddress1,PAddress2,PAddress3,PAddress4,PAddress5,PAddress6,cTelephone,cEMail FROM InvNum where AutoIndex=?";

    public static final String Q_FETCHING_DOCUMENT_DETAILS="select Duedate,RepCode,ProjectCode,DeliveryMethod,DocStateDescription from dbo._bvInvNumARFull where AutoIndex= ?";

    public static final String Q_FETCHING_NEW_DOCUMENT_DETAILS="select Duedate,RepCode,ProjectCode,DeliveryMethod,DocStateDescription from dbo._bvInvNumARFull where AutoIndex=(select MAX(AutoIndex) from dbo._bvInvNumARFull where Name=?)";

    public static final String Q_FETCHING_MORE_DOCUMENT_DETAILS="select RepCode,ProjectCode from dbo._bvInvNumARFull where AutoIndex= ?";

    public static final String Q_FETCHING_PRICE_LIST_NAME="SELECT PriceListName FROM _bvARAccountsFull where Name=?";

    public static final String Q_FETCHING_STOCK_DETAILS="SELECT dbo._evPriceListPricesFull.ItemCode, dbo._evPriceListPricesFull.ItemDescription_1,dbo._evPriceListPricesFull.WarehouseCode,dbo._evPriceListPricesFull.cName AS PriceListName,\n" +
            "dbo._evPriceListPricesFull.fExclPrice,dbo._evPriceListPricesFull.fInclPrice,dbo.TaxRate.Code AS TaxCode,dbo.TaxRate.TaxRate\n" +
            "FROM dbo._evPriceListPricesFull INNER JOIN  dbo._bvStockFull ON dbo._evPriceListPricesFull.iStockID = dbo._bvStockFull.StockLink INNER JOIN\n" +
            "dbo.TaxRate ON dbo._bvStockFull.TTInvID = dbo.TaxRate.idTaxRate WHERE cName = ? AND ItemCode = ? AND WarehouseCode = ? ";

    public static final String Q_FETCHING_DELETE_LINE="delete  from _btblInvoiceLines where idInvoiceLines=?";

    public static final String Q_FETCHING_QTY_PROCESSED="select fQtyProcessed,cLineNotes,fUnitPriceIncl from _btblInvoiceLines where idInvoiceLines=?";

    public static final String Q_FETCHING_ORDER_NUM="select OrderNum from InvNum where AutoIndex=(select MAX(AutoIndex) from dbo.InvNum where cAccountName= (select name from Client where account=?))";

    public static final String Q_FETCHING_ORDER_NUM_INVOICE="select InvNumber from InvNum where AutoIndex=(select MAX(AutoIndex) from dbo.InvNum where cAccountName= (select name from Client where account=?))";

    public static final String Q_FETCHING_INVOICE_NUM="select InvNumber from InvNum where AutoIndex=(select MAX(AutoIndex) from dbo.InvNum where cAccountName= (select name from Client where account=?))";

    public static final String Q_FETCHING_UPDATE_INVOICE_NUM="select InvNumber from InvNum where AutoIndex=? and cAccountName=(select name from Client where account=?)";

    public static final String Q_FETCHING_UPDATE_ORDER_NUM="select OrderNum from InvNum where AutoIndex=? and cAccountName=(select name from Client where account=?) ";

    public static final String Q_FETCHING_UPDATE_QUOTE_NUM="select InvNumber from InvNum where AutoIndex=? and cAccountName=(select name from Client where account=?)";

    public static final String Q_FETCHING_DOC_STATE="select DocState from InvNum where AutoIndex=?";

    public static final String Q_FETCHING_DOC_STATE_DESCRIPTION="select DocStateDescription from dbo._bvInvNumARFull where AutoIndex=?";

    public static final String Q_FETCHING_TEMPLATE_DETAILS_EXISTING = "select InvNumber,OrderNum,cAccountName,RepCode,OrderDate,name,cContact,Address1,Address2,Address3,Address4,Address5,Address6,PAddress1,PAddress2,PAddress3,PAddress4,PAddress5,PAddress6,cTelephone,Fax1,cEMail,DocStateDescription from _bvInvNumARFull where autoIndex=?";

    public static final String Q_FETCHING_TEMPLATE_DETAILS_SUPPLIER_EXISTING = "select InvNumber,OrderNum,cAccountName,RepCode,OrderDate,name,cContact,Address1,Address2,Address3,Address4,Address5,Address6,PAddress1,PAddress2,PAddress3,PAddress4,PAddress5,PAddress6,cTelephone,Fax1,cEMail,DocStateDescription from _bvInvNumAPFull where autoIndex=?";

    public static final String Q_FETCHING_TEMPLATE_ORDER_TOTAL_INVOICE="select InvTotIncl,InvDiscAmnt,InvTotExcl,InvTotTax from InvNUm where autoIndex=?";

    public static final String Q_FETCHING_TEMPLATE_ORDER_TOTAL="select OrdTotTax,OrdTotIncl,OrdTotExcl,OrdDiscAmnt from InvNUm where autoIndex=?";

    public static final String Q_FETCHING_TEMPLATE_LINES="select cDescription,fQuantity,fQtyProcessed,fQtyToProcess,fUnitPriceExcl,fQuantityLineTotIncl,fQuantityLineTaxAmount,fLineDiscount,cLineNotes from _btblInvoiceLines where iInvoiceID=?";

    public static final String Q_FETCHING_TEMPLATE_LINES_ITEM_CODE="select Code from StkItem where Description_1=?";

    public static final String  Q_FETCHING_TEMPLATE_LINES_GL_CODE="select Master_Sub_Account from _evGLAccountsFull WHERE iAllowICPurchases = 1 and Description=?";

    public static final String  Q_FETCHING_TEMPLATE_LINES_GL_CODE_SALES_ORDER="select Master_Sub_Account from _evGLAccountsFull WHERE iAllowIcSales = 1 and Description=?";

    public static final String Q_FETCHING_SIGNATURE="select CONVERT(varchar(max),imgOrderSignature) from InvNum where AutoIndex=?";

    public static final String Q_UPDATING_SIGNATURE="update InvNum set imgOrderSignature=CONVERT(VarBinarY(max),?) where AutoIndex=? ";

    public static final String Q_FETCHING_LINES_NOTES="select idInvoiceLines from _btblInvoiceLines where iInvoiceID=?";

    public static final String Q_FETCHING_AUTO_INDEX="select MAX(AutoIndex) from InvNum where cAccountName=(select name from Client where account=? )";

    public static final String Q_UPDATING_LINENOTE="update _btblInvoiceLines set cLineNotes=? where idInvoiceLines=?";

    public  static final String Q_FETCHING_PURCHASE_ORDER_LIST_DATA="SELECT DocStateDescription,InvNumber,OrderNum,OrderDate,DueDate,InvDate,INVNUMAgentName,ISNULL(StatusDescrip,'None') as 'OrderStatus',OrdTotIncl,AutoIndex FROM  dbo._bvInvNumAPFull WHERE DocType='5' AND AccountID=?  AND InvDate >= ? AND InvDate <=? ORDER BY OrderDate";

    public  static final String Q_FETCHING_PURCHASE_ORDER_LIST_DATA_DOCSTATE="SELECT DocStateDescription,InvNumber,OrderNum,OrderDate,DueDate,InvDate,INVNUMAgentName,ISNULL(StatusDescrip,'None') as 'OrderStatus',OrdTotIncl,AutoIndex FROM dbo._bvInvNumAPFull WHERE DocType='5' AND AccountID=? AND DocStateDescription= ? AND InvDate >= ? AND InvDate <=?  ORDER BY OrderDate";

    public static final String Q_FETCHING_MAIL_TO="SELECT Email FROM Client where Account=?";

    public static final String Q_FETCHING_SUPPLIER_MAIL_TO="SELECT Email FROM Vendor where Account=?";

    public static final String Q_FETCHING_MAIL_FROM_DEFAULT="SELECT cEmail FROM _RTBLAGENTS where cAgentName=?";

    public static final String Q_FETCHING_WAREHOUSE_CODE="SELECT Code FROM WhseMst ORDER BY Code";

    public static final String Q_FETCHING_REP_CODE="SELECT Code FROM SalesRep ORDER BY Code";

    public static final String Q_FETCHING_PROJECT_CODE="SELECT ProjectCode FROM Project ORDER BY ProjectCode";

    public static final String Q_FETCHING_SALES_ORDER_ACC="SELECT Account FROM Client ORDER BY Account";

    public static final String Q_FETCHING_PURCHASE_ORDER_ACC="SELECT Account FROM Vendor ORDER BY Account";

    public static final String Q_FETCHING_TILL="SELECT TillNo FROM Tills ORDER BY TillNo";

    public static final String Q_FETCHING_PSPM="SELECT Code,Description FROM TrCodes WHERE iModule = 5 ORDER BY Code";

    public static final String Q_SAVING_AGENT_DEFAULT_DETAILS="insert into agents_defaults_permission(agentName,whCode,whFilter,repCode,repFilter,project,projectFilter,salesOrderAccCode,salesOrderAccFilter,till,tillFilter,purchaseOrderAccCode,purchaseOrderAccFilter,pspmCode,pspmFilter,glAccountsSOList,glAccountsSOListFilter,glAccountsPOList,glAccountsPOListFilter) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String Q_FETCHING_GL_ACCOUNTS_IN_LIST_SO_FILTER="SELECT Master_Sub_Account,Description FROM _evGLAccountsFull WHERE iAllowICSales = 1 ORDER BY Master_Sub_Account";

    public static final String Q_FETCHING_GL_ACCOUNTS_IN_LIST_PO_FILTER="SELECT Master_Sub_Account,Description FROM _evGLAccountsFull WHERE iAllowICPurchases = 1 ORDER BY Master_Sub_Account";

    public static final String Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_INV="select DocStateDescription,AutoIndex from dbo._bvInvNumAPFull where AutoIndex=(select max(AutoIndex) from dbo._bvInvNumAPFull where INVNumber=?)";

    public static final String Q_FETCHING_DOCSTATE_TEMPLATE_FUNCTIONALITY_ORD="select DocStateDescription,AutoIndex from dbo._bvInvNumAPFull where AutoIndex=(select max(AutoIndex) from dbo._bvInvNumAPFull  where OrderNum=? )";

    public static final String Q_FETCHING_DOC_ID_ON_ORDER_BASIS="select max(AutoIndex) from dbo._bvInvNumAPFull  where OrderNum=?";

    public static final String Q_FETCHING_DOC_ID_ON_INV_BASIS="select max(AutoIndex) from dbo._bvInvNumAPFull  where INVNumber=?";

    public static final String Q_UPDATING_AGENT_DEFAULT_DETAILS="update agents_defaults_permission set agentName=?,whCode=?,whFilter=?,repCode=?,repFilter=?,project=?,projectFilter=?,salesOrderAccCode=?,salesOrderAccFilter=?,till=?,tillFilter=?,purchaseOrderAccCode=?,purchaseOrderAccFilter=?,pspmCode=?,pspmFilter=?,glAccountsSOList=?,glAccountsSOListFilter=?,glAccountsPOList=?,glAccountsPOListFilter=? where id=?";

    public static final String Q_FETCHING_PURCHASE_ORDER_NUM="select OrderNum from InvNum where AutoIndex=(select MAX(AutoIndex) from dbo.InvNum where cAccountName= (select name from Vendor where account=?))";

    public static final String Q_FETCHING_GRV_NUM="select GrvNumber from InvNum where AutoIndex=? and cAccountName= (select name from Vendor where account=?)";

    public static final String Q_FETCHING_AUTO_INDEX_PURCHASE_ORDER="select MAX(AutoIndex) from InvNum where cAccountName=(select Name from vendor where account=? )";

    public static final String Q_FETCHING_CUSTOMER_DC_BALANCE="select dcBalance,Account,Name from client";

    public static final String Q_UPDATING_AGENT_ID_AGAINST_ORDER="update InvNum set InvNum_iCreatedAgentID =? where AutoIndex =?";

    public static final String Q_FETCHING_STOCK_PRICES_PO="SELECT Code,Description_1,LastGRVCost FROM _bvStockFull";

    public  static final String Q_FETCHING_AGENT_DEFAULTS="Select * from agents_defaults_permission where agentName =?";

    public  static final String Q_FETCHING_AGENT_DEFAULTS_PSPM_DESC="SELECT Description FROM TrCodes WHERE iModule = 5 AND Code=?";

    public static final String Q_FETCHING_RECEIPTS_DATA="SELECT distinct i.OrderNum,i.InvTotIncl , c.Account,c.Name  FROM iNVNUM i,client c  where i.InvNumber=? and c.dcLink=i.AccountID";

    public  static final String Q_UPDATING_AGENT_ID_POSTAR="update POSTAR set PostAR_iCreatedAgentID=? where autoIDx=(select max(AutoIdx) from POSTAR where AccountLink=(select DCLink from client where Account=?))";

    public static final String Q_UPDATING_INVNUM_POS="update InvNum set POSAmntTendered=?,POSChange=? where InvNumber=?";

    public static final String Q_UPLOADING_AGENT_PROFILE_PIC="update agent_login_status set image=CONVERT(VarBinarY(max),?) where name=?";

    public static final String Q_FETCHING_AGENT_PROFILE_PIC="select CONVERT(varchar(max),image) from agent_login_status where name=?";

    public static final String Q_FETCHING_AGENTS_DEFAULTS_ID="select id from agents_defaults_permission where agentName=?";

    public static final String Q_UPDATING_DOC_STATE_ARCHIVED_QUOTATION="update INVNUM set docState=? where autoIndex=?";

    public static final String Q_FETCHING_LICENSE="select TOP 1 token from licenses";

    public static final String Q_FETCHING_ACTIVE_AGENT_COUNT="select sum(hitCount ) from agent_login_status";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_INVNUM="update InvNum set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=?,taxInclusive=1 where orderNum=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumAPFull="update _bvInvNumAPFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where orderNum=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_ORDER_BASIS_bvInvNumARFull="update _bvInvNumARFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where orderNum=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_INVNUM="update InvNum set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=?,taxInclusive=1 where InvNumber=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_bvInvNumAPFull="update _bvInvNumAPFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where InvNumber=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_REF_BASIS_bvInvNumARFull="update _bvInvNumARFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where InvNumber=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_INVNUM="update InvNum set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=?,taxInclusive=1 where autoIndex=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_bvInvNumAPFull="update _bvInvNumAPFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where autoIndex=?";

    public static final String Q_UPDATING_CUSTOM_FIELDS_ON_AUTOINDEX_BASIS_bvInvNumARFull="update _bvInvNumARFull set cContact=?,Address1=?,Address2=?,Address3=?,Address4=?,Address5=?,Address6=?,PAddress1=?,PAddress2=?,PAddress3=?,PAddress4=?,PAddress5=?,PAddress6=?,cTelephone=?,cEMail=? where autoIndex=?";

    public static final String Q_FETCHING_AGENTS_EMAIl_CURRENT_DB="SELECT DB_NAME() AS [Current Database] union select cEmail from _rtblAgents where cAgentName in('Admin',?)";

    public static final String Q_FETCHING_RECEIPTS_AMOUNT="select POSAmntTendered,POSChange from InvNum where InvNumber=?";
}