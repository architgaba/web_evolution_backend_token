package com.sage.api.utils;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.io.Serializable;
import java.util.Date;


/*
 *
 * Error Response Class
 *
 */
@Data
public class ResponseDomain implements Serializable {

    private Boolean status;
    private String message;
    private HttpStatus httpStatus;
    private Integer statusCode;
    private Date timestamp;


    public ResponseDomain() {
    }


    public ResponseDomain(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseDomain(HttpStatus status, Date timestamp, String message,Boolean response) {
        this.httpStatus = status;
        this.timestamp = timestamp;
        this.message = message;
        this.status=response;
        this.statusCode=status.value();
    }

    public static ResponseEntity badRequest(String message,Boolean status) {
        return new ResponseEntity<>(new ResponseDomain(HttpStatus.BAD_REQUEST, new Date(System.currentTimeMillis()), message,status),
                HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> internalServerError(String message,Boolean status) {
        return new ResponseEntity<>(new ResponseDomain(HttpStatus.INTERNAL_SERVER_ERROR,new Date(System.currentTimeMillis()),
                message,status), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
