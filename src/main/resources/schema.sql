
/*
-- ############ CREATING AGENT LOGIN STATUS TABLE ########## ---
DROP TABLE if exists agent_login_status
CREATE TABLE agent_login_status(
	id int NOT NULL PRIMARY KEY,
	android varchar(max) NULL Default 0,
	desktop varchar(max) NULL default 0,
	image varbinary(max) NULL,
	ios varchar(max) default 0,
	name varchar(256) NULL,
	hitCount int Not NULL Default 0
	)

-- ############ INSERTING DATA INTO AGENT LOGIN STATUS TABLE ########## ---
INSERT INTO agent_login_status(id , name)
select idAgents, cAgentName from _rtblAgents;

-- ############ CREATING AGENT DEFAULT STATUS TABLE ########## ---
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='agents_defaults_permission' AND xtype='U')
CREATE TABLE agents_defaults_permission (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	agentName varchar(256) NULL,
	whCode varchar(50) NULL Default 0,
	whFilter bit NULL default 0,
	repCode varchar(50) NULL,
	repFilter bit NULL default 0,
	project varchar(50) NULL Default 0,
	projectFilter bit NULL default 0,
	salesOrderAccCode varchar(50) NULL,
	salesOrderAccFilter bit NULL default 0,
	till varchar(50) NULL Default 0,
	tillFilter bit NULL default 0,
	purchaseOrderAccCode varchar(50) NULL,
	purchaseOrderAccFilter bit NULL default 0,
	pspmCode varchar(50) NULL ,
	pspmFilter bit NULL,
	glAccountsSOList varchar(max) NULL,
	glAccountsSOListFilter bit NULL,
	glAccountsPOList varchar(max) NULL,
	glAccountsPOListFilter bit NULL
	)


*/
